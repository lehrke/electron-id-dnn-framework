mkdir run
cd run
mkdir Preprocessing Training Validation HPOptimization ConversionToAthena
cd Preprocessing
ln -s ../../Preprocessing/*.py .
cd ../Training
ln -s ../../Training/*.py .
cp ../../Training/config/config.json .
cd ../Validation
ln -s ../../Validation/*.py .
cd ../HPOptimization
ln -s ../../HPOptimization/*.py
cp ../../HPOptimization/HPconfig.json .
cd ../ConversionToAthena
ln -s ../../ConversionToAthena/*.py .
ln -s ../../ConversionToAthena/lwtnn/*.py .
cp ../../ConversionToAthena/lwtnn/variables.json .
cp ../../ConversionToAthena/exampleConfFile.conf .
