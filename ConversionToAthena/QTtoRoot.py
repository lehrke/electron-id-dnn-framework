"""Transform a saved QuantileTransformer into a root file as used in athena."""
import uproot
import joblib
import argparse


def getParser():
    """Get options set in command line."""
    parser = argparse.ArgumentParser(description="??")

    parser.add_argument("--logDir", action="store", required=True, type=str,
                        help="Directory where QuantileTransformer is stored.")
    parser.add_argumetn("--qtOutName", action="store", type=str,
                        default="QT.root",
                        help="File name under which the QuantileTransformer " +
                             "will be saved in the logDir")

    args = parser.parse_args()

    return args


def makeTree(args):
    """Load QuantileTransformer and save it in a TTree."""
    qt = joblib.load(args.logDir + "/quantile.transformer")

    ref = qt.references_
    quant = qt.quantiles_

    with uproot.recreate(args.logDir + "/" + args.qtOutName) as f:
        f["tree"] = uproot.newtree({"references": "double",
                                    'd0': "double",
                                    'd0significance': "double",
                                    'trans_TRTPID': "double",
                                    'EoverP': "double",
                                    'dPOverP': "double",
                                    'deltaEta1': "double",
                                    'deltaPhiRescaled2': "double",
                                    'nPixHitsPlusDeadSensors': "double",
                                    'nSCTHitsPlusDeadSensors': "double",
                                    'Rhad1': "double",
                                    'Rhad': "double",
                                    'f1': "double",
                                    'f3': "double",
                                    'weta2': "double",
                                    'Rphi': "double",
                                    'Reta': "double",
                                    'Eratio': "double",
                                    'wtots1': "double",
                                    'et': "double",
                                    'eta': "double"
                                    })

        f["tree"].extend({"references": ref,
                          'd0': quant[:, 0],
                          'd0significance': quant[:, 1],
                          'trans_TRTPID': quant[:, 2],
                          'EoverP': quant[:, 3],
                          'dPOverP': quant[:, 4],
                          'deltaEta1': quant[:, 5],
                          'deltaPhiRescaled2': quant[:, 6],
                          'nPixHitsPlusDeadSensors': quant[:, 7],
                          'nSCTHitsPlusDeadSensors': quant[:, 8],
                          'Rhad1': quant[:, 9],
                          'Rhad': quant[:, 10],
                          'f1': quant[:, 11],
                          'f3': quant[:, 12],
                          'weta2': quant[:, 13],
                          'Rphi': quant[:, 14],
                          'Reta': quant[:, 15],
                          'Eratio': quant[:, 16],
                          'wtots1': quant[:, 17],
                          'et': quant[:, 18],
                          'eta':  quant[:, 19]
                          })


if __name__ == "__main__":
    args = getParser()

    makeTree(args)
