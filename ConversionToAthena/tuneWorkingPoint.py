"""Calculate thresholds on DNN preds to match sig eff of a LH working point."""
import h5py
import numpy as np
import argparse


def getParser():
    """Get options set in command line."""
    parser = argparse.ArgumentParser(description="??")

    parser.add_argument("--inputFile", action="store", required=True, type=str,
                        help="h5 file used to calculate thresholds.")
    parser.add_argument("--workingPoint", action="store", type=str,
                        choices=["LHLoose", "LHMedium", "LHTight"],
                        help="Working point to match signal efficiency")
    parser.add_argument("--preds", action="store", type=str,
                        help="Prediction File.")
    parser.add_argument("--applyBlayerCut", action="store_true",
                        help="Apply rectangular cut on number of Blayer hits.")
    parser.add_argument("--ambiguityCut", action="store", type=int, default=6,
                        help="Apply rectangular cut on ambiguity bit. " +
                             "Default value is 6 which corresponds to only " +
                             "photons and therefore should never happen. 5 " +
                             "is the cut that LHLoose and LHMedium apply, 2 " +
                             "for LHTight")

    args = parser.parse_args()

    return args


def calculateThresholds(args):
    """Calculate thresholds and print them out at the end.

    Args
    ----
        args: ??
            Arguments parsed by the argument parser (see above).

    """
    # Load data
    with h5py.File(args.inputFile, "r") as hf:
        et = hf["test/p_et"][:] / 1000.
        eta = hf["test/p_eta"][:]
        wp = hf["test/p_" + args.workingPoint][:]
        truth = hf["test/p_signal"][:]
        ambiguityBit = hf["test/p_ambiguityBit"][:]
        if args.applyBlayerCut:
            nBlayerCut = hf["test/p_LHBlayerCut"][:]

    # Load predictions
    preds = np.load(args.preds)
    # Convert predictions with a logit transformation
    preds = -np.log(1. / preds[truth == 1] - 1) / 10.
    # Mask all bkg since we're only interested in signal efficiency
    et = et[truth == 1]
    eta = eta[truth == 1]
    wp = wp[truth == 1]

    # Apply potential additional rectangular cuts
    if args.applyBlayerCut:
        preds[nBlayerCut[truth == 1] == 0] = -999.
    preds[ambiguityBit[truth == 1] >= args.ambiguityCut] = -999.

    # Et/eta bins in which the thresholds are determined
    etBins = [15, 20, 25, 30, 35, 40, 45, 6000]
    etaBins = [0.0, 0.1, 0.6, 0.8, 1.15, 1.37, 1.52, 1.81, 2.01, 2.37, 2.47]

    thresholds = np.ones(shape=(7, 10))

    for i in range(len(etBins) - 1):
        for j in range(len(etaBins) - 1):
            # Mask data not in the Et/eta bin
            mask = ((et > etBins[i]) & (et <= etBins[i + 1]) &
                    (np.abs(eta) >= etaBins[j]) &
                    (np.abs(eta) < etaBins[j + 1]))

            # Calucluate sig eff of LH wp
            sigEffLH = np.sum(wp[mask == 1]) / np.sum(mask)

            # Determine threshold needed to achieve sig eff of LH
            sortedPreds = -np.sort(-preds[mask == 1])
            thresholds[i, j] = sortedPreds[int(np.sum(mask) * sigEffLH)]

    # Print the values such that they can be pasted into a config file for
    # athena. The first line is printed twice at the moment since the networks
    # are not trained and therefore not tuned for low Et, but there needs to be
    # reasonable cut values for the bin below the last tuned ones since this
    # bin is used for interpolation of the cut value.
    print("+CutSelector: ", end='')
    for j in range(len(etaBins) - 1):
        print(f"{thresholds[0][j]:.6f}; ", end='')

    print("\n", end='')
    for i in range(len(etBins) - 1):
        print("+CutSelector: ", end='')
        for j in range(len(etaBins) - 1):
            print(f"{thresholds[i][j]:.6f}; ", end='')

        print("\n", end='')


if __name__ == "__main__":
    args = getParser()

    calculateThresholds(args)
