# DNN Framework
This is a framework for training a fully connected deep neural network for electron identification.
The documentation is still work in progress.

## Contents
* [Some Terminology](#some-terminology)
* [Setup](#setup)
* [Preprocessing](#preprocessing)
* [Training](#training)
* [HyperParameter Optimization](#hyperparameter-optimization)
* [Validation](#validation)
* [Conversion to athena](#conversion)
<!--
<a name="general-description"></a>
## General Description
To come ... -->

<a name="some-terminology"></a>
## Some Terminology
variable = feature, (for instance Rhad)  
data point = one electron  
HP = Hyper Parameter (parameter that defines the architecture of the network, e.g. the number of nodes in a hidden layer)


<a name="setup"></a>
## Setup
Clone the code into some directory, then run setup.sh. Creates run directory, copies the config.json file, and links all scripts which should regularly be run. All running should be done in the run directory to not pollute the source directory.
On other clusters, run
```bash
singularity build dnnTrain.simg docker://gitlab-registry.cern.ch/lehrke/electron-id-dnn-framework/train-image
```
which will create a singularity image to use (the image is about 2.1 GB).  
To run all the code you can either open a shell using the singularity image by
```bash
singularity shell <path-to-singularity-image>
```
or you can execute single commands using the singularity image by
```bash
singularity exec <path-to-singularity-image>
```
If for any command you want to be able to use GPUs yo have to add --nv to the singularity call (either exec or shell)  



<a name="preprocessing"></a>
## Preprocessing
This part can produce .h5 files from nTuples using uproot.
Each nTuple is transformed into one .h5 file, which can be combined using virtual datasets.
The Preprocessing includes: applying downsampling, and applying reweighting.
Plots of all input variables can be made before downsampling, with downsampling applied, and with downsampling and weights applied
For detailed explanations and usage see the README in the Preprocessing directory.

<a name="training"></a>
## Training
This part of the framework is responsible for the actual training of a network.
For detailed explanations and usage see the README in the Training directory.

<a name="hyperparameter-optimization"></a>
## HyperParameter Optimization
This part generates config files spanning a desired grid of HPs which can be passed to the training script.
For detailed explanations and usage see the README in the HPOptimization directory.


<a name="validation"></a>
## Validation
This part of the framework will produce performance and control plots for the trained networks.
For detailed explanations and usage see the README in the Validation directory.

<a name="conversion"></a>
## Conversion to athena
This part has conversion scripts for the model and the QuantileTransformer to transform them into the format expected by athena.
Additionally, the threshold of the model can be tuned by matching the signal efficiencies to the desired LH working point.
