"""Functions for TrainingFramework for DNN for electron PID."""

import tensorflow as tf
import tensorflow.keras.layers as lyrs
from tensorflow.keras.regularizers import l2
import tensorflow.keras.callbacks as cb

from .customCallbacks import CyclicLR
import sklearn


def distance_corr(var_1, var_2, normedweight, power=1):
    """
    Calculate distance correlation between two variables with weights.

    Taken from: https://github.com/gkasieczka/DisCo/blob/master/Disco_tf.py
    var_1: First variable to decorrelate (eg mass)
    var_2: Second variable to decorrelate (eg classifier output)
    normedweight: Per-example weight. Sum of weights should add up to N
                  (where N is the number of examples)
    power: Exponent used in calculating the distance correlation

    va1_1, var_2 and normedweight should all be 1D tf tensors with the same
    number of entries

    Usage: Add to your loss function.
           total_loss = BCE_loss + lambda * distance_corr
    """
    xx = tf.reshape(var_1, [-1, 1])
    xx = tf.tile(xx, [1, tf.size(var_1)])
    xx = tf.reshape(xx, [tf.size(var_1), tf.size(var_1)])

    yy = tf.transpose(xx)
    amat = tf.math.abs(xx - yy)

    xx = tf.reshape(var_2, [-1, 1])
    xx = tf.tile(xx, [1, tf.size(var_2)])
    xx = tf.reshape(xx, [tf.size(var_2), tf.size(var_2)])

    yy = tf.transpose(xx)
    bmat = tf.math.abs(xx - yy)

    amatavg = tf.reduce_mean(amat*normedweight, axis=1)
    bmatavg = tf.reduce_mean(bmat*normedweight, axis=1)

    minuend_1 = tf.tile(amatavg, [tf.size(var_1)])
    minuend_1 = tf.reshape(minuend_1, [tf.size(var_1), tf.size(var_1)])
    minuend_2 = tf.transpose(minuend_1)
    Amat = (amat - minuend_1 - minuend_2 +
            tf.reduce_mean(amatavg * normedweight))

    minuend_1 = tf.tile(bmatavg, [tf.size(var_2)])
    minuend_1 = tf.reshape(minuend_1, [tf.size(var_2), tf.size(var_2)])
    minuend_2 = tf.transpose(minuend_1)
    Bmat = (bmat - minuend_1 - minuend_2 +
            tf.reduce_mean(bmatavg * normedweight))

    ABavg = tf.reduce_mean(Amat * Bmat * normedweight, axis=1)
    AAavg = tf.reduce_mean(Amat * Amat * normedweight, axis=1)
    BBavg = tf.reduce_mean(Bmat * Bmat * normedweight, axis=1)

    if power == 1:
        dCorr = (tf.reduce_mean(ABavg * normedweight) /
                 tf.math.sqrt(tf.reduce_mean(AAavg * normedweight) *
                 tf.reduce_mean(BBavg * normedweight)))
    elif power == 2:
        dCorr = ((tf.reduce_mean(ABavg * normedweight))**2 /
                 (tf.reduce_mean(AAavg * normedweight) *
                 tf.reduce_mean(BBavg * normedweight)))
    else:
        dCorr = ((tf.reduce_mean(ABavg * normedweight) /
                 tf.math.sqrt(tf.reduce_mean(AAavg * normedweight) *
                 tf.reduce_mean(BBavg * normedweight)))**power)

    return dCorr


def getActivation(activation):
    """
    Return an instance of the desired activation function.

    Args
    ----
        activation : *str* or *dict*
            The name of an activation function.
            One of "relu", "leakyrelu", "elu".
            Or a config dict.

    Returns
    -------
        *Keras layer instance*

    """
    if isinstance(activation, dict):
        act, kwargs = activation['className'], activation['config']
    else:
        act = activation
        kwargs = {}

    act = activation.lower()
    if act == "leakyrelu":
        return lyrs.LeakyReLU(**kwargs)
    elif act == "elu":
        return lyrs.ELU(**kwargs)
    elif act == "relu":
        return lyrs.ReLU(**kwargs)
    else:
        raise ValueError("Activation function " + activation +
                         " not supported.")


def getOptimizer(optimizer):
    """
    Return an instance of the desired optimizer.

    Args
    ----
        optimizer : *str* or *dict*
            The name of an optimizer or a config dict.
            One of "adam".

    Returns
    -------
        *TensorFlow optimizer*

    """
    if isinstance(optimizer, dict):
        optimizer, kwargs = optimizer['className'], optimizer['config']
    else:
        kwargs = {}

    opt = optimizer.lower()
    if opt == "adam":
        return tf.keras.optimizers.Adam(**kwargs)
    else:
        raise ValueError("Optimizer " + optimizer + " not supported.")


def buildHiddenLayers(architecture, activation,
                      regularization, inputs, doBatchNorm, dropout):
    """
    Build hidden layers.

    Args
    ----
        architecture : *list of ints*
            Each entry in the list corresponds to one Dense
            layer with the specified number of nodes.

        activation : *str*
            The name of an activation function.
            One of 'relu', 'leakyrelu', 'elu'.

        regularization : *float*
            l2 regularization that is applied to
            each dense layer.

        inputs : *TensorFlow tensor*
            The input that should be processed by
            the network.

        doBatchNorm : *bool*
            If True, apply BatchNormalization after
            each activation function.

        dropout : *float*
            Between 0 and 1. Add dropout with this rate after each
            activation function (batch normalization if
            it is used).

    Returns
    -------
        *TensorFlow tensor*

    """
    for i, n in enumerate(architecture):
        if i == 0:
            tns = lyrs.Dense(n, kernel_regularizer=l2(regularization))(inputs)
        else:
            tns = lyrs.Dense(n, kernel_regularizer=l2(regularization))(tns)

        tns = getActivation(activation)(tns)

        if doBatchNorm:
            tns = lyrs.BatchNormalization()(tns)

        if dropout > 0:
            tns = lyrs.Dropout(dropout)(tns)

    return tns


def buildModel(inputVars, activation, architecture,
               regularization, doBatchNorm, verbose, dropout,
               discoCoeff=0, multiClass=False):
    """
    Build full model.

    Args
    ----
        inputVars : *list of strs*
            List of the used input variables.

        architecture : *list of ints*
            Each entry in the list corresponds to one Dense
            layer with the specified number of nodes.

        activation : *str*
            The name of an activation function.
            One of 'relu', 'leakyrelu', 'elu'.

        regularization : *float*
            l2 regularization that is applied to
            each dense layer.

        inputs : *TensorFlow tensor*
            The input that should be processed by
            the network.

        doBatchNorm : *bool*
            If True, apply BatchNormalization after
            each activation function.

        verbose : *??*
            Some sort of verbosity ???

        dropout : *float*
            Between 0 and 1. Add dropout with this rate after each
            activation function (batch normalization if
            it is used).

        discoCoeff: *float*
            Coefficient to be used for distance correlation. If the value is
            zero no term will be added to the loss function.
            Default: 0

        mutliClass: *bool*
            Train a multiclass or a binary classifier. At the moment only 5
            classes are possible.
            Default: False

    Returns
    -------
        model: *TensorFlow model*
            tf model which needs to be compiled and then can be trained.

    """
    nInput = len(inputVars)

    inputs = lyrs.Input(shape=(nInput,), name="ScalarInputs")
    if discoCoeff > 0:
        decorrelate = lyrs.Input(shape=(1,), name="VariableToDecorrelate")
        w = lyrs.Input(shape=(1,), name="WeightsForDisCo")

    tns = buildHiddenLayers(architecture,
                            activation,
                            regularization,
                            inputs,
                            doBatchNorm,
                            dropout)
    if not multiClass:
        outputs = lyrs.Dense(1, activation="sigmoid",
                             kernel_regularizer=l2(regularization),
                             dtype="float32")(tns)
    else:
        outputs = lyrs.Dense(5, activation="softmax",
                             kernel_regularizer=l2(regularization),
                             dtype="float32")(tns)

    if discoCoeff > 0:
        model = tf.keras.Model(inputs=[inputs, decorrelate, w],
                               outputs=outputs)
        model.add_loss(discoCoeff * distance_corr(outputs, decorrelate, w))

    else:
        model = tf.keras.Model(inputs=[inputs], outputs=outputs)

    if verbose:
        print(model.summary())

    return model


def getCallbacks(logDir, metric, earlyStopping, lrSchedule=None):
    """
    Register all callbacks.

    Args
    ----
        logDir : *str*
            Directory everything will be stored in.

        metric : *str*
            Metric which will be used for early stopping
            and monitoring the best model.

        earlyStopping : *int*
            Number of epochs for early stopping.
            If earlyStoppingRound <= 0 no early stopping
            is applied.


    Returns
    -------
        *list of TensorFlow callbacks*

    """
    callbacks = []

    tensorboard = cb.TensorBoard(log_dir=logDir)
    callbacks.append(tensorboard)

    modelCheckpoint = cb.ModelCheckpoint(filepath=logDir+"/bestModel.h5",
                                         monitor=metric,
                                         verbose=0,
                                         save_best_only=True)
    callbacks.append(modelCheckpoint)

    if earlyStopping > 0:
        earlyStoppingCallback = cb.EarlyStopping(monitor=metric,
                                                 patience=earlyStopping)
        callbacks.append(earlyStoppingCallback)

    if lrSchedule is not None:
        if lrSchedule["className"] == "cyclicLR":
            lrScheduleCB = CyclicLR(**lrSchedule["config"])
            callbacks.append(lrScheduleCB)

    return callbacks


def calcPermutationImportance(model, X, y):
    """
    Calculate permutation importance from ML model with given data.

    Args
    ----
        model : any kind of ML model (tensorflow DNN in mind)
            Model for which permutation importance will be calculated

        X : *array*
            Input data for the model.

        y : *array*
            Truth values for the input data.

    Returns
    -------
        base_score : *float*
            Score (logLoss) with no permutations of the input data

        score_decreases : *array*
            Amount that the score decreased for each run permutation.
            Note: For logLoss a negative score_decrease means that the
            model looses prediction power when the variable is permuted.

    """
    def scoreFunc(X, y):
        preds = model.predict(X).flatten()
        preds[preds < 1e-10] = 1e-5  # avoid nans and infinities
        preds[preds > (1-1e-5)] = 1 - 1e-5  # avoid nans and infinities
        logLoss = sklearn.metrics.log_loss(y, preds)
        return logLoss

    from eli5.permutation_importance import get_score_importances
    baseScore, scoreDecreases = get_score_importances(scoreFunc, X, y)
    return baseScore, scoreDecreases
