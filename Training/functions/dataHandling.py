"""Functions handling data for the DNN (load, mask, preprocess, etc)."""
import numpy as np
import tensorflow as tf
from sklearn.preprocessing import (QuantileTransformer, RobustScaler,
                                   MinMaxScaler)

import sys
sys.path.insert(0, sys.path[0]+"/../../")

from CommonFunctions.functions import readData, weightedQuantileTransformer


def getLabels(data, truthVar, multiClass, mask):
    """
    Get labels for the training, mutliclass or binary.

    Args
    ----
        data : *dict*
            Dictionary containing data.

        truthVar : *list*
            List of variables needed to determine the labels. In the binary
            case this is just 'p_signal', in the multiclass case 'p_signal',
            'p_iff', and 'p_truthType' are needed at the moment.

        multiClass : *bool*
            Whether to get labels for a multiclass algorithm or a binary
            classifier.

        mask : *array*
            Array masking entries of the labels which are not needed (e.g. due
            to downsampling).

    Returns
    -------
        labels : *array*
            Array containing the labels needed for the training. In the
            multiclass case the labels are one-hot encoded.

    """
    if not multiClass:
        return data[truthVar[0]][mask == 1]

    else:
        labels = np.zeros(data[truthVar[0]].shape)
        # Signal is 0 so nothing necessary
        # PhotonConv
        labels[data["p_iff"] == 5] = 1
        # HeavyFlavor
        labels[(data["p_iff"] == 8) | (data["p_iff"] == 9)] = 2
        # LFEgamma
        labels[(data["p_iff"] == 10) &
               ((data["p_truthType"] == 4) | (data["p_truthType"] == 16))] = 3
        # LFHadron
        labels[(data["p_iff"] == 10) & (data["p_truthType"] == 17)] = 4

        return tf.keras.utils.to_categorical(labels[mask == 1])


def loadAndPrepareData(fileName, sett, downsample, weights,
                       downsampleWeightDir, inputVars, truthVar,
                       multiClass=False, discoVar="", discoCoeff=0,
                       discoPreprocessing="", preproc=None, discoPreproc=None):
    """
    Load data, mask data, and preprocess data.

    Args
    ----
        fileName: *str*
            Name of the .h5 file where the data is stored.
        sett: *str*
            Which dataset to load: train, val, or test
        downsample: *bool*
            Whether to apply downsampling to the data or not.
        weights: *bool*
            Whether to apply per sample weights during the training or not.
        downsampleWeightDir: *str*
            Directory where downsampling masks and per sample weights are
            stored.
        inputVars: *list*
            List of variables used for the training.
        truthVar: *list* or *str*
            Variables or Variable necessary to get the truth information. In
            binary classification everything is contained in one variable.
        multiClass: *bool*
            Whether to get labels for a multi class algorithm.
        discoVar: *str*
            Variable which will be decorrelated using DisCo when wanted.
        discoCoeff: *float*
            Coefficient determining the strength of the decorrelation. If it is
            zero no decorrelation will be applied.
        discoPreprocesssing: *str*
            Which preprocessing should be applied to the variable that is being
            decorrelated.
        preproc: sklearn based transformer/scaler
            Applies preprocessing to the data. Only used for val and test set,
            since on train set it is being created and fitted.
        discoPreproc: sklearn based transformer/scaler or *str*
            Preprocessing applied to the disco var for the validation set.

    Returns
    -------
        X: numpy.array
            Data input for the DNN
        Y: numpy.array
            Truth information input for the DNN
        outWeights: numpy.array
            Per sample weights input for the DNN
        discoVar: numpy.array
            Variable to be decorrelated from input for the DNN if decorrelation
            is wanted
        preproc: sklearn based transformer/scaler
            Transformer/Scaler object which is applied to the data before
            training/evaluation.
        discoPreproc: sklearn based transformer/scaler or *str*
            Preprocessing applied to the disco var.

    """
    if type(truthVar) == str:
        truthVar = [truthVar]

    variables = inputVars + truthVar
    if discoCoeff > 0:
        variables += [discoVar]

    data = readData(fileName, {sett: None}, variables)[sett]

    mask, outWeights = getMaskWeights(downsample, weights, downsampleWeightDir,
                                      sett, data[variables[0]].shape)

    # Test to change hard coded values in f3 to weighted means in previous eta
    # bin (Could also try that for TRTPID)
    data["p_f3Masked"][np.abs(data["p_eta"]) > 2.01] = 0.05
    data["p_TRTPID"][(np.abs(data["p_eta"]) > 2.01) &
                     (np.abs(data["p_TRTPID"]) < 1e-6)] = 0.15
    # data["p_f3Masked"][np.abs(data["p_eta"]) > 2.01] = 0

    X = np.array([np.abs(data[f"{var}"][mask == 1]) if var is "p_eta" else
                 data[f"{var}"][mask == 1] for var in inputVars]).T

    for var in list(set(inputVars) - set([discoVar])):
        del data[var]

    if sett == "train" or sett == "val":
        discoVar, discoPreproc = None, None
        Y = getLabels(data, truthVar, multiClass, mask)
        if sett == "train":
            preproc = weightedQuantileTransformer(X, np.linspace(0, 1, 1000),
                                                  outWeights)
            X = preproc.transform(X)
            if discoCoeff > 0:
                discoVar = data[discoVar][mask == 1]
                discoVar, discoPreproc = preprocessDisco(discoVar,
                                                         discoPreprocessing,
                                                         preproc=None)

        elif sett == "val":
            X = preproc.transform(X)

            if discoPreproc is not None:
                discoVar = data[discoVar][mask == 1]
                discoVar, _ = preprocessDisco(discoVar, discoPreprocessing,
                                              preproc=discoPreproc)

        return X, Y, outWeights, discoVar, preproc, discoPreproc
    elif sett == "test":
        return preproc.transform(X), None, None, None, None, None


def getMaskWeights(downsample, weights, downsampleWeightDir, sett, shape):
    """
    Load downsampling mask and per sample weights.

    Args
    ----
        downsample: *bool*
            Whether to apply downsampling to the data or not.
        weights: *bool*
            Whether to apply per sample weights during the training or not.
        downsampleWeightDir: *str*
            Directory where downsampling masks and per sample weights are
            stored.
        sett: *str*
            For which dataset the mask and the weights should be loaded: train,
            val, or test

    Returns
    -------
        mask: numpy.array
            Mask removing events from downsampling.
        outWeights: numpy.array
            Per sample weights input for the DNN.

    """
    if downsample:
        mask = np.load(downsampleWeightDir + f"/downsamplingMask{sett}.npy")
    else:
        mask = np.ones(shape, dtype=np.int)

    if weights:
        outWeights = np.load(downsampleWeightDir +
                             f"/downsamplingWeights{sett}.npy")[mask == 1]
    else:
        outWeights = np.ones(np.sum(mask))

    return mask, outWeights


def preprocessDisco(discoVar, discoPreprocessing, preproc=None):
    """
    Apply preprocessing to the variabe which should be decorrelated.

    Args
    ----
        discoVar: numpy.array
            Variable which will be decorrelated using DisCo when wanted.
        discoPreprocesssing: *str*
            Which preprocessing should be applied to the variable that is being
            decorrelated.
        preproc: sklearn based transformer/scaler or *str*
            Preprocessing applied to the disco var for the validation set.

    Returns
    -------
        discoVar: numpy.array
            Variable which will be decorrelated using DisCo with preprocessing
            applied,
        discoPreproc: sklearn based transformer/scaler or *str*
            Preprocessing applied to the disco var for the validation set.

    """
    if preproc is None:
        if discoPreprocessing == "qt":
            discoPreproc = QuantileTransformer()
            discoVar = discoPreproc.fit_transform(discoVar.reshape(-1, 1))
        elif discoPreprocessing == "rs":
            discoPreproc = RobustScaler()
            discoVar = discoPreproc.fit_transform(discoVar.reshape(-1, 1))
        elif discoPreprocessing == "mm":
            discoPreproc = MinMaxScaler()
            discoVar = discoPreproc.fit_transform(discoVar.reshape(-1, 1))
        elif discoPreprocessing == "log":
            discoVar = np.log(discoVar)
            discoPreproc = "log"
        else:
            discoPreproc = "none"

    else:
        if type(discoPreproc) != str:
            discoVar = discoPreproc.transform(discoVar.reshape(-1, 1))
        else:
            if discoPreprocessing == "log":
                discoVar = np.log(discoVar)

    return discoVar, discoPreproc
