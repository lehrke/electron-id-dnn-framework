Training Documentation:

The main script is train.py which trains a network.
It loads the data from a .h5 file, transforms the data, trains the network, and calculates the output for the test set.

Command line arguments for the train.py script:
* logDir: Directory that will be created and all the outputs will be saved in here
* inputDir: Not implemented correctly at the moment
* configFile: Name of the configuration file to use
* permImp: Calculate permutation importance of the network (probably not working correctly right now)
* task: Definitely not working right now
* HPConfig: Name of an additional configuration file. HPs set in here will override the ones in the regular config file. This can be used to do HP optimization.

Parameters in the config file:
* logDir: Same as the command line argument.
* inputDir: Same as the command line argument. Does not work correctly at the moment.
* weights: Boolean whether weights should be used during the training.
* epochs: Number of maximal epochs to be trained given as an int.
* batchSize: Batch size to be used during the training given as an int.
* earlyStopping: Number of early stopping rounds used during the training given as an int.
<!-- optimizer
className
config
lr
lrSchedule
className
config
baseLr
maxLr
nEpochs
trainStepsPerEpoch -->
* metrics: List of metrics to be monitored during the training. The first one in the list will be used for early stopping and saving the best model.
* weightedMetrics: List of metrics to be monitored during the training including the weights.
* loss: Loss to be used during the training.
* architecture: Number of hidden layers with the number of nodes given as a list. Each entry in the list corresponds to a hidden layer with the given number as number of nodes.
* activation: Activation function used after each hidden layer.
* regularization: L2-regularization used in each hidden layer.
* doBatchNorm: Whether to use batch normalization or not.
* dropout: Dropout rate to be used during the training.
* verbose: Verbosity during the training
