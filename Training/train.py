"""
Main script of the framework.

This script trains the NN, calculates permutation importances (if wanted),
and predicts on a test set (this step can be run alone).
"""

import numpy as np
import json
import argparse
import joblib
from copy import copy
import tensorflow as tf

# Disable tf eager execution since it seems to double memory usage
from tensorflow.python.framework.ops import disable_eager_execution
disable_eager_execution()

import sys
sys.path.insert(0, sys.path[0]+"/../")
from Training.functions.functions import buildModel, getCallbacks, getOptimizer
from Training.functions.dataHandling import loadAndPrepareData
import Validation.evaluateAfterTraining as ev
from CommonFunctions.functions import mkdir


def getParser():
    """Get options set in command line."""
    parser = argparse.ArgumentParser(description="Options for NN Training")

    parser.add_argument("--logDir", action="store", default="", type=str,
                        help="Directory where outputs will be saved.")
    parser.add_argument("--trainFile", action="store", default="", type=str,
                        help="File where training/validation data will be " +
                             "loaded from.")
    parser.add_argument("--testFile", action="store", default="", type=str,
                        help="File where test data will be " +
                             "loaded from.")
    parser.add_argument("--downsampleWeightDir", action="store", default="",
                        type=str,
                        help="Directory where downsampling masks and " +
                             "weights will be loaded from.")
    parser.add_argument("--configFile", action="store", default="config.json",
                        type=str,
                        help="Config File to read in the configs. Note: " +
                             "Options set on the command line overwrite " +
                             "options from the config file.")
    parser.add_argument("--permImp", action="store_true",
                        help="Set this flag to calculate permutation " +
                             "importance after the training in finished")
    parser.add_argument("--task", action="store", default="train", type=str,
                        choices=["train", "predict", "saveModel"],
                        help="Train a new model or use an already trained " +
                             "model to predict on a test set")
    parser.add_argument("--HPConfig", action="store", type=str, default=None,
                        help="Config which overwrites entries from the " +
                             "configFile to do HP optimization")
    args = parser.parse_args()

    return args


def train(config, doPermImp):
    """Train the neural network.

    Trains, calculates permutation importances, and predicts
    based on the configuration.
    """
    ###########################################################################
    # Create logDir it it does not exist already
    ###########################################################################
    mkdir(config["logDir"])

    inputVars = ["p_d0", "p_d0Sig", "p_TRTPID", "p_EoverP",
                 "p_dPOverP", "p_deltaEta1", "p_deltaPhiRescaled2",
                 "p_nPixelHits",  # "p_numberOfInnermostPixelLayerHits",
                 "p_nSCTHits",
                 "p_Rhad1", "p_Rhad", "p_f1", "p_f3Masked",
                 "p_weta2", "p_Rphi", "p_Reta",  "p_Eratio", "p_wtots1",
                 "p_et", "p_eta"]
    if not config["multiClass"]:
        truthVar = "p_signal"
    else:
        truthVar = ["p_signal", "p_iff", "p_truthType"]

    if config["task"] == "train":
        #######################################################################
        # Build and compile the model, and save configuration and architecture
        #######################################################################
        model = buildModel(inputVars,
                           config["activation"],
                           config["architecture"],
                           config["regularization"],
                           config["doBatchNorm"],
                           config["verbose"],
                           config["dropout"],
                           config["discoCoeff"],
                           config["multiClass"])

        model.compile(optimizer=getOptimizer(config["optimizer"]),
                      loss=config["loss"],
                      metrics=config["metrics"],
                      weighted_metrics=config["weightedMetrics"])

        architecture = model.to_json()
        with open(config["logDir"]+"/architecture.json", "w") as archFile:
            archFile.write(architecture)

        #######################################################################
        # Set callbacks and fit the model
        #######################################################################
        (X, Y, weights, discoVar, preproc,
         discoPreproc) = loadAndPrepareData(config["trainFile"], "train",
                                            config["downsample"],
                                            config["weights"],
                                            config["downsampleWeightDir"],
                                            inputVars, truthVar,
                                            config["multiClass"],
                                            config["discoVar"],
                                            config["discoCoeff"],
                                            config["discoPreprocessing"])

        joblib.dump(preproc, config["logDir"] + "/quantile.transformer")

        config["trainPoints"] = X.shape[0]
        tspe = int(config["trainPoints"] / config["batchSize"]) + 1
        config["trainStepsPerEpoch"] = tspe

        if config["lrSchedule"]:
            if config["lrSchedule"]["config"]:
                config["lrSchedule"]["config"]["trainStepsPerEpoch"] = tspe
        json.dump(config, open(config["logDir"]+"/config.json", "w"), indent=4)

        (valX, valY, valWeights,
         valDiscoVar, _, _) = loadAndPrepareData(config["trainFile"], "val",
                                                 config["downsample"],
                                                 config["weights"],
                                                 config["downsampleWeightDir"],
                                                 inputVars, truthVar,
                                                 config["multiClass"],
                                                 config["discoVar"],
                                                 config["discoCoeff"],
                                                 preproc=preproc,
                                                 discoPreproc=discoPreproc)

        callbacks = getCallbacks(logDir=config["logDir"],
                                 metric="val_" + config["weightedMetrics"][0] +
                                        "_1" if config["weightedMetrics"] else
                                        "val_" + config["metrics"][0],
                                 earlyStopping=config["earlyStopping"],
                                 lrSchedule=config["lrSchedule"])

        if config["discoCoeff"] > 0:
            X = [X, discoVar, weights]
            valData = ([valX, valDiscoVar, valWeights],
                       valY, valWeights)
        else:
            valData = (valX, valY, valWeights)

        if config["useClassWeights"]:
            classWeights = config["classWeights"]
        else:
            classWeights = None
        model.fit(X, Y, batch_size=config["batchSize"],
                  epochs=config["epochs"], verbose=config["verbose"],
                  callbacks=callbacks,
                  sample_weight=weights,
                  class_weight=classWeights,
                  validation_data=valData)

        del X, Y, weights, discoVar,
        del valX, valY, valDiscoVar, valWeights, valData

    else:
        #######################################################################
        # Load and create the model from a saved architecture file
        #######################################################################
        with open(config["logDir"] + "/architecture.json", "r") as jsonFile:
            arch = jsonFile.read()

        model = tf.keras.models.model_from_json(arch)

        preproc = joblib.load(config["logDir"] + "/quantile.transformer")

    ###########################################################################
    # Load weights of best model, predict on test set and save predictions
    ###########################################################################
    model.load_weights(config["logDir"] + "/bestModel.h5")

    if config["task"] == "saveModel":
        model.save(config["logDir"] + "/tf_model")
        return

    # Lwtnn scripts only work with only weights saved and not the whole model
    model.save_weights(config["logDir"] + "/weightsForLwtnn.h5")

    (X, _, _,
     _, _, _) = loadAndPrepareData(config["testFile"], "test", False, False,
                                   "", inputVars, truthVar, preproc=preproc)

    if config["discoCoeff"] > 0:
        preds = model.predict([X, np.zeros(X.shape[0]),
                               np.zeros(X.shape[0])],
                              batch_size=config["batchSize"])
    else:
        preds = model.predict(X, batch_size=config["batchSize"])

    if not config["multiClass"]:
        preds = preds.flatten()
    else:
        preds = preds.reshape(-1, 5)

    np.save(config["logDir"] + "/preds.npy", preds)


def replaceHPKeys(config, hpConfig):
    """
    Replace settings in config file with values given in HP config file.

    Args
    ----
        config: *dict*
            Original config dictionary.
        hpConfig: *dict*
            Dictionary with HP values that should be used for this training.

    Returns
    -------
        config: *dict*
            Updated config dictionary.

    """
    for key in hpConfig:
        if key in config:
            if type(config[key]) == dict:
                config[key] = replaceHPKeys(config[key], hpConfig[key])
            else:
                config[key] = hpConfig[key]
    return config


def getConfig(args):
    """Load the configFile and compare settings to the command line ones."""
    # Load the config file
    with open(args.configFile, "r") as configFile:
        config = json.load(configFile)

    # if arguments are set on the command line they overwrite the values given
    # in the config file
    if args.logDir != "":
        config["logDir"] = args.logDir
    if args.trainFile != "":
        config["trainFile"] = args.trainFile
    if args.testFile != "":
        config["testFile"] = args.testFile
    if args.downsampleWeightDir != "":
        config["downsampleWeightDir"] = args.downsampleWeightDir

    config["task"] = args.task

    with open(config["downsampleWeightDir"] + "/metaData.json") as mf:
        md = json.load(mf)
    # For multiClass classifier all classes should have the same sum of weights
    if config["useClassWeights"]:
        d = copy(md["classWeights"])
        config["classWeights"] = {}
        config["classWeights"][0] = d["0"]
        config["classWeights"][1] = d["1"]
        config["classWeights"][2] = d["2"]
        config["classWeights"][3] = d["3"]
        config["classWeights"][4] = d["4"]

    # Load metadata information, which includes number of points, which file,
    # downsampling and reweighting information etc,
    for key in md:
        if type(md[key]) == dict:
            continue
        config[key] = md[key]

    # If HP optimization overwrite values in the config file by the ones given
    # by the HP config file
    if args.HPConfig:
        with open(args.HPConfig, "r") as hpConfigFile:
            hpConfig = json.load(hpConfigFile)

        config = replaceHPKeys(config, hpConfig)

    # if no downsampling is used don"t use weights since the weights are
    # calculated for the downsampled distributions
    if not config["downsample"]:
        config["weights"] = False

    return config


if __name__ == "__main__":
    # Read arguments from the command line
    args = getParser()

    # Load configFile and finalize config directory
    config = getConfig(args)

    # Based on the set options do training, permImp, predictions, ...
    train(config, args.permImp)

    if args.HPConfig:
        df = ev.processLogDir(config, HPConfig=args.HPConfig)

        ev.writeCombinedDataFrame(args.logDir + "/../combined.csv", df)
