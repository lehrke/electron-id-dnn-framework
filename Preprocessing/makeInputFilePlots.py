"""Make plots of all input variables from an h5 file."""
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import argparse

import sys
sys.path.insert(0, sys.path[0]+"/../")

from Preprocessing.functions.functions import plotHists, plotVarDict
from CommonFunctions.functions import readData, mkdir


def getParser():
    """Get options set in command line."""
    parser = argparse.ArgumentParser(description="Options for making plots " +
                                                 "of an input h5 file")

    parser.add_argument("--inputFile", action="store", type=str,
                        default="VDSLocal.h5",
                        help="Input file which should be downsampled.")
    parser.add_argument("--outDir", action="store", type=str,
                        help="Output directory inside the 'Plots/' directory")
    parser.add_argument("--sample", action="store", type=str, default="ZJF",
                        help="Sample name which will be put on the plot")
    parser.add_argument("--kinematic", action="store_true",
                        help="Make plots of eta and et distributions")
    parser.add_argument("--other", action="store_true",
                        help="Make plots of all other variables")
    parser.add_argument("--all", action="store_true",
                        help="Make plots of all variables")
    parser.add_argument("--downsampleDir", action="store", type=str,
                        default="")

    args = parser.parse_args()

    return args


def plot(args, sampleName):
    """Plot all wanted plots, main function of the script."""
    ###########################################################################
    # Load data and create all necessary masks
    ###########################################################################
    inputVars = ["p_d0", "p_d0Sig",
                 "p_dPOverP", "p_deltaEta1", "p_deltaPhiRescaled2", "p_TRTPID",
                 "p_EoverP",
                 "p_Rhad1", "p_Rhad", "p_f3", "p_f1",
                 "p_weta2", "p_Rphi", "p_Reta",  "p_Eratio", "p_wtots1",
                 "p_et", "p_eta", "p_mu", "p_nvtx"]
    vars = ["p_iff", "p_truthType", "p_et", "p_eta", "p_signal"] + inputVars

    data = readData(args.inputFile, {"train": None}, vars)["train"]

    mkdir("Plots")
    mkdir("Plots/" + args.outDir)

    binaryMasks = {"Signal": (data["p_signal"] == 1),
                   "Background": (data["p_signal"] == 0)}

    multiMasks = {
                  "Signal": (data["p_signal"] == 1),
                  "PhotonConv": (data["p_iff"] == 5),
                  "HeavyFlavor": (data["p_iff"] == 8) |
                                 (data["p_iff"] == 9),
                  "LFEgamma": ((data["p_iff"] == 10) &
                               ((data["p_truthType"] == 4) |
                                (data["p_truthType"] == 16))),
                  "LFHadron": ((data["p_iff"] == 10) &
                               (data["p_truthType"] == 17))
                 }

    ###########################################################################
    # Plot bar plot of contributions of the single classes to the total set
    ###########################################################################
    tot = data["p_iff"].shape[0]
    ratios = np.array([np.sum(multiMasks["Signal"]) / tot,
                       np.sum(multiMasks["PhotonConv"]) / tot,
                       np.sum(multiMasks["HeavyFlavor"]) / tot,
                       np.sum(multiMasks["LFEgamma"]) / tot,
                       np.sum(multiMasks["LFHadron"]) / tot,
                       (tot - np.sum(multiMasks["Signal"] |
                                     multiMasks["PhotonConv"] |
                                     multiMasks["HeavyFlavor"] |
                                     multiMasks["LFEgamma"] |
                                     multiMasks["LFHadron"])) / tot])

    width = 0.4
    labels = ["Signal", "PhotonConv", "HeavyFlavor", "LFEgamma",
              "LFHadron", "Other"]
    x = np.arange(len(labels))

    plt.rcParams["axes.grid"] = False
    fig, ax = plt.subplots()
    bar1 = ax.bar(x, ratios * 100, width, label=sampleName)

    def autolabel(rects):
        """Attach a text label above each bar, displaying its height."""
        for rect in rects:
            height = rect.get_height()
            ax.annotate(f'{height:.1f}%',
                        xy=(rect.get_x() + rect.get_width() / 2, height),
                        xytext=(0, 3),
                        textcoords="offset points",
                        ha='center', va='bottom',
                        fontsize=24)

    autolabel(bar1)
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.set_ylabel("% of training data before preprocessing")
    plt.tight_layout()
    plt.legend()
    plt.savefig("Plots/" + args.outDir + "/compositionTrainingSetImages.pdf")
    plt.close("all")
    plt.rcParams["axes.grid"] = True

    if args.kinematic or args.all:
        mkdir("Plots/" + args.outDir + "/Kinematic/")
        #######################################################################
        # Distributions versus ET
        #######################################################################
        bins = np.logspace(np.log10(4500), np.log10(2000000), 101)
        # Binary distribution
        plotHists(data["p_et"], binaryMasks, bins,
                  "Plots/" + args.outDir + "/Kinematic/EtBinary.pdf",
                  xlabel=r"$E_T\ \mathrm{[MeV]}$", xlog=True, ylog=True,
                  text=sampleName + " sample")

        # Multiclass distribution
        plotHists(data["p_et"], multiMasks, bins,
                  "Plots/" + args.outDir + "/Kinematic/EtMultiClass.pdf",
                  xlabel=r"$E_T\ \mathrm{[MeV]}$", xlog=True, ylog=True,
                  text=sampleName + " sample")

        #######################################################################
        # Distributions versus eta
        #######################################################################
        bins = np.linspace(-2.47, 2.47, 101)
        # Binary distribution
        plotHists(data["p_eta"], binaryMasks, bins,
                  "Plots/" + args.outDir + "/Kinematic/EtaBinary.pdf",
                  xlabel=r"$\eta$", xlog=False, ylog=True,
                  text=sampleName + " sample")

        # Multiclass distribution
        plotHists(data["p_eta"], multiMasks, bins,
                  "Plots/" + args.outDir + "/Kinematic/EtaMultiClass.pdf",
                  xlabel=r"$\eta$", xlog=False, ylog=True,
                  text=sampleName + " sample")

    if args.other or args.all:
        mkdir("Plots/" + args.outDir + "/Other/")
        #######################################################################
        # Distributions versus all other input varables
        #######################################################################
        for var in list(set(inputVars) -
                        set(["p_et", "p_eta"])):
            # Binary distributions no range in the plot
            plotHists(data[var], binaryMasks, 100,
                      f"Plots/" + args.outDir + "/Other/" +
                      f"{plotVarDict[var]['fileName']}BinaryNoRange.pdf",
                      xlabel=plotVarDict[var]["plotName"],
                      xlog=plotVarDict[var]["xlog"],
                      ylog=plotVarDict[var]["ylog"],
                      text=sampleName + " sample")

            # Binary distributions with a set range in the plot
            plotHists(data[var], binaryMasks, plotVarDict[var]["bins"],
                      f"Plots/" + args.outDir + "/Other/" +
                      f"{plotVarDict[var]['fileName']}Binary.pdf",
                      xlabel=plotVarDict[var]["plotName"],
                      xlog=plotVarDict[var]["xlog"],
                      ylog=plotVarDict[var]["ylog"],
                      text=sampleName + " sample")

            # Multiclass distributions no range in the plot
            plotHists(data[var], multiMasks, 100,
                      f"Plots/" + args.outDir + "/Other/" +
                      f"{plotVarDict[var]['fileName']}MultiClassNoRange.pdf",
                      xlabel=plotVarDict[var]["plotName"],
                      xlog=plotVarDict[var]["xlog"],
                      ylog=plotVarDict[var]["ylog"],
                      text=sampleName + " sample")

            # Multiclass distributions with a set range in the plot
            plotHists(data[var], multiMasks, plotVarDict[var]["bins"],
                      f"Plots/" + args.outDir + "/Other/" +
                      f"{plotVarDict[var]['fileName']}MultiClass.pdf",
                      xlabel=plotVarDict[var]["plotName"],
                      xlog=plotVarDict[var]["xlog"],
                      ylog=plotVarDict[var]["ylog"],
                      text=sampleName + " sample")

    if args.downsampleDir != "":
        mask = np.load(args.downsampleDir + "/downsamplingMasktrain.npy")
        weights = np.load(args.downsampleDir + "/downsamplingWeightstrain.npy")

        binaryMasks = {"Signal": (data["p_signal"] == 1) & (mask == 1),
                       "Background": (data["p_signal"] == 0) & (mask == 1)}

        multiMasks = {
                      "Signal": (data["p_signal"] == 1) & (mask == 1),
                      "PhotonConv": (data["p_iff"] == 5) & (mask == 1),
                      "HeavyFlavor": ((data["p_iff"] == 8) |
                                      (data["p_iff"] == 9)) & (mask == 1),
                      "LFEgamma": (((data["p_iff"] == 10) &
                                    ((data["p_truthType"] == 4) |
                                     (data["p_truthType"] == 16))) &
                                   (mask == 1)),
                      "LFHadron": ((data["p_iff"] == 10) &
                                   (data["p_truthType"] == 17)) & (mask == 1)
                     }

        mkdir("Plots/" + args.outDir + "/Downsampled/")
        mkdir("Plots/" + args.outDir + "/Weighted/")
        if args.kinematic or args.all:
            ###################################################################
            # Distributions versus ET
            ###################################################################
            bins = np.logspace(np.log10(4500), np.log10(2000000), 101)
            # Binary distribution
            plotHists(data["p_et"], binaryMasks, bins,
                      "Plots/" + args.outDir + "/Downsampled/EtBinary.pdf",
                      xlabel=r"$E_T\ \mathrm{[MeV]}$", xlog=True, ylog=True,
                      text=sampleName + " sample\nDownsampled distribution")

            # Multiclass distribution
            plotHists(data["p_et"], multiMasks, bins,
                      "Plots/" + args.outDir + "/Downsampled/EtMultiClass.pdf",
                      xlabel=r"$E_T\ \mathrm{[MeV]}$", xlog=True, ylog=True,
                      text=sampleName + " sample\nDownsampled distribution")

            # Binary distribution with weights
            plotHists(data["p_et"], binaryMasks, bins,
                      "Plots/" + args.outDir + "/Weighted/EtBinary.pdf",
                      weights=weights,
                      xlabel=r"$E_T\ \mathrm{[MeV]}$", xlog=True, ylog=True,
                      text=sampleName + " sample\n" +
                      "Downsampled + weighted distribution")

            # Multiclass distribution with weights
            plotHists(data["p_et"], multiMasks, bins,
                      "Plots/" + args.outDir + "/Weighted/EtMultiClass.pdf",
                      weights=weights,
                      xlabel=r"$E_T\ \mathrm{[MeV]}$", xlog=True, ylog=True,
                      text=sampleName + " sample\n" +
                      "Downsampled + weighted distribution")

            ###################################################################
            # Distributions versus eta
            ###################################################################
            bins = np.linspace(-2.47, 2.47, 101)
            # Binary distribution
            plotHists(data["p_eta"], binaryMasks, bins,
                      "Plots/" + args.outDir + "/Downsampled/EtaBinary.pdf",
                      xlabel=r"$\eta$", xlog=False, ylog=True,
                      text=sampleName + " sample\nDownsampled distribution")

            # Multiclass distribution
            plotHists(data["p_eta"], multiMasks, bins,
                      "Plots/" + args.outDir +
                      "/Downsampled/EtaMultiClass.pdf",
                      xlabel=r"$\eta$", xlog=False, ylog=True,
                      text=sampleName + " sample\nDownsampled distribution")

            # Binary distribution
            plotHists(data["p_eta"], binaryMasks, bins,
                      "Plots/" + args.outDir + "/Weighted/EtaBinary.pdf",
                      weights=weights,
                      xlabel=r"$\eta$", xlog=False, ylog=True,
                      text=sampleName + " sample\n" +
                      "Downsampled + weighted distribution")

            # Multiclass distribution
            plotHists(data["p_eta"], multiMasks, bins,
                      "Plots/" + args.outDir +
                      "/Weighted/EtaMultiClass.pdf",
                      weights=weights,
                      xlabel=r"$\eta$", xlog=False, ylog=True,
                      text=sampleName + " sample\n" +
                      "Downsampled + weighted distribution")

        if args.other or args.all:
            mkdir("Plots/" + args.outDir + "/Other/")
            ###################################################################
            # Distributions versus all other input varables
            ###################################################################
            for var in list(set(inputVars) -
                            set(["p_et", "p_eta"])):
                # Binary distributions no range in the plot
                plotHists(data[var], binaryMasks, 100,
                          f"Plots/" + args.outDir + "/Downsampled/" +
                          f"{plotVarDict[var]['fileName']}BinaryNoRange.pdf",
                          xlabel=plotVarDict[var]["plotName"],
                          xlog=plotVarDict[var]["xlog"],
                          ylog=plotVarDict[var]["ylog"],
                          text=sampleName + " sample\n" +
                          "Downsampled distribution")

                # Binary distributions with a set range in the plot
                plotHists(data[var], binaryMasks, plotVarDict[var]["bins"],
                          f"Plots/" + args.outDir + "/Downsampled/" +
                          f"{plotVarDict[var]['fileName']}Binary.pdf",
                          xlabel=plotVarDict[var]["plotName"],
                          xlog=plotVarDict[var]["xlog"],
                          ylog=plotVarDict[var]["ylog"],
                          text=sampleName + " sample\n" +
                          "Downsampled distribution")

                # Multiclass distributions no range in the plot
                plotHists(data[var], multiMasks, 100,
                          f"Plots/" + args.outDir + "/Downsampled/" +
                          f"{plotVarDict[var]['fileName']}" +
                          "MultiClassNoRange.pdf",
                          xlabel=plotVarDict[var]["plotName"],
                          xlog=plotVarDict[var]["xlog"],
                          ylog=plotVarDict[var]["ylog"],
                          text=sampleName + " sample\n" +
                          "Downsampled distribution")

                # Multiclass distributions with a set range in the plot
                plotHists(data[var], multiMasks, plotVarDict[var]["bins"],
                          f"Plots/" + args.outDir + "/Downsampled/" +
                          f"{plotVarDict[var]['fileName']}MultiClass.pdf",
                          xlabel=plotVarDict[var]["plotName"],
                          xlog=plotVarDict[var]["xlog"],
                          ylog=plotVarDict[var]["ylog"],
                          text=sampleName + " sample\n" +
                          "Downsampled distribution")

                # Binary distributions no range in the plot
                plotHists(data[var], binaryMasks, 100,
                          f"Plots/" + args.outDir + "/Weighted/" +
                          f"{plotVarDict[var]['fileName']}BinaryNoRange.pdf",
                          weights=weights,
                          xlabel=plotVarDict[var]["plotName"],
                          xlog=plotVarDict[var]["xlog"],
                          ylog=plotVarDict[var]["ylog"],
                          text=sampleName + " sample\n" +
                          "Downsampled + weighted distribution")

                # Binary distributions with a set range in the plot
                plotHists(data[var], binaryMasks, plotVarDict[var]["bins"],
                          f"Plots/" + args.outDir + "/Weighted/" +
                          f"{plotVarDict[var]['fileName']}Binary.pdf",
                          weights=weights,
                          xlabel=plotVarDict[var]["plotName"],
                          xlog=plotVarDict[var]["xlog"],
                          ylog=plotVarDict[var]["ylog"],
                          text=sampleName + " sample\n" +
                          "Downsampled + weighted distribution")

                # Multiclass distributions no range in the plot
                plotHists(data[var], multiMasks, 100,
                          f"Plots/" + args.outDir + "/Weighted/" +
                          f"{plotVarDict[var]['fileName']}" +
                          "MultiClassNoRange.pdf",
                          weights=weights,
                          xlabel=plotVarDict[var]["plotName"],
                          xlog=plotVarDict[var]["xlog"],
                          ylog=plotVarDict[var]["ylog"],
                          text=sampleName + " sample\n" +
                          "Downsampled + weighted distribution")

                # Multiclass distributions with a set range in the plot
                plotHists(data[var], multiMasks, plotVarDict[var]["bins"],
                          f"Plots/" + args.outDir + "/Weighted/" +
                          f"{plotVarDict[var]['fileName']}MultiClass.pdf",
                          weights=weights,
                          xlabel=plotVarDict[var]["plotName"],
                          xlog=plotVarDict[var]["xlog"],
                          ylog=plotVarDict[var]["ylog"],
                          text=sampleName + " sample\n" +
                          "Downsampled + weighted distribution")


if __name__ == "__main__":
    args = getParser()

    if args.sample == "ZJF":
        sampleName = r"$Z+\mathrm{JF17}$"
    elif args.sample == "Z" or args.sample == "Zee":
        sampleName = r"$Z\rightarrow ee$"
    elif args.sample == "JF" or args.sample == "JF17":
        sampleName = "JF17"
    elif args.sample == "tt" or args.sample == "ttbar":
        sampleName = r"$t\bar{t}$"
    else:
        sampleName = r"$Z+t\bar{t}$"

    plot(args, sampleName)
