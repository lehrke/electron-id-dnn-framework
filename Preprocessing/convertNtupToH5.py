"""Transform a ROOT TTree into an .h5 file."""
import h5py
import uproot
import numpy as np
import argparse


def getParser():
    """Get options set in command line."""
    parser = argparse.ArgumentParser(description="Transform a ROOT TTree " +
                                                 "into an .h5 file")

    parser.add_argument("--outFile", action="store", required=True, type=str,
                        help="Directory where to save the plots.")
    parser.add_argument("--inputFiles", action="store", type=str, nargs="+",
                        help="Root files with trees to transform.")
    parser.add_argument("--treeName", action="store", type=str,
                        help="Name of the tree to transform")
    parser.add_argument("--splitEvtNumber", action="store_true",
                        help="Split the sample based on the event number " +
                             "into train, val, and test set")
    parser.add_argument("--evtNumberVar", action="store", type=str,
                        default="p_evtNumber",
                        help="Name under which event number is saved in tree.")

    args = parser.parse_args()

    return args


def transform(rootFileNames, outFileName, treeName, splitEvtNumber,
              evtNumberVar):
    """
    Load data from ROOT TTree and save it in a .h5 file.

    Args
    ----
        rootFileNames: *list of str*
            List of .root files which should be transformed. All data will be
            saved in one .h5 file.
        outFileName: *str*
            Name of the .h5 file where the data will be saved.
        treeName: *str*
            Name of the ROOT TTree as it is saved in the .root files.
        splitEvtNumber: *bool*
            Whether to split the data into a train, val, and test set based on
            the event number. The splitting is done that events with
            event number % 4 < 2 will be saved in the train set,
            event number % 4 == 2 in the val set, and
            event number % 4 == 3 in the test set,
            such that the split is 50%, 25%, 25%.
        evtNumberVar: *str*
            Name of the branch which holds the event number in the ROOT TTree.

    """
    for i, fileName in enumerate(rootFileNames):
        tree = uproot.open(fileName)[treeName]

        if splitEvtNumber:
            evtNumberModulo = tree.array(evtNumberVar) % 4

            maskSplitting = {"train": evtNumberModulo < 2,
                             "val": evtNumberModulo == 2,
                             "test": evtNumberModulo == 3}
        else:
            maskSplitting = None

        for j, key in enumerate(tree.keys()):
            data = tree.array(key)
            if i == 0:
                saveToH5(outFileName, data, key, j, maskSplitting)
            else:
                appendToH5(outFileName, data, key, maskSplitting)


def saveToH5(outFileName, data, key, keyNumber, maskSplitting):
    """
    Save data to an .h5 file.

    Args
    ----
        outFileName: *str*
            Name of the .h5 file where the data will be saved.
        data: *np.array*
            Data to be saved in the .h5 file
        key: *str*
            Name of the variable which is being saved. The data is saved in a
            dataset with this name.
        keyNumber: *int*
            Integer how often something was already saved into the .h5 file.
            The first time the file has to be created, whereas the other times
            the data needs to be appended.
        maskSplitting: *dict*
            Dictionary containig the names of the different datasets
            (e.g. train, val, test) and masks leaving only the specific
            dataset. If the mask is None, no separate dataset will be created.

    """
    with h5py.File(outFileName, "w" if keyNumber == 0 else "a") as hf:
        key = key.decode("utf-8")
        if maskSplitting is not None:
            for sett in maskSplitting:
                hf.create_dataset(sett + "/" + key,
                                  data=data[maskSplitting[sett] == 1],
                                  chunks=True, maxshape=(None,),
                                  compression="lzf")
        else:
            hf.create_dataset(key,
                              data=data,
                              chunks=True, maxshape=(None,),
                              compression="lzf")


def appendToH5(outFileName, data, key, maskSplitting):
    """
    Append data to an .h5 file if the variable already exists in the .h5 file.

    Args
    ----
        outFileName: *str*
            Name of the .h5 file where the data will be saved.
        data: *np.array*
            Data to be saved in the .h5 file
        key: *str*
            Name of the variable which is being saved. The data is saved in a
            dataset with this name.
        maskSplitting: *dict*
            Dictionary containig the names of the different datasets
            (e.g. train, val, test) and masks leaving only the specific
            dataset. If the mask is None, no separate dataset will be created.

    """
    with h5py.File(outFileName, "a") as hf:
        key = key.decode("utf-8")
        if maskSplitting is not None:
            for sett in maskSplitting:
                nNew = np.sum(maskSplitting[sett] == 1)
                hf[sett + "/" + key].resize(hf[sett + "/" + key].shape[0] +
                                            nNew, axis=0)
                hf[sett + "/" + key][-nNew:] = data[maskSplitting[sett] == 1]
        else:
            nNew = data.shape[0]
            hf[key].resize(hf[key].shape[0] + nNew, axis=0)
            hf[key][-nNew:] = data


if __name__ == "__main__":
    args = getParser()

    transform(args.inputFiles, args.outFile, args.treeName,
              args.splitEvtNumber, args.evtNumberVar)
