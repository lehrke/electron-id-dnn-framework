"""Functions used commonly in the Preprocessing."""
import numpy as np
import matplotlib.pyplot as plt

import sys
sys.path.insert(0, sys.path[0]+"/../../")

from CommonFunctions.functions import (weightedQuantileTransformer,
                                       positionLegend, rcParams)
plt.rcParams.update(rcParams)

inputVars = ["p_numberOfInnermostPixelHits", "p_d0", "p_d0Sig",
             "p_dPOverP", "p_deltaEta1", "p_deltaPhiRescaled2", "p_TRTPID",
             "p_numberOfPixelHits", "p_numberOfSCTHits", "p_EoverP",
             "p_Rhad1", "p_Rhad", "p_f3", "p_f1",
             "p_weta2", "p_Rphi", "p_Reta",  "p_Eratio", "p_wtots1",
             "p_et_calo", "p_etaClusterLr2"]

plotVarDict = {"p_et_calo": {"bins": np.logspace(np.log10(15), np.log10(2000),
                                                 100),
                             "fileName": "Et",
                             "plotName": r"$E_T$ [GeV]",
                             "xlog": True,
                             "ylog": True},
               "p_et": {"bins": np.logspace(np.log10(15000), np.log10(2000000),
                                            100),
                        "fileName": "Et",
                        "plotName": r"$E_T$ [MeV]",
                        "xlog": True,
                        "ylog": True},
               "p_etaClusterLr2": {"bins": np.linspace(-2.47, 2.47, 100),
                                   "fileName": "Eta",
                                   "plotName": r"$\eta$",
                                   "xlog": False,
                                   "ylog": True},
               "p_eta": {"bins": np.linspace(-2.47, 2.47, 100),
                         "fileName": "Eta",
                         "plotName": r"$\eta$",
                         "xlog": False,
                         "ylog": True},
               "p_numberOfInnermostPixelHits": {"bins": np.linspace(0, 4, 5),
                                                "fileName": "nBlayer",
                                                "plotName": "nBlayer",
                                                "xlog": False,
                                                "ylog": True},
               "p_numberOfPixelHits": {"bins": np.linspace(0, 11, 12),
                                       "fileName": "nPix",
                                       "plotName": "nPix",
                                       "xlog": False,
                                       "ylog": True},
               "p_numberOfSCTHits": {"bins": np.linspace(0, 21, 22),
                                     "fileName": "nSCT",
                                     "plotName": "nSCT",
                                     "xlog": False,
                                     "ylog": True},
               "p_d0": {"bins": np.linspace(-0.5, 0.5, 101),
                        "fileName": "d0",
                        "plotName": r"$d_0$",
                        "xlog": False,
                        "ylog": True},
               "p_d0Sig": {"bins": np.linspace(0, 10, 101),
                           "fileName": "d0Sig",
                           "plotName": r"$|\frac{d_0}{\sigma(d_0)}|$",
                           "xlog": False,
                           "ylog": True},
               "p_dPOverP": {"bins": np.linspace(-1.2, 1.2, 101),
                             "fileName": "dPoverP",
                             "plotName": r"$\frac{dP}{P}$",
                             "xlog": False,
                             "ylog": True},
               "p_deltaEta1": {"bins": np.linspace(-0.01, 0.01, 101),
                               "fileName": "deltaEta1",
                               "plotName": r"$\Delta\eta_1$",
                               "xlog": False,
                               "ylog": True},
               "p_deltaPhiRescaled2": {"bins": np.linspace(-0.03, 0.03, 101),
                                       "fileName": "deltaPhiREscaled2",
                                       "plotName": r"$\Delta\phi_{2,res}$",
                                       "xlog": False,
                                       "ylog": True},
               "p_TRTPID": {"bins": np.linspace(-1, 1, 101),
                            "fileName": "TRTPID",
                            "plotName": "TRTPID",
                            "xlog": False,
                            "ylog": True},
               "p_EoverP": {"bins": np.linspace(0, 2, 101),
                            "fileName": "EoverP",
                            "plotName": r"$\frac{E}{P}$",
                            "xlog": False,
                            "ylog": True},
               "p_Rhad1": {"bins": np.linspace(-0.05, 0.05, 101),
                           "fileName": "Rhad1",
                           "plotName": "Rhad1",
                           "xlog": False,
                           "ylog": True},
               "p_Rhad": {"bins": np.linspace(-0.05, 0.05, 101),
                          "fileName": "Rhad",
                          "plotName": "Rhad",
                          "xlog": False,
                          "ylog": True},
               "p_f3": {"bins": np.linspace(-0.05, 0.15, 101),
                        "fileName": "f3",
                        "plotName": r"$f_3$",
                        "xlog": False,
                        "ylog": True},
               "p_f1": {"bins": np.linspace(-0.02, 0.7, 101),
                        "fileName": "f1",
                        "plotName": r"$f_1$",
                        "xlog": False,
                        "ylog": True},
               "p_weta2": {"bins": np.linspace(0.005, 0.02, 101),
                           "fileName": "weta2",
                           "plotName": r"$w_{\eta 2}$",
                           "xlog": False,
                           "ylog": True},
               "p_Rphi": {"bins": np.linspace(0.45, 1.05, 101),
                          "fileName": "Rphi",
                          "plotName": r"$R_\phi$",
                          "xlog": False,
                          "ylog": True},
               "p_Reta": {"bins": np.linspace(0.8, 1.1, 101),
                          "fileName": "Reta",
                          "plotName": r"$R_\eta$",
                          "xlog": False,
                          "ylog": True},
               "p_Eratio": {"bins": np.linspace(0.50, 1.05, 101),
                            "fileName": "Eratio",
                            "plotName": "Eratio",
                            "xlog": False,
                            "ylog": True},
               "p_wtots1": {"bins": np.linspace(0, 8, 101),
                            "fileName": "wtots1",
                            "plotName": "wtots1",
                            "xlog": False,
                            "ylog": True},
               "NvtxReco": {"bins": np.linspace(0, 80, 81),
                            "fileName": "NvtxReco",
                            "plotName": "NvtxReco",
                            "xlog": False,
                            "ylog": True},
               "p_nvtx": {"bins": np.linspace(0, 80, 81),
                          "fileName": "NvtxReco",
                          "plotName": "NvtxReco",
                          "xlog": False,
                          "ylog": True},
               "p_mu": {"bins": np.linspace(0, 100, 101),
                        "fileName": "Mu",
                        "plotName": r"$<\mu>$",
                        "xlog": False,
                        "ylog": True},
               "p_numberOfBLayerHits": {"bins": np.linspace(0, 4, 5),
                                        "fileName": "nBlayer",
                                        "plotName": "nBlayer",
                                        "xlog": False,
                                        "ylog": True},
               "p_nPixelHits": {"bins": np.linspace(0, 11, 12),
                                "fileName": "nPix",
                                "plotName": "nPix",
                                "xlog": False,
                                "ylog": True},
               "p_nSCTHits": {"bins": np.linspace(0, 21, 22),
                              "fileName": "nSCT",
                              "plotName": "nSCT",
                              "xlog": False,
                              "ylog": True},
               "p_dnnValue": {"bins": np.linspace(-5, 5, 100),
                              "fileName": "dnnScore",
                              "plotName": "DNN Score",
                              "xlog": False,
                              "ylog": True}}


def plotHists(data, masks, bins, outName, weights=None, xlabel="", ylabel="",
              xlog=False, ylog=False, mask=None, text=None, norm=False):
    """
    Plot a histogram of the data making one histogram for each mask.

    Args
    ----
        data : *array*
            Array of the data to be plotted in histograms
        masks : *dict*
            Dictionary holding all masks which should be used. The key of each
            mask will be used as a label in the legend of the plot.
        bins : *array* or *int*
            Bins to be used for the histograms. If an array then this values
            will be used as bins. If int the full range of the data array will
            be divied into the given number of bins .
        outName : *str*
            Name of the file where the plot will be saved.
        weights : *array*
            Default=None; Weights to be used during the plotting. If None, no
            weights will be used.
        xlabel : *str*
            Default=""; Label to use for the x-axis.
        ylabel : *str*
            Default=""; Label to use for the y-axis.
        xlog : *bool*
            Default=False; Whether to plot the x-axis on a log-scale.
        ylog : *bool*
            Default=False; Whether to plot the y-axis on a log-scale.
        mask : numpy.array
            Mask portions of the data. E.g. plot only one bkg class.
        text : *str*
            Add text to the top left of the plot.
        norm : *bool*
            Normalize all histograms to compare the shape more easily.

    """
    if mask is not None:
        q = weightedQuantileTransformer(data[mask == 1].reshape(-1, 1),
                                        np.linspace(0, 1, 1000),
                                        weights[mask == 1])
        data = q.transform(data.reshape(-1, 1))

    if type(bins) == int:
        histRange = (np.min(data), np.max(data))
    else:
        histRange = None
    fig, ax = plt.subplots()
    for key in masks:
        if weights is None:
            ax.hist(data[masks[key] == 1],
                    range=histRange, bins=bins, label=key,
                    histtype="step", density=norm)
        else:
            ax.hist(data[masks[key] == 1], weights=weights[masks[key] == 1],
                    range=histRange, bins=bins, label=key,
                    histtype="step", density=norm)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    if xlog:
        ax.set_xscale("log")
    if ylog:
        ax.set_yscale("log")
    if text is not None:
        ax.text(0.02, 0.98, text, transform=ax.transAxes,
                verticalalignment='top')

    plt.tight_layout()
    positionLegend(fig, ax)
    plt.savefig(outName)
    plt.close("all")
