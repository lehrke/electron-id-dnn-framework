# Overview  

Starting from some number of flat nTuples.
convertNtupToH5.py uses uproot to transform the nTuples into .h5 files.
The data will be split into a train, val, and test set based on the event number of the candidate.
Multiple nTuples can be combined into one .h5 file.
With makeVirtualDataset.py single files can be combined into one which makes reading easier.
The data is not replicated but it is done via some sort of linking.
This can be used to have .h5 files for different processes separately and combine them only via these virtual datasets
makeInputFilePlots.py makes plots for all input variables (defined in the script) of the distribution in a binary and 6 class case. For Et and eta special plots to see the contribution of different MC samples to each of the six classes are made in addition.
downsampling.py donwsamples the distributions of the 5 classes to a specified number in ins of Et and eta. This probably needs some input from the plots made.
weighting.py takes the downsampled data and reweights it to a predefined shape again in bins of Et and eta.
weighting.py reweights the
functions.py contains functions which are used across multiple of these scripts.

# Scripts  

### convertNtupToH5.py
##### Args:


### makeVirtualDataset.py
##### Args:
--outFile Path to the file which should hold the virtual datasets. Will be created while running the script  
--inputFiles Paths to the files which should be used in the virtual datasets. Absolute paths should be used. Files should be separated by spaces. Placeholder like * do work.

Note: The input files have to be reachable with the path given while creating the combined h5 file. This means absolute paths should be used such that the file can be properly read across the whole system. If you use a cluster and have it mounted to use it locally, you would need two files, one for using locally and one for using on the cluster.


### downsampling.py
##### Args:
--logDir Where to save the mask and then later on the weights as well  
--inputFile path to the file with the virtual datasets
--nTrainPoints number of maximal data points in each of the et-eta bins for the train set (this should be decided by looking at the plots beforehand and potentially also need some tuning)
--nValPoints number of maximal data points in each of the et-eta bins for the val set (this should be decided by looking at the plots beforehand and potentially also need some tuning)
--makePlots will make plots of downsampled et and eta distribution in the logdir of the val set

Note: The test set is not downsampled or weighted


### weighting.py
##### Args:
--logDir Where to load the mask from and where to save the weights
--inputFile path to the file with the virtual datasets
--makePlots will make plots of downsampled and weighted et and eta distribution in the logdir of the val set


### makeInputFilePlots.py
##### Args:
--inputFile Which h5 file to use for the plotting (outFile from makeVirtualDataset.py)
--kinematic Make plots for the kinematic variables (et, eta)
--other Make plots for all remaining input variables
--all Make plots for all input variables. Combines --kinematic and --other
