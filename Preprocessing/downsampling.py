"""Downsample one file to the same distribution in Et, eta for sig and bkg."""
import numpy as np
import h5py
import argparse

import sys
sys.path.insert(0, sys.path[0]+"/../")
from CommonFunctions.functions import mkdir
from Preprocessing.functions.functions import plotHists


def getParser():
    """Get options set in command line."""
    parser = argparse.ArgumentParser(description="Options for downsampling")

    parser.add_argument("--logDir", action="store", type=str,
                        default="test/",
                        help="Save masks for the downsampling in this " +
                             "directory and make plots of the downsampled " +
                             "distributions in Et and eta")
    parser.add_argument("--inputFile", action="store", type=str,
                        default="VDSLocal.h5",
                        help="Input file which should be downsampled.")
    parser.add_argument("--nTrainPoints", action="store", type=int,
                        default=int(1e4), help="Number of maximal train " +
                                               "points in each et-eta bin")
    parser.add_argument("--nValPoints", action="store", type=int,
                        default=int(5e3), help="Number of maximal val " +
                                               "points in each et-eta bin")
    parser.add_argument("--makePlots", action="store_true",
                        help="Make plots of downsampled Et and eta " +
                             "distributions for the validation set")
    parser.add_argument("--ttbar", action="store_true",
                        help="Downsample signal to match bkg. If not given " +
                             "signal will be downsampled to five times the " +
                             "bkg to keep more statistics.")

    args = parser.parse_args()

    return args


def simpleDownsample(fileName, sett, etBins, ttbar):
    """
    Downsamples signal to not have (5x) more events in one Et bin than bkg.

    Args
    ----
        fileName: *str*
            Name of the .h5 file where the data is stored.
        sett: *str*
            Which dataset to downsample: train, val, or test
        etBins: numpy.array
            Bins in which the signal should be downsampled to the bkg.
        ttbar: *bool*
            Whether to downsample signal to not have more events than bkg
            (if True) or to not have more events than five times the bkg
            (if False).

    """
    factor = 1 if ttbar else 5
    # Load the necessary data
    with h5py.File(fileName, "r") as hf:
        et = hf[f"{sett}/p_et"][:] / 1e3
        truth = hf[f"{sett}/p_signal"][:]
        iff = hf[f"{sett}/p_iff"][:]

    # Get bin numbers for the data
    binNumbersEt = np.digitize(et, etBins) - 1

    indices = []

    for i in range(len(etBins) - 1):
        # Get mask for signal and background
        masks = {"Signal": ((binNumbersEt == i) & (truth == 1)),
                 "Background": ((binNumbersEt == i) & (truth == 0)) &
                               ((iff != 7) & (iff != 0) & (iff != 1) &
                                (iff != 4) & (iff != 6) & (iff != 3) &
                                (iff != 2))}

        # Get indices of Signal and Background
        sigIndices = np.argwhere(masks["Signal"] == 1).flatten()
        bkgIndices = np.argwhere((masks["Background"] == 1)).flatten()

        # Get Number of Signal and Background events
        nSig = len(sigIndices)
        nBkg = len(bkgIndices)

        # If there is less signal than five times the background, keep all data
        if nSig < factor * nBkg:
            sigRandom = np.sort(np.random.choice(sigIndices, nSig,
                                                 replace=False))
            bkgRandom = np.sort(np.random.choice(bkgIndices, nBkg,
                                                 replace=False))
        # Else remove signal that there is five times as much signal as bkg
        else:
            sigRandom = np.sort(np.random.choice(sigIndices, factor * nBkg,
                                                 replace=False))
            bkgRandom = np.sort(np.random.choice(bkgIndices, nBkg,
                                                 replace=False))
        # Collect signal and background indices to keep
        indices += list(sigRandom)
        indices += list(bkgRandom)

    # Create mask to keep only the wanted events
    mask = np.zeros(et.shape[0], dtype=np.int)
    mask[np.sort(indices)] = 1

    return mask


def downsample(fileName, nPoints, sett, etBins, etaBins):
    """
    Downsample data to lowest component in bins of eta and Et.

    Not used at the moment. "simpleDownsample" works better.

    Args
    ----
        fileName : *str*
            Path to the file which should be downsampled.
        nPoints: *int*
            Number that the hists will be downsampled to.
        sett : *str*
            Dataset which should be downsampled (train, val, test)
        etBins : *array*
            Array containing the bin edges of the bins in Et.
        etaBins : *array*
            Array containing the bin edges of the bins in eta.

    Returns
    -------
        mask : *array*
            Mask which applies the retireved downsampling.

    """
    # Load the necessary data
    with h5py.File(fileName, "r") as hf:
        et = hf[f"{sett}/p_et_calo"][:]
        eta = np.abs(hf[f"{sett}/p_eta"][:])
        iff = hf[f"{sett}/p_iffTruth"][:]
        tt = hf[f"{sett}/p_truthType"][:]
        signal = hf[f"{sett}/p_signal"][:]

    # Get bin numbers for the data
    binNumbersEt = np.digitize(et, etBins) - 1
    binNumbersEta = np.digitize(eta, etaBins) - 1

    indices = []

    for i in range(len(etBins) - 1):
        for j in range(len(etaBins) - 1):
            # Get masks for each of the six classes in the specific et-eta bin
            masks = {"Signal": ((binNumbersEt == i) & (binNumbersEta == j)
                                & (signal == 1)),
                     "PhotonConv": ((binNumbersEt == i) & (binNumbersEta == j)
                                    & (iff == 5)),
                     "HeavyFlavor": ((binNumbersEt == i) & (binNumbersEta == j)
                                     & ((iff == 8) | (iff == 9))),
                     "LFEgamma": ((binNumbersEt == i) & (binNumbersEta == j)
                                  & ((iff == 10) & ((tt == 4) | (tt == 16)))),
                     "LFHadron": ((binNumbersEt == i) & (binNumbersEta == j)
                                  & ((iff == 10) & (tt == 17)))}

            nParticles = int(nPoints)

            # Iterate over the defined masks
            for key in masks:
                # if there are less than the required number of particles,
                # keep all
                if np.sum(masks[key]) < nParticles:
                    indices += list(np.argwhere(masks[key] == 1).flatten())
                # Randomly select nParticles of the data points
                else:
                    ind = np.argwhere(masks[key] == 1).flatten()
                    randomInd = np.sort(np.random.choice(ind,
                                                         nParticles,
                                                         replace=False))

                    indices += list(randomInd)

    mask = np.zeros(et.shape[0], dtype=np.int)
    mask[np.sort(indices)] = 1

    return mask


if __name__ == "__main__":
    args = getParser()

    mkdir(args.logDir)
    # Do not downsample test set to keep full statistics
    for sett in ["train", "val"]:
        # Get the mask and save it
        etBins = np.concatenate((np.logspace(np.log10(15), np.log10(600), 100),
                                 [5000]))
        mask = simpleDownsample(args.inputFile, sett, etBins, args.ttbar)

        np.save(args.logDir + f"/downsamplingMask{sett}.npy", mask)

        # if desired make plots of downsampled eta and et distributions
        if args.makePlots and sett == "val":
            mkdir(args.logDir + "/Plots/")

            data = {}
            tapData = True  # TODO some way to set this option or automated
            if tapData:
                with h5py.File(args.inputFile, "r") as hf:
                    data["p_iff"] = hf["val/p_iff"][:]
                    data["p_truthType"] = hf["val/p_truthType"][:]
                    data["p_truth"] = hf["val/p_signal"][:]
                    data["p_eta"] = hf["val/p_eta"][:]
                    data["p_et"] = hf["val/p_et"][:]
            else:
                with h5py.File(args.inputFile, "r") as hf:
                    data["p_iff"] = hf["val/p_iffTruth"][:]
                    data["p_truthType"] = hf["val/p_truthType"][:]
                    data["p_truth"] = hf["val/p_truth"][:]
                    data["p_eta"] = hf["val/p_etaClusterLr2"][:]
                    data["p_et"] = hf["val/p_et_calo"][:]

            mask = np.load(args.logDir + f"/downsamplingMask{sett}.npy")

            binaryMasks = {"Signal": (data["p_truth"] == 1) & (mask == 1),
                           "Background": (data["p_truth"] == 0) & (mask == 1)}

            multiMasks = {
                          "Signal": (data["p_truth"] == 1) & (mask == 1),
                          "PhotonConv": (data["p_iff"] == 5) &
                                        (mask == 1),
                          "HeavyFlavor": ((data["p_iff"] == 8) |
                                          (data["p_iff"] == 9)) &
                                         (mask == 1),
                          "LFEgamma": (((data["p_iff"] == 10) &
                                        ((data["p_truthType"] == 4) |
                                         (data["p_truthType"] == 16))) &
                                       (mask == 1)),
                          "LFHadron": (((data["p_iff"] == 10) &
                                        (data["p_truthType"] == 17)) &
                                       (mask == 1))
                         }

            ###################################################################
            # Distributions versus ET
            ###################################################################
            bins = np.logspace(np.log10(15), np.log10(2000), 101)
            # Binary distribution
            plotHists(data["p_et"]/1000., binaryMasks, bins,
                      args.logDir + "/Plots/EtBinaryDownsampled.pdf",
                      xlabel=r"$E_T\ \mathrm{[GeV]}$", xlog=True, ylog=True)

            # Multiclass distribution
            plotHists(data["p_et"]/1000, multiMasks, bins,
                      args.logDir + "/Plots/EtMultiClassDownsampled.pdf",
                      xlabel=r"$E_T\ \mathrm{[GeV]}$", xlog=True, ylog=True)

            ###################################################################
            # Distributions versus eta
            ###################################################################
            bins = np.linspace(-2.47, 2.47, 101)
            # Binary distribution
            plotHists(data["p_eta"], binaryMasks, bins,
                      args.logDir + "/Plots/EtaBinaryDownsampled.pdf",
                      xlabel=r"$\eta$", xlog=False, ylog=True)

            # Multiclass distribution
            plotHists(data["p_eta"], multiMasks, bins,
                      args.logDir + "/Plots/EtaMultiClassDownsampled.pdf",
                      xlabel=r"$\eta$", xlog=False, ylog=True)
