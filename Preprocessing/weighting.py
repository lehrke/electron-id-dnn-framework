"""Reweight the downsampled eta and et distributions to a desired shape."""
import numpy as np
import h5py
import argparse
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import json

import sys
sys.path.insert(0, sys.path[0]+"/../")
from CommonFunctions.functions import mkdir
from Preprocessing.functions.functions import plotHists


def getParser():
    """Get options set in command line."""
    parser = argparse.ArgumentParser(description="Options for reweighting")

    parser.add_argument("--logDir", action="store", type=str,
                        default="test/",
                        help="Save masks for the downsampling in this " +
                             "directory and make plots of the downsampled " +
                             "distributions in Et and eta")
    parser.add_argument("--inputFile", action="store", type=str,
                        default="VDSLocal.h5",
                        help="Input file which should be downsampled.")
    parser.add_argument("--makePlots", action="store_true",
                        help="Make plots of downsampled Et and eta " +
                             "distributions for the test set")

    args = parser.parse_args()

    return args


def getClassWeights(fileName, sett, downsamplingMask, weights):
    """
    Get weights for each class such that all have the same sum of weights.

    Args
    ----
        fileName: *str*
            Name of the .h5 file where the data is stored.
        sett: *str*
            Which dataset to downsample: train, val, or test
        downsamplingMask: numpy.array
            Array masking all events that are already downsampled since the
            class weights should make classes have the same sum of weights
            after downsampling and per sample weights.
        weights: numpy.array
            Array giving the per sample weights since the
            class weights should make classes have the same sum of weights
            after downsampling and per sample weights.

    Returns
    -------
        classWeights: *dict*
            Dictionary holding class weights for the single classes. The
            classes are encoded as integers as they are in a multi class
            training.

    """
    with h5py.File(fileName, "r") as hf:
        iff = hf[f"{sett}/p_iff"][:]
        tt = hf[f"{sett}/p_truthType"][:]
        signal = hf[f"{sett}/p_signal"][:]
    classMasks = {"Signal": (signal == 1) &
                            (downsamplingMask == 1),
                  "PhotonConv": (iff == 5) & (downsamplingMask == 1),
                  "HeavyFlavor": ((iff == 8) | (iff == 9)) &
                                 (downsamplingMask == 1),
                  "LFEgamma": (((iff == 10) & ((tt == 4) | (tt == 16))) &
                               (downsamplingMask == 1)),
                  "LFHadron": (((iff == 10) & (tt == 17)) &
                               (downsamplingMask == 1))}
    norm = np.sum(weights[classMasks["Signal"] == 1])
    classWeights = {}
    classWeights[0] = 1
    classWeights[1] = norm / np.sum(weights[classMasks["PhotonConv"] == 1])
    classWeights[2] = norm / np.sum(weights[classMasks["HeavyFlavor"] == 1])
    classWeights[3] = norm / np.sum(weights[classMasks["LFEgamma"] == 1])
    classWeights[4] = norm / np.sum(weights[classMasks["LFHadron"] == 1])

    return classWeights


def sameShapeReweight(fileName, sett, etaBins, downsamplingMask):
    """
    Reweights each of the single classes to a common shape in Et and eta.

    The Et shape is taken from LFHadron bkg from a ttbar sample and is given
    as a histogram below. Therefore, if the binning in Et is changed, this
    histogram would need to change as well.
    The eta shape is just flat.

    Args
    ----
        fileName: *str*
            Name of the .h5 file where the data is stored.
        sett: *str*
            Which dataset to downsample: train, val, or test
        etaBins: numpy.array
            Bins in eta which are used to calcuate the weights.
        downsamplingMask: numpy.array
            Array masking all events that are already downsampled.

    Returns
    -------
        fullW: numpy.array
            Calculated weights to give desired shape in Et and eta
        description: *str*
            Short description of what the reweighting does.

    """
    with h5py.File(fileName, "r") as hf:
        et = hf[f"{sett}/p_et"][:] / 1000.
        eta = np.abs(hf[f"{sett}/p_eta"][:])
        iff = hf[f"{sett}/p_iff"][:]
        tt = hf[f"{sett}/p_truthType"][:]
        signal = hf[f"{sett}/p_signal"][:]

    classMasks = {"Sig": (signal == 1) & (downsamplingMask == 1),
                  "Phot": (iff == 5) & (downsamplingMask == 1),
                  "HF": ((iff == 8) | (iff == 9)) & (downsamplingMask == 1),
                  "LFEg": (((iff == 10) & ((tt == 4) | (tt == 16))) &
                           (downsamplingMask == 1)),
                  "LFH": (((iff == 10) & (tt == 17)) &
                          (downsamplingMask == 1))}

    # Number when making a histogram with ttbar LFHadron bkg. This shape seems
    # to work well for the reweighting
    refHist = np.array([1543958, 1499303, 1461237, 1413642, 1363050, 1324721,
                        1279264, 1240566, 1197661, 1154568, 1118529, 1079636,
                        1044889, 1016922, 976302,  938869,  905265,  868922,
                        836080,  802221,  773836,  743877,  715094,  685599,
                        657916,  630583,  605153,  579086,  554333,  529842,
                        508252,  486086,  463719,  441392,  418490,  396858,
                        379554,  358975,  339334,  322651,  304711,  287090,
                        271153,  255308,  240368,  225297,  210807,  198104,
                        186165,  172923,  161375,  150793,  139384,  130225,
                        119802,  112290,  104062,  95995,   87871,   81528,
                        74778,   68463,   63112,   57816,   52637,   48022,
                        43532,   39758,   36297,   33226,   30173,   27271,
                        24830,   22694,   20221,   18587,   16773,   15101,
                        13594,   12182,   10908,   9798,    8948,    8027,
                        7220,    6203,    5733,    5161,    4462,    3958,
                        3553,    3148,    2703,    2495,    2149,    1923,
                        1747,    1490,    1281,    8304])

    etBins = np.concatenate((np.logspace(np.log10(15), np.log10(400), 100),
                             [2000]))

    weightHists = {}
    for key in classMasks:
        weightHists[key] = refHist / np.histogram(et[classMasks[key] == 1],
                                                  bins=etBins)[0]

    weights = np.ones(et.shape)

    # Assign each data point the weight of its corresponding et-eta bin
    for key in classMasks:
        for i in range(len(etBins) - 1):
            binMask = ((et > etBins[i]) & (et <= etBins[i+1]))
            weights[(classMasks[key] == 1) &
                    (binMask == 1)] = weightHists[key][i]

        weights[classMasks[key] == 1] /= np.mean(weights[classMasks[key] == 1])

    etaHist = np.ones(etaBins.shape[0] - 1)

    fullW = np.ones(et.shape)

    weightHists = {}
    for key in classMasks:
        weightHists[key] = etaHist / np.histogram(eta[classMasks[key] == 1],
                                                  bins=etaBins)[0]

    for key in classMasks:
        for i in range(len(etaBins) - 1):
            binMask = ((eta > etaBins[i]) & (eta <= etaBins[i+1]))
            fullW[(classMasks[key] == 1) &
                  (binMask == 1)] = (weightHists[key][i] *
                                     weights[(classMasks[key] == 1) &
                                             (binMask == 1)])

        fullW[(np.isnan(fullW)) | (np.isinf(fullW))] = 0
        fullW[classMasks[key] == 1] /= np.mean(fullW[classMasks[key] == 1])

    fullW[classMasks["Sig"] == 1] *= ((np.sum(fullW[classMasks["Phot"] == 1]) +
                                       np.sum(fullW[classMasks["HF"] == 1]) +
                                       np.sum(fullW[classMasks["LFEg"] == 1]) +
                                       np.sum(fullW[classMasks["LFH"] == 1])) /
                                      np.sum(fullW[classMasks["Sig"] == 1]))

    # Normalize the weights of the downsampled weights
    fullW /= np.mean(fullW[downsamplingMask == 1])

    return fullW, "et: LFHadron of ttbar sample, eta: flat"


def simpleReweight(fileName, sett, etBins, etaBins, downsamplingMask):
    """
    Reweights Signal to match Bkg in Et and eta.

    Not used at the moment. Use "sameShapeReweight".

    Args
    ----
        fileName: *str*
            Name of the .h5 file where the data is stored.
        sett: *str*
            Which dataset to downsample: train, val, or test.
        etBins: numpy.array
            Bins in Et which are used to calcuate the weights.
        etaBins: numpy.array
            Bins in eta which are used to calcuate the weights.
        downsamplingMask: numpy.array
            Array masking all events that are already downsampled.

    Returns
    -------
        fullWeights: numpy.array
            Calculated weights to give desired shape in Et and eta
        description: *str*
            Short description of what the reweighting does.

    """
    with h5py.File(fileName, "r") as hf:
        et = hf[f"{sett}/p_et"][:] / 1e3
        eta = np.abs(hf[f"{sett}/p_eta"][:])
        truth = hf[f"{sett}/p_signal"][:]

    fig, ax = plt.subplots()
    sigHist = ax.hist(et[(truth == 1) & (downsamplingMask == 1)],
                      bins=etBins)[0]
    bkgHist = ax.hist(et[(truth == 0) & (downsamplingMask == 1)],
                      bins=etBins)[0]
    plt.close("all")

    scaling = bkgHist / sigHist

    weights = np.ones(et.shape)

    # Assign each data point the weight of its corresponding et-eta bin
    for i in range(len(etBins) - 1):
        binMask = ((et > etBins[i]) & (et <= etBins[i+1]))

        weights[(truth == 1) & (binMask == 1)] = scaling[i]

    # Normalize the weights of the downsampled weights
    weights /= np.mean(weights[downsamplingMask == 1])

    fig, ax = plt.subplots()
    sigHist = ax.hist(eta[(truth == 1) & (downsamplingMask == 1)],
                      weights=weights[(truth == 1) & (downsamplingMask == 1)],
                      bins=etaBins)[0]
    bkgHist = ax.hist(eta[(truth == 0) & (downsamplingMask == 1)],
                      weights=weights[(truth == 0) & (downsamplingMask == 1)],
                      bins=etaBins)[0]
    plt.close("all")

    scaling = sigHist / bkgHist
    bkgScaling = bkgHist / sigHist

    fullWeights = np.ones(et.shape)

    # Assign each data point the weight of its corresponding et-eta bin
    for i in range(len(etaBins) - 1):
        binMask = ((eta > etaBins[i]) & (eta <= etaBins[i+1]))

        if etaBins[i + 1] < 1:
            fullWeights[(truth == 1) &
                        (binMask == 1)] = (weights[(truth == 1) &
                                                   (binMask == 1)] *
                                           bkgScaling[i])

            fullWeights[(truth == 0) &
                        (binMask == 1)] = weights[(truth == 0) &
                                                  (binMask == 1)]
        else:
            fullWeights[(truth == 0) &
                        (binMask == 1)] = weights[(truth == 0) &
                                                  (binMask == 1)] * scaling[i]

            fullWeights[(truth == 1) &
                        (binMask == 1)] = weights[(truth == 1) &
                                                  (binMask == 1)]
    # Normalize the weights of the downsampled weights
    fullWeights /= np.mean(fullWeights[downsamplingMask == 1])

    # Get a brief description of the function used for the reweighting
    description = "Signal reweighted to match bkg, first in et then in eta"

    return fullWeights, description


def functionalForm(et, eta):
    """
    Return an array following the shape the et-eta distribution should follow.

    Not used at the moment. Use "sameShapeReweight".

    Args
    ----
        et : *array*
            Et coordinates
        eta : *array*
            Eta coordinates

    Returns
    -------
        out : *array*
            Values which follow the shape given the et and eta coordinates
        description : *str*
            Brief description of the shape used.

    """
    description = "eta: flat, et: slowly rising until 7 GeV, flat until " + \
                  "100 GeV, slowly falling until end."
    out = np.ones(et.shape[0])
    out[et < 7] = et[et < 7] / 7.
    out[(et >= 7) & (et < 100)] = 1. + 0 * et[(et >= 7) & (et < 100)]
    out[et >= 100] = -0.5/1900 * et[et >= 100] + 0.5/1900 * 100 + 1
    return out, description


def reweight(fileName, sett, etBins, etaBins, downsamplingMask):
    """
    Calculate the weights for each data point after downsampling.

    Not used at the moment. Use "sameShapeReweight".

    Args
    ----
        fileName : *str*
            Input h5 file which contains the data
        sett : *str*
            train, val, or test set.
        etBins : *array*
            Bins in et which are used for the reweighting.
        etaBins : *array*
            Bins in eta which are used for the reweighting.
        downsamplingMask : *array*
            Mask for the downsampling.

    Returns
    -------
        weights : *array*
            weights for all data points, downsampled ones as well as not
            downsampled ones, which all receive a weight of one (different at
            the end since weights are normalized).
        description : *str*
            Short string which gives a brief description of how the result of
            the reweightihng should look like.

    """
    # Load data and make masks for different classes (only 5 since signal and
    # chargeFlip are combined for binary classification)
    with h5py.File(fileName, "r") as hf:
        et = hf[f"{sett}/p_et_calo"][:]
        eta = hf[f"{sett}/p_etaClusterLr2"][:]
        iff = hf[f"{sett}/p_iffTruth"][:]
        tt = hf[f"{sett}/p_truthType"][:]

    classMasks = {"Signal": ((iff == 2) | (iff == 3)) &
                            (downsamplingMask == 1),
                  "PhotonConv": (iff == 5) & (downsamplingMask == 1),
                  "HeavyFlavor": ((iff == 8) | (iff == 9)) &
                                 (downsamplingMask == 1),
                  "LFEgamma": (((iff == 10) & ((tt == 4) | (tt == 16))) &
                               (downsamplingMask == 1)),
                  "LFHadron": (((iff == 10) & (tt == 17)) &
                               (downsamplingMask == 1))}

    # Make 2d hists for all classes
    hists = {}
    fig, ax = plt.subplots()
    for key in classMasks:
        hists[key] = ax.hist2d(et[classMasks[key]],
                               eta[classMasks[key]],
                               bins=(etBins, etaBins))[0]
    plt.close("all")

    # Create a 2d hist for the shape that is desired (given by a function)
    etBinCenters = (etBins[1:] + etBins[:-1]) / 2.
    refHist = np.ones((etBins.shape[0] - 1, etaBins.shape[0] - 1))
    for i in range(etaBins.shape[0] - 1):
        refHist[:, i], _ = functionalForm(etBinCenters,
                                          np.zeros(etaBins.shape[0] - 1))

    # Scale the reference histogram such that the mean of the weights should
    # be close to one
    scale = np.sum(hists["Signal"]) / np.sum(refHist)
    refHist *= scale
    bkgRefHist = refHist / 4.
    weightsHists = {}
    # Get the weights in each bin by dividing the hists
    for key in hists:
        if key == "Signal":
            weightsHists[key] = refHist / hists[key]
        else:
            weightsHists[key] = bkgRefHist / hists[key]

    weights = np.ones(et.shape)

    # Assign each data point the weight of its corresponding et-eta bin
    for i in range(len(etBins) - 1):
        for j in range(len(etaBins) - 1):
            binMask = ((et > etBins[i]) & (et <= etBins[i+1]) &
                       (eta > etaBins[j]) & (eta <= etaBins[j+1]))

            for key in classMasks:
                weights[(classMasks[key] == 1) &
                        (binMask == 1)] = weightsHists[key][i][j]

    # Normalize the weights of the downsampled weights
    weights /= np.mean(weights[downsamplingMask == 1])

    # Get a brief description of the function used for the reweighting
    _, description = functionalForm(np.array([10]), np.array([10]))

    return weights, description


if __name__ == "__main__":
    args = getParser()

    # etBins = np.logspace(np.log10(4.5), np.log10(2000), 40)
    # etaBins = np.linspace(-2.47, 2.47, 20)
    etBins = np.concatenate((np.logspace(np.log10(15), np.log10(400), 100),
                             [2000]))
    etaBins = np.linspace(0, 2.47, 50)

    for sett in ["train", "val"]:
        # load the mask for downsampling since we want to reweight the
        # downsampled distribution
        downsamplingMask = np.load(args.logDir +
                                   f"/downsamplingMask{sett}.npy")

        # Calculate actual weights and save them
        weights, description = sameShapeReweight(args.inputFile, sett,
                                                 etaBins, downsamplingMask)

        np.save(args.logDir + f"/downsamplingWeights{sett}.npy", weights)

        # Save some information of where the data comes from and what king of
        # reweighting was used
        if sett == "train":
            classWeights = getClassWeights(args.inputFile, sett,
                                           downsamplingMask, weights)
            metadata = {"Originalh5File": args.inputFile,
                        "descriptionWeights": description,
                        "classWeights": classWeights}
            json.dump(metadata, open(args.logDir + "/metaData.json", "w"),
                      indent=4)

        # if desired make plots of downsampled eta and et distributions
        if args.makePlots and sett == "val":
            mkdir(args.logDir + "/Plots/")

            data = {}
            tapData = True  # TODO some way to set this option or automated
            if tapData:
                with h5py.File(args.inputFile, "r") as hf:
                    data["p_iff"] = hf["val/p_iff"][:]
                    data["p_truthType"] = hf["val/p_truthType"][:]
                    data["p_truth"] = hf["val/p_signal"][:]
                    data["p_eta"] = hf["val/p_eta"][:]
                    data["p_et"] = hf["val/p_et"][:]
            else:
                with h5py.File(args.inputFile, "r") as hf:
                    data["p_iff"] = hf["val/p_iffTruth"][:]
                    data["p_truthType"] = hf["val/p_truthType"][:]
                    data["p_truth"] = hf["val/p_truth"][:]
                    data["p_eta"] = hf["val/p_etaClusterLr2"][:]
                    data["p_et"] = hf["val/p_et_calo"][:]

            weights = np.load(args.logDir + f"/downsamplingWeights{sett}.npy")

            binaryMasks = {"Signal": (data["p_truth"] == 1) &
                                     (downsamplingMask == 1),
                           "Background": (data["p_truth"] == 0) &
                                         (downsamplingMask == 1)}

            multiMasks = {
                          "Signal": (data["p_truth"] == 1) &
                                    (downsamplingMask == 1),
                          "PhotonConv": (data["p_iff"] == 5) &
                                        (downsamplingMask == 1),
                          "HeavyFlavor": ((data["p_iff"] == 8) |
                                          (data["p_iff"] == 9)) &
                                         (downsamplingMask == 1),
                          "LFEgamma": (((data["p_iff"] == 10) &
                                        ((data["p_truthType"] == 4) |
                                         (data["p_truthType"] == 16))) &
                                       (downsamplingMask == 1)),
                          "LFHadron": (((data["p_iff"] == 10) &
                                        (data["p_truthType"] == 17)) &
                                       (downsamplingMask == 1))
                         }

            ###################################################################
            # Distributions versus ET
            ###################################################################
            bins = np.logspace(np.log10(4.5), np.log10(2000), 101)
            # Binary distribution
            plotHists(data["p_et"] / 1000., binaryMasks, bins,
                      args.logDir + "/Plots/EtBinaryDownsampledWeighted.pdf",
                      weights=weights,
                      xlabel=r"$E_T\ \mathrm{[GeV]}$", xlog=True, ylog=True)

            # Multiclass distribution
            plotHists(data["p_et"] / 1000., multiMasks, bins,
                      args.logDir +
                      "/Plots/EtMultiClassDownsampledWeighted.pdf",
                      weights=weights,
                      xlabel=r"$E_T\ \mathrm{[GeV]}$", xlog=True, ylog=True)

            ###################################################################
            # Distributions versus eta
            ###################################################################
            bins = np.linspace(-2.47, 2.47, 101)
            # Binary distribution
            plotHists(data["p_eta"], binaryMasks, bins,
                      args.logDir + "/Plots/EtaBinaryDownsampledWeighted.pdf",
                      weights=weights,
                      xlabel=r"$\eta$", xlog=False, ylog=True)

            # Multiclass distribution
            plotHists(data["p_eta"], multiMasks, bins,
                      args.logDir +
                      "/Plots/EtaMultiClassDownsampledWeighted.pdf",
                      weights=weights,
                      xlabel=r"$\eta$", xlog=False, ylog=True)
