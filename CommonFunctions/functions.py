"""Function used throughout the whole framework."""
import numpy as np
import h5py
import copy
import os
from sklearn.preprocessing import QuantileTransformer
import matplotlib.pyplot as plt


def readData(filename, nPoints, variables):
    """
    Load a number of points for some variables for a .h5 file.

    Args
    ----
        filename : *str*
            Name of the .h5 file which should be read.
        nPoints : *dict*
            Dictionary holding on ekey for each dataset which should be loaded
            (train/val/test), with the value the number of points to load. None
            loads all available points.
        variables : *list*
            List of all variables to load.

    Returns
    -------
        data : *dict*
            Dictionary holding all the data. In the dictionary is a dictionary
            for each dataset that is loaded with the corresponding name. These
            dictionaries hold arrays for each variable with the name of the
            variable being the key.

    """
    # Prepare loading only the wanted data points
    # Make copy of nPoints to use for loading (changes to nPoints
    # will persist outside of this function)
    loadNPoints = copy.deepcopy(nPoints)

    for setName in loadNPoints:
        # If a list (of specific indices) is not given
        if not hasattr(loadNPoints[setName], '__iter__'):
            # If all data points should be loaded
            if loadNPoints[setName] is None:
                loadNPoints[setName] = slice(None)
            # If only the first int(loadNPoints[setName]) data points
            # should be loaded
            else:
                loadNPoints[setName] = int(loadNPoints[setName])
                n_samples = loadNPoints[setName]
                loadNPoints[setName] = slice(None, loadNPoints[setName])
                print(f"Loading only the {n_samples} first " +
                      f"data points of the {setName} set.")
        else:
            loadNPoints[setName] = list(np.sort(loadNPoints[setName]))

    data = {}
    with h5py.File(filename, 'r') as hf:
        for sett in loadNPoints:
            if sett not in hf.keys():
                raise KeyError(f"No set with key '{sett}'")
            data[sett] = {}
            for var in variables:
                if var not in hf[f'{sett}'].keys():
                    raise KeyError(f"No variable with key '{sett}/{var}'")
                data[sett][var] = hf[f"{sett}/{var}"][loadNPoints[sett]]

    return data


def mkdir(directory):
    """Make a directory (helper fuunction)."""
    if not os.path.exists(directory):
        os.makedirs(directory)


def weightedQuantileTransformer(data, qtReferences, weights):
    """
    Create a quantile transformer taking weights into account.

    Args
    ----
        data : *array*
            Array of the input data.
        qtReferences : *array*
            Array which contains the values of the desired quantiles
        weights : *array*
            Weights of the distributions

    Return
    ------
        q : sklearn.preprocessing.QuantileTransformer
            QuantileTransformer object which was fit manually taking weights
            into account.

    """
    # Create QuantileTransformer object and set reference_ to the given ones
    q = QuantileTransformer()
    q.references_ = qtReferences
    # Create "empty" quantiles to fill
    quantiles = np.zeros((qtReferences.shape[0], data.shape[1]))

    # Process each of the features separately
    for i in range(data.shape[1]):
        # Sort the data and the weights
        ind = np.argsort(data[:, i])
        sortedData = data[ind, i]
        sortedWeights = weights[ind]
        # The first weight has to be 0 for some reason
        # (not completely sure why exactly)
        sortedWeights[0] = 0
        # Interpolate usiing the qtReferences as wanted x-coordinates, the
        # normalized cumsum of the weights as x-values, and the data as
        # y-values
        quantiles[:, i] = np.interp(qtReferences, np.cumsum(sortedWeights) /
                                    np.sum(sortedWeights), sortedData)

    q.quantiles_ = quantiles
    return q


def positionLegend(fig, ax):
    """
    Extend y-axis range such that legend can be placed w/o overlap with plot.

    Args
    ----
        fig: *matplotlib figure*
            Figure object which the legend should be placed on.
        ax: *matplotlib axis*
            Axis object which will hold the legend.

    """
    # Split legend into columns such that there are never more than 3 rows
    ncol = int(np.floor(len(ax.get_legend_handles_labels()[1])/3 + 0.9))
    # Place the legend in the upper right to be able to get the position to
    # adjust the axis range
    leg = ax.legend(loc="upper right", ncol=ncol)
    # Draw the plot to make it possible to extract actual positions.
    plt.draw()
    # Get legend positions in pixels of the figures
    a = leg.get_frame().get_bbox().bounds
    ylow, yup = ax.get_ylim()
    # Transform the figure pixels to data coordinates
    q = ax.transData.inverted().transform((0, a[1]))
    # Claculate the new upper limit of the y-axis depending on whether the plot
    # is on a logarithmic scale or not
    if ax.get_yscale() == "log":
        newYup = (10 ** ((np.log10(q[1]) * np.log10(ylow) + np.log10(yup) ** 2
                  - 2 * np.log10(ylow) * np.log10(yup)) / (np.log10(q[1])
                  - np.log10(ylow))))
    else:
        newYup = (q[1] * ylow + yup ** 2 - 2 * ylow * yup) / (q[1] - ylow)

    # Set the new axis limits
    ax.set_ylim([ylow, newYup])


rcParams = {'font.size': 30,

            'axes.titlesize': 44,
            'axes.labelsize': 40,
            'axes.grid': True,
            'grid.alpha': 0.4,

            'lines.linewidth': 4,
            'lines.markersize': 20,
            'patch.linewidth': 4,

            'xtick.labelsize': 36,
            'ytick.labelsize': 36,
            'xtick.top': True,
            'xtick.direction': 'in',
            'xtick.minor.visible': True,
            'xtick.major.size': 5,
            'ytick.right': True,
            'ytick.direction': 'in',
            'ytick.minor.visible': True,
            'ytick.major.size': 5,

            'legend.fontsize': 36,
            'legend.columnspacing': 0.5,
            'legend.handletextpad': 0.4,

            'figure.figsize': [19.20, 10.80],
            'figure.titlesize': 44,
            'figure.subplot.hspace': 0.0,
            'figure.subplot.wspace': 0.1}
