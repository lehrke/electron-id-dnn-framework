"""Create configFiles spanning a grid based on a given HP config file."""
import json
import argparse
import itertools

import sys
sys.path.insert(0, sys.path[0]+"/../")
from CommonFunctions.functions import mkdir


def getParser():
    """Argparse option for Config Generator script."""
    parser = argparse.ArgumentParser(description="Options for creating" +
                                     "config files for HP optimisation.")

    parser.add_argument('--outDir', type=str,
                        help="Save the config files in this directory.")
    parser.add_argument('--configFile', type=str, default="HPconfig.json")
    parser.add_argument('--outputBase', type=str, default="configNew",
                        help="output file base")

    args = parser.parse_args()
    return args


def fillListValues(config, key, comb, keys, prevKey=""):
    """
    Fill value of the config file in a common list.

    Args
    ----
        config: *dict*
            Dictionary holding the HPs to change during the optimization and
            the values it should be changed to.
        key: *str*
            The key of the HP in the config file.
        comb: *list*
            List containing all options for all the different HPs which should
            be tuned.
        keys: *list*
            List containing the names of all the different HPs which should be
            tuned. The order has to be the same as in comb.
        prevKey: *str*
            If the HP is given in another dictionary the complete position in
            the dictionary is given by adding a '/' for a dictionary.

    Returns
    -------
        comb: *list*
            Updated list with options for all HPs.
        keys: *list*

    """
    if key in config.keys():
        # listValue = False
        # for i in range(len(config[key])):
        #     if type(config[key][i]) == list:
        #         listValue = True
        #
        # if listValue:
        #     preUnits = []
        #     for unit in config[key]:
        #         if isinstance(unit, list):
        #                 preUnits.append(unit)
        #         else:
        #                 preUnits.append([unit])
        #
        #     procUnits = list(itertools.product(*preUnits))
        #     procUnits2 = []
        #     for units in procUnits:
        #         procUnits2.append(units)
        #     comb.append(procUnits2)
        #     keys.append(prevKey + key)
        # else:
        comb.append(config[key])
        keys.append(prevKey + key)

    return comb, keys


def setDictValues(dicti, splitted, val):
    """
    Put HPs which are inside dictionaries into the right format.

    Args
    ----
        dicti: *dict*
            Dictionary which should be filled.
        splitted: *list*
            List of the string of the HP splitted at each '/'.
        val: *?*
            Value of the HP.

    """
    if len(splitted) == 1:
        dicti[splitted[0]] = val
    else:
        if splitted[0] not in dicti:
            dicti[splitted[0]] = {}
        setDictValues(dicti[splitted[0]], splitted[1:], val)


def createConfig(args):
    """Create the config files for the grid search."""
    # Open the config file with the different HPs which should be used
    with open(args.configFile, "r") as configFile:
        config = json.load(configFile)

    # Save the config file into the outDir to store the full one
    json.dump(config, open(args.outDir + f"/HPconfig.json",  "w"), indent=4)

    print(config)
    # comb holds the different values of the HP while keys holds the name of it
    comb = []
    keys = []

    # Fill the different values for the HPs
    for key in ["batchSize", "activation", "regularization", "dropout",
                "doBatchNorm", "architecture",
                "discoCoeff", "discoPreprocessing"]:
        comb, keys = fillListValues(config, key, comb, keys)

    # HPs given in dicts or convoluted dicts
    for key in ["lrSchedule"]:
        if key in config.keys():
            for inKey in ["config"]:
                # HPs given in two dicts
                if inKey in config[key].keys():
                    # HPs given as value in two dicts
                    for ininKey in ["baseLr", "increaseLr", "nEpochs"]:
                        comb, keys = fillListValues(config[key][inKey],
                                                    ininKey, comb, keys,
                                                    prevKey=key + "/" +
                                                    inKey + "/")

    # Combine all different HP values to one large grid
    allCombs = list(itertools.product(*comb))
    print(allCombs)

    # Each tuple in allCombs is one HP configuration which should be tested
    for i, comb in enumerate(allCombs):
        # Make a dictionary out of the configuration
        dicti = {}
        for j, val in enumerate(comb):
            splitted = keys[j].split("/")
            setDictValues(dicti, splitted, val)

        # Save the HP configuration as a config file to be used
        json.dump(dicti, open(args.outDir + f"/{args.outputBase}_{i}.json",
                              "w"), indent=4)


if __name__ == "__main__":
    args = getParser()

    mkdir(args.outDir)

    createConfig(args)
