"""Plot HP search results in parallel coordinates."""
import numpy as np
import os
import json
import re
import pandas as pd
import argparse
import matplotlib as mpl
from matplotlib import ticker
import matplotlib.pyplot as plt

import sys
sys.path.insert(0, sys.path[0]+"/../")
from CommonFunctions.functions import rcParams

plt.rcParams.update(rcParams)


def getParser():
    """Get options set in command line."""
    parser = argparse.ArgumentParser(description="Options for HPO Plotting")

    parser.add_argument("--logDir", action="store", default="", type=str,
                        help="Directory where HP optimization is stored.")
    args = parser.parse_args()

    return args


def getEpochLossTime(logFile, metric):
    """
    Read out loss or metric, and wall time per epoch for a tensorboard file.

    Args
    ----
        logFile : *str*
            Tensorboard file which has the loss and the metrics saved

        metric : *str*
            Metric to read out

    Returns
    -------
        loss : *list*
            List of the loss/metric values for each epoch

        time : *list*
            List of the wall time values for each epoch

    """
    import tensorboard.backend.event_processing.event_accumulator as evt
    import glob

    logs = glob.glob(logFile)
    if logs == []:
        return -999, -999
    fileIn = max(logs, key=os.path.getctime)
    ea = evt.EventAccumulator(fileIn,
                              size_guidance={evt.COMPRESSED_HISTOGRAMS: 1,
                                             evt.IMAGES: 1,
                                             evt.AUDIO: 1,
                                             evt.SCALARS: 0,
                                             evt.HISTOGRAMS: 1})
    ea.Reload()

    loss = []
    time = []
    for epoch in ea.Scalars('epoch_' + metric):
        loss.append(epoch.value)
        time.append(epoch.wall_time)

    if len(loss) <= 39:
        return -999, -999

    return np.min(loss), np.mean(np.diff(time)[1:])


def getValues(logDirs, hp, metric="binary_crossentropy"):
    """
    Read the configurations for each logDir and their results.

    Args
    ----
        logDirs : *list of str*
            Logging directories for each configuration of the HPO
        hp : *list of str*
            Names of the HPs which are optimized in this HPO.
        metric : *str*
            Metric which should be plotted as the last coordinate of the plots.

    Return
    ------
        hpDict : *dict*
            Dictionary holding the configuration of the optimized HPs.
        metricArr : *array*
            Array holding the lowest metric value on the validation set for
            each configuration.
        timeArr : *array*
            Array holding the mean epoch time during the training for each
            configuration.
    """
    hpDict = {}
    for key in hp:
        hpDict[key] = []
    metricList = []
    timeList = []
    for directory in logDirs:
        loss, time = getEpochLossTime(directory + "/validation/events*v2",
                                      metric)
        if loss == -999:
            continue
        if loss >= 0.053:
            continue

        metricList.append(loss)
        timeList.append(time)

        with open(directory + "/config.json", "r") as configFile:
            config = json.load(configFile)

        for key in hp:
            if type(config[key]) == list:
                hpDict[key].append(str(config[key]))
            else:
                hpDict[key].append(config[key])
    metricArr = np.array(metricList)
    timeArr = np.array(timeList)
    return (hpDict, metricArr, timeArr)


def plotParallelCoordinates(df, cols, dicti, name, nTicks):
    """Plot parallel coordinates."""
    transformedCols = []
    for col in cols:
        if "Numeric" in col:
            transformedCols.append(True)
        else:
            transformedCols.append(False)
    colorCol = cols[-1]
    x = [i for i, _ in enumerate(cols)]

    # Create (X-1) sublots along x axis
    fig, axes = plt.subplots(1, len(x) - 1, sharey=False, figsize=(75, 12))

    # Get min, max and range for each column
    # Normalize the data for each column
    min_max_range = {}
    length = len(cols)
    for i, col in enumerate(cols):
        if i == length - 1:
            min_max_range[col] = [df[col].min(), df[col].max(),
                                  np.ptp(df[col])]
            df[col] = (np.true_divide(df[col] - df[col].min(),
                       np.ptp(df[col])) - 1/22.) * 11./10.
            color = np.true_divide(df[col] - df[col].min(), np.ptp(df[col]))
        else:
            min_max_range[col] = [df[col].min(), df[col].max(),
                                  np.ptp(df[col])]
            df[col] = np.true_divide(df[col] - df[col].min(), np.ptp(df[col]))

    # Plot each row
    cm = plt.get_cmap("jet")
    for i, ax in enumerate(axes):
        for idx in df.index:
            ax.plot(x, df.loc[idx, cols], color=cm(color[idx]))
        ax.set_xlim([x[i], x[i+1]])

    # Set the tick positions and labels on y axis for each plot
    # Tick positions based on normalised data
    # Tick labels are based on original data
    def set_ticks_for_axis(dim, transformedCols, dicti, ax, ticks):
        if transformedCols[dim]:
            ticks = len(dicti[re.sub("Numeric", "", cols[dim])])
            tick_labels = dicti[re.sub("Numeric", "", cols[dim])]
            norm_min = df[cols[dim]].min()
            norm_range = np.ptp(df[cols[dim]])
            norm_step = norm_range / float(ticks-1)
            ticks = [round(norm_min + norm_step * i, 2) for i in range(ticks)]
        else:
            min_val, max_val, val_range = min_max_range[cols[dim]]
            step = val_range / float(ticks-1)
            tick_labels = [round(min_val + step * i, 2) for i in range(ticks)]
            norm_min = df[cols[dim]].min()
            norm_range = np.ptp(df[cols[dim]])
            norm_step = norm_range / float(ticks-1)
            ticks = [round(norm_min + norm_step * i, 2) for i in range(ticks)]
        ax.yaxis.set_ticks(ticks)
        ax.set_yticklabels(tick_labels)

    for dim, ax in enumerate(axes):
        ax.xaxis.set_major_locator(ticker.FixedLocator([dim]))
        ax.set_ylim([-0.05, 1.05])
        set_ticks_for_axis(dim, transformedCols, dicti, ax, ticks=nTicks[dim])
        ax.set_xticklabels([re.sub("Numeric", "", cols[dim])])

    # Move the final axis' ticks to the right-hand side
    ax = plt.twinx(axes[-1])
    dim = len(axes)
    ax.set_ylim([-0.05, 1.05])
    ax.yaxis.set_ticklabels([])
    ax.xaxis.set_major_locator(ticker.FixedLocator([x[-2], x[-1]]))
    ax.set_xticklabels([re.sub("Numeric", "", cols[-2]),
                        re.sub("Numeric", "", cols[-1])])

    # Add legend to plot
    norm = mpl.colors.Normalize(vmin=min_max_range[colorCol][0],
                                vmax=min_max_range[colorCol][1])

    sm = plt.cm.ScalarMappable(cmap=cm, norm=norm)
    plt.colorbar(sm)

    # plt.tight_layout()
    # Remove space between subplots
    plt.subplots_adjust(wspace=0)
    plt.savefig(name)


def makeNumeric(df, columns):
    """
    Turn values of HPs into strings if they are not plain numbers.

    Args
    ----
        df : *pandas dataframe*
            DataFrame containing all the HPs which should be plotted.
        columns : *list of str*
            Names of the columns which need to be turned into strings. Only
            entries which are in the DataFrame will be considered.

    Return
    ------
        df : *pandas dataframe*
            Updated DataFrame which includes the transformed values. The
            transformed columns get an additional 'Numeric' after their
            original name.
        dicti : *dict*
            Dictionary containing the original values of every transformed
            column to later put these values on the plot.
    """
    dicti = {}
    for col in columns:
        if col in df:
            liste = df[col].tolist()
            a = sorted(list(set(liste)))

            numeric = []
            for val in liste:
                for i, value in enumerate(a):
                    if value == val:
                        numeric.append(i)

            df[f"{col}Numeric"] = numeric
            dicti[col] = a
    return df, dicti


def makePlots(hpoDir):
    """
    Plot the parallel coordinate plots for the given directory.

    Args
    ----
        hpoDir : *str*
            Directory where the logDirs of the HPO are located. Additionally,
            the HP config file from which the config files for each network
            are produced should be located here.
    """
    df = pd.read_csv(hpoDir + "/combined.csv")
    df = df.drop(["logDir", "Loss", "Unnamed: 0"], axis=1)
    df = df.sort_values("Binary Crossentropy", axis=0,
                        ascending=False, inplace=False)

    hp = df.columns

    strings = ["logDir", "Loss", "activation",
               "doBatchNorm", "architecture"]
    df, dicti = makeNumeric(df, strings)

    print(df)
    plotKey = ([key + "Numeric" if key in strings else key for key in hp] +
               ["dropout", "regularization", "activationNumeric",
                "doBatchNormNumeric", "Binary Crossentropy"])
    plotParallelCoordinates(df, plotKey, dicti, hpoDir +
                            f"/parallelCoordinates.pdf", [0]*(len(hp) + 10))


if __name__ == "__main__":
    # Read arguments from the command line
    args = getParser()

    makePlots(args.logDir)
