"""Calculate metrics and plot control/performance plots for trained NNs."""
import pandas as pd
import numpy as np
import json
import h5py
import os.path
import argparse

import sys
sys.path.insert(0, sys.path[0]+"/../")
import Validation.functions.metrics as mt


def getParser():
    """Get options set in command line."""
    parser = argparse.ArgumentParser(description="Options for NN Training")

    parser.add_argument("--logDir", action="store", required=True, type=str,
                        help="Directory where network was saved.")
    parser.add_argument("--combined", action="store", default="", type=str,
                        help="Append df to a combined one found at this path")
    parser.add_argument("--hpConfigFile", action="store", type=str,
                        required=True)

    args = parser.parse_args()

    return args


def processLogDir(config, HPConfig=None):
    """
    Calculate several metrics of the predictions and saves them in a .csv file.

    Args
    ----
        config : *dict*
            Dictionary obtained from reading the config.json file of the
            desired logDir. It needs to contain the following keys: 'logDir',
            'testFile'.
        hpConfig : *dict*
            Dictionary containing the updated HP parameters. With this the
            changed HP can be put in the .csv file

    """
    if os.path.isfile(config["logDir"] + "/val.csv"):
        return pd.read_csv(config["logDir"] + "/val.csv")

    # maskData = {}
    plotData = {}
    with h5py.File(config["testFile"], "r") as hf:
        plotData["truth"] = hf["test/p_signal"][:]
        plotData["iff"] = hf["test/p_iff"][:]
        plotData["truthType"] = hf["test/p_truthType"][:]
        plotData["et"] = hf["test/p_et"][:] / 1e3
        plotData["eta"] = hf["test/p_eta"][:]
        plotData["mu"] = hf["test/p_mu"][:]

    preds = np.load(config["logDir"] + "/preds.npy")

    etBins = np.array([15, 25, 40, 60, 6000])
    etaBins = np.array([0.0, 1.32, 1.57, 2.47])

    data = {"logDir": [config["logDir"]],
            "Loss": [config["loss"]],
            "Binary Crossentropy": [mt.binaryCrossentropy(preds,
                                                          plotData["truth"])],
            "Bkg rej at 70% sig eff": [1 / mt.bkgEffAtSigEff(preds,
                                                             plotData["truth"],
                                                             0.7)],
            "Bkg rej at 80% sig eff": [1 / mt.bkgEffAtSigEff(preds,
                                                             plotData["truth"],
                                                             0.8)],
            "Bkg rej at 90% sig eff": [1 / mt.bkgEffAtSigEff(preds,
                                                             plotData["truth"],
                                                             0.9)],
            "Mean Bkg rej at " +
            "70% sig eff": [1 / mt.meanBkgEffAtSigEff(preds,
                                                      plotData["truth"],
                                                      etBins, plotData["et"],
                                                      etaBins, plotData["eta"],
                                                      0.7)],
            "Mean Bkg rej at " +
            "80% sig eff": [1 / mt.meanBkgEffAtSigEff(preds,
                                                      plotData["truth"],
                                                      etBins, plotData["et"],
                                                      etaBins, plotData["eta"],
                                                      0.8)],
            "Mean Bkg rej at " +
            "90% sig eff": [1 / mt.meanBkgEffAtSigEff(preds,
                                                      plotData["truth"],
                                                      etBins, plotData["et"],
                                                      etaBins, plotData["eta"],
                                                      0.9)],


            "AUC": [mt.auc(preds, plotData["truth"])],
            "AUC between 60% and 98% sig eff": [mt.aucWindow(preds,
                                                             plotData["truth"],
                                                             low=0.6,
                                                             up=0.98)]
            }

    df = pd.DataFrame(data)

    if HPConfig:
        with open(HPConfig, "r") as hpConfigFile:
            hpConfig = json.load(hpConfigFile)

    for key in hpConfig:
        if type(hpConfig[key]) == dict:
            for inKey in hpConfig[key]:
                if type(hpConfig[key][inKey]) == dict:
                    for ininKey in hpConfig[key][inKey]:
                        df[key + "/" +
                           inKey + "/" +
                           ininKey] = hpConfig[key][inKey][ininKey]
                else:
                    df[key + "/" + inKey] = hpConfig[key][inKey]
        else:
            df[key] = (hpConfig[key] if type(hpConfig[key]) is not list
                       else [hpConfig[key]])

    df.to_csv(config["logDir"] + "/val.csv")

    return df


def writeCombinedDataFrame(path, df):
    """
    Read a csv file and save the file after merging the dataframe into it.

    Args
    ----
        path : *str*
            CSV file which holds the metrics for other logDirs. If it does not
            exist yet, it will be created. If it contains the given dataframe
            already, it will not be changed; no logDir will be saved twice in
            the file.
        df : pandas.DataFrame
            DataFrame which contains the calculated metrics given by
            processLogDir.

    """
    if os.path.isfile(path):
        fullDf = pd.read_csv(path)
        if df["logDir"][0] in fullDf["logDir"].values:
            return
        fullDf = fullDf.append(df)
        fullDf.to_csv(path, index=False)

    else:
        df.to_csv(path, index=False)


if __name__ == "__main__":
    args = getParser()

    with open(args.logDir + "/config.json", "r") as confFile:
        config = json.load(confFile)

    df = processLogDir(config, HPConfig=args.hpConfigFile)

    if args.combined != "":
        writeCombinedDataFrame(args.combined, df, config)
