"""Plot performance plot for several trained networks."""
import numpy as np
import h5py
import argparse
import seaborn as sns
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

import sys
sys.path.insert(0, sys.path[0]+"/../")
from CommonFunctions.functions import mkdir, rcParams

plt.rcParams.update(rcParams)


def getParser():
    """Get options set in command line."""
    parser = argparse.ArgumentParser(description="??")

    parser.add_argument("--outDir", action="store", required=True, type=str,
                        default="ImprovementPlots/",
                        help="Directory where to save the plots.")
    parser.add_argument("--inputFile", action="store", type=str,
                        default="/home/users/e/ehrke/scratch/hdf5/" +
                                "DNN/VDS_Zee_JF17_e_baobab.h5",
                        help="h5 file to calculate improvements.")
    parser.add_argument("--workingPoint", action="store", type=str,
                        choices=["LHLoose", "LHMedium", "LHTight"],
                        help="Working point to match signal efficiency")
    parser.add_argument("--preds", action="store", type=str,
                        help="Prediction File.")
    parser.add_argument("--applyBlayerCut", action="store_true",
                        help="Apply rectangular cut on number of Blayer hits.")
    parser.add_argument("--ambiguityCut", action="store", type=int, default=6,
                        help="Apply rectangular cut on ambiguity bit. " +
                             "Default value is 6 which corresponds to only " +
                             "photons and therefore should never happen. 5 " +
                             "is the cut that LHLoose and LHMedium apply, 2 " +
                             "for LHTight")
    parser.add_argument("--model", action="store", type=str,
                        choices=["ZJF", "Ztt"], default="ZJF")
    parser.add_argument("--datasetName", action="store", type=str,
                        choices=["ZJF", "Ztt"], default="ZJF")
    args = parser.parse_args()

    return args


def calculateThresholds(etBins, etaBins, et, eta, truth, weight, wp, preds):
    """
    Calculate thresholds in bins on DNN score to reach same sig eff as LH.

    Args
    ----
        etBins: numpy.array
            Binning of the thresholds in Et.
        etaBins: numpy.array
            Binning of the thresholds in |eta|.
        et: numpy.array
            Et values of data
        eta: numpy.array
            eta values of data
        truth: numpy.array
            Truth information of data. (1 for signal, 0 for bkg)
        weight: numpy.array
            Per sample weight for the data. Note: this is not the weight from
            any reweighting applied to match signal and bkg but weights from
            "physics" like the pileup weight.
        wp: numpy.array
            Decision of the LH working point if the event should be accepted
            or not.
        preds: numpy.array
            Predictions of the DNN.

    Returns
    -------
        threshold: numpy.array
            2D array holding thresholds on the DNN score to match sig eff of
            a LH working point in bins of Et/eta.

    """
    threshold = np.ones(shape=(len(etBins) - 1, len(etaBins) - 1))
    for i in range(len(etBins) - 1):
        for j in range(len(etaBins) - 1):
            maskBin = (et > etBins[i]) & \
                      (et <= etBins[i + 1]) & \
                      (np.abs(eta) > etaBins[j]) & \
                      (np.abs(eta) <= etaBins[j + 1]) & \
                      (truth == 1)

            nSig = np.sum(weight[(maskBin == 1)])
            sigEffLH = np.sum(weight[(wp == 1) & (maskBin == 1)]) / nSig

            indx = np.argsort(preds[(maskBin == 1)])
            weightCumSum = np.cumsum(weight[(maskBin == 1)][indx]) / nSig
            cutIndx = np.sum(weightCumSum <= (1 - sigEffLH))
            threshold[i, j] = preds[(maskBin == 1)][indx][(cutIndx + 1)]

    return threshold


def calculateImprovements(etBins, etaBins, et, eta, truth, weight,
                          wp, preds, threshold, mask):
    """
    Calculate improvement in bkg rejection of DNN over LH in bins and combined.

    Args
    ----
        etBins: numpy.array
            Binning of the thresholds in Et.
        etaBins: numpy.array
            Binning of the thresholds in |eta|.
        et: numpy.array
            Et values of data
        eta: numpy.array
            eta values of data
        truth: numpy.array
            Truth information of data. (1 for signal, 0 for bkg)
        weight: numpy.array
            Per sample weight for the data. Note: this is not the weight from
            any reweighting applied to match signal and bkg but weights from
            "physics" like the pileup weight.
        wp: numpy.array
            Decision of the LH working point if the event should be accepted
            or not.
        preds: numpy.array
            Predictions of the DNN.
        threshold: numpy.array
            Thresholds on the DNN score to match the signal efficiency of the
            LH working point.
        mask: numpy.array
            Mask to mask certain events. Can be used to get improvements for
            only one background class separately.

    Returns
    -------
        improvement: *float*
            Overall improvement calculated by taking the weighted average of
            the improvements in the single bins where the weight is given by
            the number of bkg events in the bin before any selection
        improvementErr: *float*
            Statistical uncertainty on the overall improvement. Only
            uncertainties on the improvement and not on the total number
            of bkg events are used.
         arrBkgDNN: numpy.array
             Improvements in bins of Et/eta
         arrBkgDNNErr: numpy.array
             Statistical uncertainty of the improvements in bins of Et/eta

    """
    meanDNN = 0
    meanDNNErrSquared = 0
    totalBkg = 0
    arrBkgDNN = np.ones(shape=(len(etBins) - 1, len(etaBins) - 1))
    arrBkgDNNErr = np.ones(shape=(len(etBins) - 1, len(etaBins) - 1))
    for i in range(len(etBins) - 1):
        for j in range(len(etaBins) - 1):
            maskBin = (et > etBins[i]) & \
                      (et <= etBins[i + 1]) & \
                      (np.abs(eta) > etaBins[j]) & \
                      (np.abs(eta) <= etaBins[j + 1]) & \
                      (truth == 0) & \
                      (mask == 1)

            nBkg = np.sum(weight[(maskBin == 1)])

            nBkgLH = np.sum(weight[(wp == 1) & (maskBin == 1)])
            nBkgEffLH = nBkgLH / nBkg
            nBkgEffLHErr = np.sqrt(nBkgEffLH * (1 - nBkgEffLH) /
                                   nBkg)
            nBkgLHErr = nBkgEffLHErr / nBkgEffLH**2

            nBkgDNN = np.sum(weight[(preds > threshold[i, j]) &
                                    (maskBin == 1)])
            nBkgEffDNN = nBkgDNN / nBkg
            nBkgEffDNNErr = np.sqrt(nBkgEffDNN * (1 - nBkgEffDNN) /
                                    nBkg)
            nBkgDNNErr = nBkgEffDNNErr / nBkgEffDNN**2

            arrBkgDNN[i, j] = nBkgLH / nBkgDNN
            arrBkgDNNErr[i, j] = (arrBkgDNN[i, j] *
                                  np.sqrt((nBkgLHErr * nBkgEffLH) ** 2 +
                                          (nBkgDNNErr * nBkgEffDNN) ** 2))

            # TODO not the best way
            if etBins[i] < 60 and arrBkgDNN[i, j] < 1e3 \
               and arrBkgDNN[i, j] > 1e-4:
                meanDNN += arrBkgDNN[i, j] * nBkg
                meanDNNErrSquared += (nBkg * arrBkgDNNErr[i, j]) ** 2
                totalBkg += nBkg

    improvement = meanDNN / totalBkg
    improvementErr = np.sqrt(meanDNNErrSquared) / totalBkg
    return improvement, improvementErr, arrBkgDNN, arrBkgDNNErr


def plotHeatMap(impr, imprErr, bkg, workingPoint, outDir, model, datasetName):
    """
    Plot a 2D heatmap showing the improvements.

    Args
    ----
        impr: numpy.array
            2D improvements to be plotted in the heatmap.
        imprErr: numpy.array
            Uncertainty on the improvements.
        bkg: *str*
            What bkg class was used to calcualte the improvements.
        workingPoint: *str*
            Which LH working point is the DNN being comared to.
        outDir: *str*
            Directory where the plot will be stored.
        model: *str*
            Which dataset was used to train the DNN. Right now either "ZJF" to
            indicate that it was trained on Zee + JF17 or something else for
            Zee + ttbar.
        datasetName: *str*
            Which dataset is used to calculate the improvements. Right now
            either "ZJF" to  indicate that it was trained on Zee + JF17 or
            something else for Zee + ttbar.

    """
    if model == "ZJF":
        mdlPlot = r"$Z+\mathrm{JF17}$"
    else:
        mdlPlot = r"$Z+t\bar{t}$"
    if datasetName == "ZJF":
        dataPlot = r"$Z+\mathrm{JF17}$"
    else:
        dataPlot = r"$Z+t\bar{t}$"
    impr = np.flip(impr, axis=0)
    mask = (impr > 1000) | (impr < 1e-4)
    imprErr = np.flip(imprErr, axis=0)

    precision = np.asarray([len(f"{uncer:.1g}".partition(".")[2])
                            for uncer in imprErr.flatten()])
    labels = (np.asarray([rf"${value:.{prec}f}\pm{uncer:.{prec}f}$"
                          for value, uncer, prec in zip(impr.flatten(),
                                                        imprErr.flatten(),
                                                        precision)])
              ).reshape(impr.shape)

    cbarLbl = rf"$\epsilon^\mathrm{{{bkg}}}_\mathrm{{{workingPoint}}}$" + \
              rf"$/\epsilon^\mathrm{{{bkg}}}_\mathrm{{DNN}}$"
    fig, ax = plt.subplots(constrained_layout=True)
    sns.heatmap(impr, vmin=0, vmax=5, center=1, mask=mask,
                annot=labels, fmt="", annot_kws={"fontsize": 30},
                cmap=sns.color_palette("coolwarm", 21), ax=ax,
                linewidths=.1, linecolor="black",
                cbar_kws={"label": cbarLbl}
                )
    ax.set_ylabel(r"$E_\mathrm{T}$ [GeV]")
    ax.set_xlabel(r"$|\eta|$")
    ax.set_title(f"WP: {workingPoint}")  # Dataset: {dataPlot}, Model: {mdlPlot},
    ax.tick_params(axis="both", which="major", pad=15)
    plt.yticks(np.arange(10), (r"$\infty$", "250", "100", "60", "40", "35",
                               "30", "25", "20", "15"), rotation=0)
    plt.xticks(np.arange(6), ("0.0", "0.8", "1.37", "1.52", "2.01", "2.47"),
               rotation=0)
    fig.savefig(outDir + f"/Model{model}_dataset{datasetName}" +
                f"_{workingPoint}_{bkg}.pdf")
    plt.close("all")


def calculateAndPlotImprovements(args):
    """
    Calculate improvements of the DNN over the LH and plot them in a heatmap.

    Args
    ----
        args: ??
            Arguments from the ArgumentParser. See above for more details.

    """
    with h5py.File(args.inputFile, "r") as hf:
        et = hf["test/p_et"][:] / 1000.
        eta = hf["test/p_eta"][:]
        wp = hf["test/p_" + args.workingPoint][:]
        truth = hf["test/p_signal"][:]
        iff = hf["test/p_iff"][:]
        weight = hf["test/p_pileupWeight"][:]
        truthType = hf["test/p_truthType"][:]
        ambiguityBit = hf["test/p_ambiguityBit"][:]

        if args.applyBlayerCut:
            nBlayerCut = hf["test/p_LHBlayerCut"][:]

    mask = (truth == 1) | (iff == 5) | \
           (iff == 8) | (iff == 9) | \
           (iff == 10)

    preds = np.load(args.preds)[mask == 1]
    et = et[mask == 1]
    eta = eta[mask == 1]
    wp = wp[mask == 1]
    weight = weight[mask == 1]
    truth = truth[mask == 1]
    iff = iff[mask == 1]
    truthType = truthType[mask == 1]

    if args.applyBlayerCut:
        preds[nBlayerCut[mask == 1] == 0] = -999.
    preds[ambiguityBit[mask == 1] >= args.ambiguityCut] = -999.

    etBins = np.array([15, 20, 25, 30, 35, 40, 60, 100, 250, 2000])
    etaBins = np.array([0, 0.8, 1.37, 1.52, 2.01, 2.47])

    bkgMasks = {"All Bkg": (truth == 1) | (truth == 0),
                "PhotonConv": (truth == 1) | (iff == 5),
                "HeavyFlavor": ((truth == 1) | (iff == 8) | (iff == 9)),
                "LFEgamma": ((truth == 1) |
                             ((iff == 10) &
                              ((truthType == 4) | (truthType == 16)))),
                "LFHadron": ((truth == 1) | ((iff == 10) & (truthType == 17)))
                }

    threshold = calculateThresholds(etBins, etaBins, et, eta,
                                    truth, weight, wp, preds)

    for key in bkgMasks:
        (improvement, improvementErr,
         arrBkgDNN, arrBkgDNNErr) = calculateImprovements(etBins, etaBins, et,
                                                          eta, truth, weight,
                                                          wp, preds, threshold,
                                                          bkgMasks[key])

        print(key, improvement, improvementErr)

        plotHeatMap(arrBkgDNN, arrBkgDNNErr, key, args.workingPoint,
                    args.outDir, args.model, args.datasetName)


if __name__ == "__main__":
    args = getParser()

    mkdir(args.outDir)

    calculateAndPlotImprovements(args)
