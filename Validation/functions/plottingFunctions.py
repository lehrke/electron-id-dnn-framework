"""
Functions which are used for plotting.

Common arguments for the functions in this file. If there are specific things
for a function, they will be mentioned in the functions doc string.

logDir: *str*
    Directory where everything will be saved from this function. Often the
    actual directory where stuff will be saved is a subdirectory to keep plots
    more organized. When a single model should be plotted give the logDir of
    this model. For multiple models give a logDir which needs to be created
    before.

plotData: *dict*
    Dictionary containing all the necessary additional data for the respective
    function. Each key is the name of a variable and has an array as value
    holding this variable. Common needed variables are: et, eta, LH working
    points (e.g. LHLoose).

preds: *array* or *dict*
    Predictions of the model that should be plotted. If multiple models should
    be plotted on one plot (only possible/sensible for some functions) a
    dictionary should be passed which has one item for each model containing
    in 'preds' an array holding the predictions and in 'label' a string which
    will be used in the legend of the plots to distinguish between models. In
    the doc string of each function it is stated whether multiple models can be
    plotted with this function.

etBins: *array*
    Array containing the bin edges in Et which are used for the plots.
    E.g. [4, 15, 25, 40, 60, 5000] would result in 5 bins with the first one
    going from 4 to 15

etaBins: *array*
    Array containing the bin edges in eta which are used for the plots.
    Usually the absolute value of eta is used.
    E.g. [0.0, 1.32, 1.57, 2.47] would result in 3 bins with the first one
    going from 0.0 to 1.32

"""
import os
import numpy as np
from sklearn.metrics import roc_curve
import seaborn as sns
# Make sure that plotting works while running on a cluster
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


import sys
sys.path.insert(0, sys.path[0]+"/../../")

from CommonFunctions.functions import positionLegend, rcParams

# Set some of the common parameters to make plots "nice" and hopefully readable
# if put into presentations.
plt.rcParams.update(rcParams)


def convertBins(bins):
    """
    Convert an array with bin edges into array with [lower edge, upper edge].

    Args
    ----
        bins: *array of floats*
            Numpy array with bin edges.

    Returns
    -------
        newBins: *array of floats*
            Array with one entry for each bin with [lower edge, upper edge]

    """
    newBins = []
    for i in range(len(bins)-1):
        newBins.append([bins[i], bins[i+1]])

    return newBins


def getBinCenter(binEdges, log=False):
    """
    Get bin centers for given bin edges.

    Args
    ----
        binEdges: *array of floats*
            Edges of the bins for which the centers should be calculated
        log: *bool*
            Wheter the binning is on a log scale or not. (Not sure how to do
            this or if this is needed at all)

    Returns
    -------
        *array of floats*
            Array with the center of each bin.

    """
    if log:
        return binEdges[:-1], binEdges[:-1]  # TODO is that needed ?
    else:
        return (
                (binEdges[:-1] + binEdges[1:]) / 2.,
                (binEdges[1:] - binEdges[:-1]) / 2.
               )


def plotErrorBarPlot(binCenters, binLength, values, outName,
                     legend=False, xlabel="", ylabel="", text=""):
    """
    Plot multiple errorbars by passing a dictionary of the values.

    Args
    ----
        binCenters: *array*
            x-coordinates for all errorbars
        binLength: *array*
            x-error for all errorbars
        values: *dict*
            Dictionary containing the values to be plotted. The key of each
            element will be used in the legend of the plot. The values is a
            list of two arrays, the first one giving the y-values,
            the second one the y-error.
        outName: *str*
            Name of the file in which the plot will be saved.
        legend: *bool*
            Default=False; Whether to draw the legend on the plot
        xlabel: *str*
            Default=""; Label to use for the x-axis.
        ylabel: *str*
            Default=""; Label to use for the y-axis.
        text: *str*
            Additional text to be placed on the plot.

    """
    fig, ax = plt.subplots()
    for key in values:
        ax.errorbar(binCenters, values[key][0], xerr=binLength,
                    yerr=values[key][1], fmt="d", label=key, capsize=7,
                    capthick=3)
    # Cosmetics of plot
    ax.set_ylabel(ylabel)
    ax.set_xlabel(xlabel)
    ax.text(0.02, 0.98, text, transform=ax.transAxes, verticalalignment="top")
    plt.tight_layout()
    if legend:
        positionLegend(fig, ax)
    plt.savefig(outName)
    plt.close("all")


def plotErrorBarRatioPlot(binCenters, binLength, reference, values, outName,
                          xlabel="", ylabelTop="", ylabelBot="", text="",
                          ylim=None):
    """
    Plot an errorbar plot with a ratio plot to a given reference at the bottom.

    Args
    ----
        binCenters: *array*
            x-coordinates for all errorbars
        binLength: *array*
            x-error for all errorbars
        reference: *dict*
            Dictionary containing the values of the reference to which the
            ratio should be taken. These values will be plotted in the top
            plot, but not in the bottom (ratio) plot. The dictionary needs the
            same structure as the values dictionary (see below).
        values: *dict*
            Dictionary containing the values to be plotted. The key of each
            element will be used in the legend of the plot. The values are a
            list of two arrays, the first one giving the y-values,
            the second one the y-error.
        outName: *str*
            Name of the file in which the plot will be saved.
        legend: *bool*
            Default=False; Whether to draw the legend on the plot
        xlabel: *str*
            Default=""; Label to use for the x-axis.
        ylabelTop: *str*
            Default=""; Label to use for the y-axis of the top plot.
        ylabelBot: *str*
            Default=""; Label to use for the y-axis of the bottom plot.
        text: *str*
            Additional text to be placed on the top plot.
        ylim: *list*
            Limits for the y-axis of the ratio plot. If None, will not set
            specific limits but will use the default ones from matplotlib

    """
    fig, ax = plt.subplots(2, gridspec_kw={"height_ratios": [3, 1]},
                           sharex=True)

    eps = 1e-10
    # Plot the reference errorbars first (should only be one)
    for key in reference:
        refVal = reference[key][0]
        refErr = reference[key][1]
        ax[0].errorbar(binCenters, refVal, xerr=binLength, yerr=refErr,
                       fmt="d", color="black", label=key,
                       capsize=7, capthick=3)

    # Plot the remaining errorbars
    for key in values:
        ax[0].errorbar(binCenters, values[key][0], xerr=binLength,
                       yerr=values[key][1], fmt="d", label=key,
                       capsize=7, capthick=3)

        # Calculate the ratio with respect to the reference and plot the ratio
        ratio = np.divide(values[key][0], (refVal + eps))
        error = ratio * np.sqrt((refErr / refVal)**2 +
                                (values[key][1] / values[key][0])**2)
        ax[1].errorbar(binCenters, ratio, xerr=binLength, yerr=error,
                       fmt="d", capsize=7, capthick=3)

    ax[1].plot([binCenters[0]-binLength[0], binCenters[-1]+binLength[-1]],
               [1, 1], color="grey", alpha=0.7)
    # Cosmetics of plot
    ax[0].set_ylabel(ylabelTop)
    ax[0].text(0.02, 0.98, text, transform=ax[0].transAxes,
               verticalalignment="top")

    ax[1].set_ylabel(ylabelBot)
    ax[1].set_xlabel(xlabel)
    if ylim is not None:
        ax[1].set_ylim(ylim)

    plt.tight_layout()
    plt.subplots_adjust(hspace=0)
    positionLegend(fig, ax[0])
    fig.savefig(outName)
    plt.close("all")


def getEpochLoss(logFile, metric):
    """
    Read out the loss or a metric per epoch for a tensorboard file.

    Args
    ----
        logFile: *str*
            Tensorboard file which has the loss and the metrics saved.
        metric: *str*
            Metric to read out.

    Returns
    -------
        loss: *list*
            List of the loss/metric values for each epoch
        lossEpoch: *list*
            List of the number of epochs.

    """
    import tensorboard.backend.event_processing.event_accumulator as evt
    import glob

    logs = glob.glob(logFile)
    fileIn = max(logs, key=os.path.getctime)
    ea = evt.EventAccumulator(fileIn,
                              size_guidance={evt.COMPRESSED_HISTOGRAMS: 1,
                                             evt.IMAGES: 1,
                                             evt.AUDIO: 1,
                                             evt.SCALARS: 0,
                                             evt.HISTOGRAMS: 1})
    ea.Reload()

    loss = []
    lossEpoch = np.arange(len(ea.Scalars('epoch_' + metric))) + 1
    for epoch in ea.Scalars('epoch_' + metric):
        loss.append(epoch.value)

    return loss, lossEpoch


def plotTrainValLoss(logDir, metric):
    """
    Plot the training and validation loss during the training.

    Args
    ----
        logDir: *str*
            See in top doc string.
        metric: *str*
            Metric which should be plotted. This has to be the exact same
            spelling as it is given to the model. For instance
            "binary_crossentropy" for binary crossentropy.

    """
    trainLoss, trainLossEpoch = getEpochLoss(logDir + "/train/events*v2",
                                             metric)
    valLoss, valLossEpoch = getEpochLoss(logDir + "/validation/events*v2",
                                         metric)

    def getPlotMetric(metric):
        """
        Get a nice name for the metric for the plot.

        Args
        ----
            metric: *str*
                Metric which will be plotted. Right now "loss" and
                "binary_crossentropy" are supported. Other metrics will not get
                a different name.

        """
        if metric == "loss":
            return "Loss"
        elif metric == "binary_crossentropy":
            return "CE"
        else:
            return metric

    plotMetric = getPlotMetric(metric)

    fig, ax = plt.subplots(figsize=(19.20, 12.00))
    ax.plot(trainLossEpoch, trainLoss, label=f"Training {plotMetric}")
    ax.plot(valLossEpoch, valLoss, label=f"Validation {plotMetric}")
    ax.plot(valLossEpoch, [np.min(valLoss)]*len(valLossEpoch),
            label=f"Minimal Validation {plotMetric} at Epoch " +
                  f"{np.argmin(valLoss)+1}",
            color="black", linestyle="--", alpha=0.5)
    ax.set_xlabel("Epoch")
    ax.set_ylabel(f"{plotMetric}")
    plt.legend()
    plt.tight_layout()
    fig.savefig(logDir + f"/Plots/trainVal_{plotMetric}.pdf")
    plt.close("all")


def prepareDataROCCurvePlotting(preds, weights, plotData, maskDict, maskKey):
    """
    Prepare data such that it can be used by the ROC curve plotting functions.

    Args
    ----
        preds: *array* or *dict*
            See in top doc string.
            Both single and multiple model plotting possible.
        weights: *array*
            Weights to be applied to each data point. If None weights equal to
            one will be used for every data point.
        plotData: *dict*
            See in top doc string.
            No variables needed for this part, but for the
            functions that are calling this function. Needs at least one
            variable because otherwise the function will crash.
        maskDict: *dict*
            Dictionary holding different masks such as a mask to only plot the
            ROC curve vs one background.
        maskKey: *str*
            Key for the mask which is to be used out of maskDict.

    Returns
    -------
        preds: *array* or *dict*
            See in top doc string.
            Applied mask from mask dict
        weights: *array*
            Ready to use weights.
        plotData: *dict*
            See in top doc string.
            Applied mask from mask dict

    """
    # If multiple models should be plotted
    if type(preds) == dict:
        for key in preds:
            # mask the predictions of each model
            preds[key]["preds"] = preds[key]["preds"][maskDict[maskKey] == 1]
        if weights is None:
            # Create weights array with just ones if no weights are given
            for key in preds:
                weights = np.ones(preds[key]["preds"].shape)
        else:
            # mask the weights array
            weights = weights[maskDict[maskKey] == 1]
    else:
        # mask the predictions
        preds = preds[maskDict[maskKey]]
        if weights is None:
            # Create array with just ones for the weights
            weights = np.ones(preds.shape)
        else:
            # Mask the weights array
            weights = weights[maskDict[maskKey] == 1]
    # Mask each variable which is inside plotData
    for key in plotData:
        plotData[key] = plotData[key][maskDict[maskKey] == 1]

    return preds, weights, plotData


def plotOneBinROCCurve(logDir, plotData, preds, maskDict=None, maskKey=None,
                       weights=None, text=None):
    """
    Plot ROC curves for one or multiple models using the test set.

    Args
    ----
        logDir: *str*
            See in top doc string.
        plotData: *dict*
            See in top doc string.
            Needed variables: truth, LHVeryLoose, LHLoose,
            LHLooseBL, LHMedium, LHTight
        preds: *dict* or *array*
            See in top doc string.
            Both single and multiple model plotting possible.
        maskDict: *dict*
            Dictionary holding different masks such as a mask to only plot the
            ROC curve vs one background.
        maskKey: *str*
            Key for the mask which is to be used out of maskDict.
        weights: *array*
            Weights to applied during the plotting. Pass None if no weights
            should be applied.

    """
    if maskDict and maskKey:
        preds, weights, plotData = prepareDataROCCurvePlotting(preds, weights,
                                                               plotData,
                                                               maskDict,
                                                               maskKey)
    elif weights is None:
        weights = np.ones(plotData["truth"].shape)

    fig, ax = plt.subplots()

    # Calculate signal and bkg efficiencies for each of the LH working points
    sigEff = []
    bkgEff = []
    for point in ["LHLoose", "LHMedium",  # "LHVeryLoose", "LHLooseBL",
                  "LHTight"]:
        sigEff.append(np.sum(weights[(plotData["truth"] == 1)
                                     & (plotData[point] == 1)]) /
                      np.sum(weights[plotData["truth"] == 1]))
        bkgEff.append(np.sum(weights[(plotData["truth"] == 0)
                                     & (plotData[point] == 1)]) /
                      np.sum(weights[plotData["truth"] == 0]))

    # Plot LH points as black crosses
    sigEff = np.array(sigEff)
    bkgEff = np.array(bkgEff)
    minEff = np.min(sigEff) - 0.1 * (1 - np.min(sigEff))

    # Plot roc curves for given predictions using sklearns roc_curve
    if type(preds) == dict:
        # If multiple predictions are given loop over them and plot all
        for key in preds:
            fpr, tpr, _ = roc_curve(plotData["truth"], preds[key]["preds"],
                                    sample_weight=weights)
            ax.plot(tpr[tpr > minEff], 1./fpr[tpr > minEff],
                    label=preds[key]["label"])
    else:
        # If only a single model plot only this one
        fpr, tpr, _ = roc_curve(plotData["truth"], preds,
                                sample_weight=weights)
        ax.plot(tpr[tpr > minEff], 1./fpr[tpr > minEff], label="DNN")

    ax.plot(sigEff[sigEff > minEff], 1./bkgEff[sigEff > minEff], "X",
            color="black", label="LH WP")

    if text is not None:
        ax.text(0.02, 0.02, text, transform=ax.transAxes,
                verticalalignment="bottom")

    # Cosmetics of plot
    ax.set_xlim([minEff, 1])
    ax.set_xlabel("Signal Efficiency")
    ax.set_ylabel("Background Rejection")

    plt.legend(loc='upper right')
    plt.tight_layout()

    plt.savefig(logDir + f"/Plots/ROC/oneBinROCCurve{maskKey}.pdf" if maskKey
                else logDir + f"/Plots/ROC/oneBinROCCurve.pdf")
    plt.close("all")


def plotBinnedROCCurve(logDir, plotData, preds, etBins, etaBins, maskDict=None,
                       maskKey=None, weights=None, text=None):
    """
    Plot ROC curves for one model in bins of et and eta using the test set.

    Args
    ----
        logDir: *str*
            See in top doc string.
        plotData: *dict*
            See in top doc string.
            Needed variables: truth, LHVeryLoose, LHLoose, LHLooseBL, LHMedium,
            LHTight, et, eta
        preds: *dict* or *array*
            See in top doc string.
            Both single and multiple model plotting possible.
        etBins: *array*
            See in top doc string.
        etaBins: *array*
            See in top doc string.
        maskDict: *dict*
            Dictionary holding different masks such as a mask to only plot the
            ROC curve vs one background.
        maskKey: *str*
            Key for the mask which is to be used out of maskDict.
        weights: *array*
            Weights to applied during the plotting. Pass None if no weights
            should be applied.

    """
    if maskDict and maskKey:
        preds, weights, plotData = prepareDataROCCurvePlotting(preds, weights,
                                                               plotData,
                                                               maskDict,
                                                               maskKey)
    elif weights is None:
        weights = np.ones(plotData["truth"].shape)

    # Iterate over the single et-/etaBins
    for i, (et_low, et_up) in enumerate(convertBins(etBins)):
        for j, (eta_low, eta_up) in enumerate(convertBins(etaBins)):
            # if et_low > 17:
            #     continue
            # mask to select only electron candidates in this et/etaBin
            mask = (
                     (plotData["et"] > et_low) & (plotData["et"] <= et_up)
                     & (np.abs(plotData["eta"]) > eta_low)
                     & (np.abs(plotData["eta"]) <= eta_up)
                   )

            fig, ax = plt.subplots()

            sigEff = []
            bkgEff = []
            # Calculate signal and bkg effs for each of the LH working points
            for point in ["LHLoose", "LHMedium", "LHTight"]:
                sigEff.append(np.sum(weights[(plotData["truth"] == 1)
                              & (mask == 1) & (plotData[point] == 1)]) /
                              np.sum(weights[(plotData["truth"] == 1)
                                             & (mask == 1)]))
                bkgEff.append(np.sum(weights[(plotData["truth"] == 0)
                              & (mask == 1) & (plotData[point] == 1)]) /
                              np.sum(weights[(plotData["truth"] == 0)
                                             & (mask == 1)]))

            # Plot LH points as black crosses
            sigEff = np.array(sigEff)
            bkgEff = np.array(bkgEff)
            minEff = np.min(sigEff) - 0.1 * (1 - np.min(sigEff))

            # Plot roc curves for given predictions using sklearns roc_curve
            for key in preds:
                fpr, tpr, _ = roc_curve(plotData["truth"][mask],
                                        preds[key]["preds"][mask],
                                        sample_weight=weights[mask])
                ax.plot(tpr[tpr > minEff], 1./fpr[tpr > minEff],
                        label=preds[key]["label"])

            ax.plot(sigEff[sigEff > minEff], 1./bkgEff[sigEff > minEff], "X",
                    color="black", label="LH WP")

            # Cosmetics of plot
            ax.set_xlim([minEff, 1])
            ax.set_xlabel("Signal Efficiency")
            ax.set_ylabel("Background Rejection")

            # TODO need to adjust position at some point
            pltText = (rf"${et_low} < E_T\ [\mathrm{{GeV}}] \leq $" +
                       rf"${et_up}$" + "\n" +
                       rf"${eta_low} < |\eta| \leq {eta_up}$")
            if text is not None:
                pltText = pltText + "\n" + text
            if maskKey is not None:
                pltText = pltText + "\n" + f"Background only {maskKey}"

            ax.text(0.02, 0.02, pltText,
                    transform=ax.transAxes,
                    verticalalignment="bottom")

            plt.legend(loc="upper right")
            plt.tight_layout()

            plt.savefig(logDir + f"/Plots/ROC/Binned/binnedROCCurve{maskKey}" +
                        f"_et{et_low}_{et_up}_eta{eta_low}_{eta_up}.pdf"
                        if maskKey else logDir +
                        f"/Plots/ROC/Binned/binnedROCCurve_et" +
                        f"{et_low}_{et_up}_eta{eta_low}_{eta_up}.pdf")
            plt.close("all")


def plotROCRatioOneBin(logDir, plotData, preds, maskDict=None, maskKey=None,
                       weights=None):
    """
    Plot ROC curves and a ratio plot of the ROC curves of multiple models.

    Args
    ----
        logDir: *str*
            See in top doc string.
        plotData: *dict*
            See in top doc string.
            Needed vars: truth
        preds: *dict*
            See in top doc string.
            Only plotting for multiple models makes sense and is supported.
        maskDict: *dict*
            Dictionary holding different masks such as a mask to only plot the
            ROC curve vs one background.
        maskKey: *str*
            Key for the mask which is to be used out of maskDict.
        weights: *array*
            Weights to applied during the plotting. Pass None if no weights
            should be applied.

    """
    # The function only makes sense when multiple models are given.
    if type(preds) != dict:
        return
    if maskDict and maskKey:
        preds, weights, plotData = prepareDataROCCurvePlotting(preds, weights,
                                                               plotData,
                                                               maskDict,
                                                               maskKey)
    elif weights is None:
        weights = np.ones(plotData["truth"].shape)

    fig, ax = plt.subplots(2, gridspec_kw={"height_ratios": [3, 1]},
                           sharex=True)

    # To make a ratio plot the fpr is needed at the same tpr values
    xVals = np.linspace(0.6, 1, 100)
    for i, key in enumerate(preds):
        fpr, tpr, _ = roc_curve(plotData["truth"], preds[key]["preds"],
                                sample_weight=weights)
        # Get the fpr at the values given by xVals to plot them and use them
        # for the ratio plot
        preds[key]["fpr"] = np.interp(xVals, tpr, fpr)
        if i == 0:
            # Remember the key of the first model plotted
            refKey = key
            # To have the colors matching between original and ratio plot, the
            # first model which is used as the reference needs to be plotted
            # in some given color, in this cas black
            ax[0].plot(xVals, 1. / preds[key]["fpr"],
                       label=preds[key]["label"], color="black")
        else:
            ax[0].plot(xVals, 1. / preds[key]["fpr"],
                       label=preds[key]["label"])
            ax[1].plot(xVals, np.divide(preds[refKey]["fpr"],
                                        preds[key]["fpr"]))

    # Cosmetics of plot
    ax[1].set_xlabel("Signal Efficiency")
    ax[1].set_ylabel(f"Ratio to first")  # {preds[refKey]['label']}")
    ax[0].set_ylabel("Background Rejection")
    ax[1].set_ylim([0.8, 1.2])

    ax[0].legend()
    plt.tight_layout()

    plt.savefig(logDir + f"/Plots/ROC/oneBinROCRatio{maskKey}.pdf" if maskKey
                else logDir + f"/Plots/ROC/oneBinROCRatio.pdf")
    plt.close("all")


def plotOneBinScore(logDir, truth, preds, classNames):
    """
    Plot the score distribution of the test set.

    Args
    ----
        logDir: *str*
            See in top doc string.
        truth: *array*
            Array with the truth information of the data points in the test
            set. Does not have to be only binary but can be multiclass as well.
        preds: *array*
            See in top doc string.
            Only single model plotting possible.
        classNames: *list of str*
            List of the names of the single classes corresponding to 0, 1, ...
            in the truth array

    """
    fig, ax = plt.subplots()
    for i in range(len(classNames)):
        ax.hist(preds[truth == i], bins=100, range=(0, 1), histtype="step",
                label=classNames[i], density=True)

    # Cosmetics of plot
    ax.set_yscale("log")
    ax.set_xlabel("DNN Score")
    ax.set_ylabel("Frequency")

    plt.tight_layout()
    positionLegend(fig, ax)
    plt.savefig(logDir +
                f"/Plots/Scores/oneBinScores{len(classNames)}Classes.pdf")
    plt.close("all")


def plotBinnedScore(logDir, plotData, truth, preds, etBins, etaBins,
                    classNames):
    """
    Plot the score distribution of the test set.

    Args
    ----
        logDir: *str*
            See in top doc string.
        plotData: *dict*
            See in top doc string.
            Needed variables: et, eta.
        truth: *array*
            Array with the truth information of the data points in the test
            set. Does not have to be only binary but can be multiclass as well.
        preds: *array*
            See in top doc string.
            Only single model plotting possible.
        etBins: *array*
            See in top doc string.
        etaBins: *array*
            See in top doc string.
        classNames: *list of str*
            List of the names of the single classes corresponding to 0, 1, ...
            in the truth array.

    """
    # Iterate over the single et-/etaBins
    for i, (et_low, et_up) in enumerate(convertBins(etBins)):
        for j, (eta_low, eta_up) in enumerate(convertBins(etaBins)):
            # mask to select only electron candidates in this et/etaBin
            mask = (
                     (plotData["et"] > et_low) & (plotData["et"] <= et_up)
                     & (np.abs(plotData["eta"]) > eta_low)
                     & (np.abs(plotData["eta"]) <= eta_up)
                   )

            fig, ax = plt.subplots()
            for i in range(len(classNames)):
                ax.hist(preds[(truth == i) & (mask == 1)], bins=100,
                        range=(0, 1), histtype="step", label=classNames[i],
                        density=True)

            # Cosmetics of plot
            ax.set_yscale("log")
            ax.set_xlabel("DNN Score")
            ax.set_ylabel("Frequency")

            ax.text(0.02, 0.98,
                    rf"${et_low} < E_T\ [\mathrm{{GeV}}] \leq {et_up}$" +
                    "\n" +
                    rf"${eta_low} < |\eta| \leq {eta_up}$",
                    transform=ax.transAxes,
                    verticalalignment="top")

            plt.tight_layout()
            positionLegend(fig, ax)
            plt.savefig(logDir + f"/Plots/Scores/Binned/binnedScores" +
                        f"et{et_low}_{et_up}_eta{eta_low}_{eta_up}_" +
                        f"{len(classNames)}Classes.pdf")
            plt.close("all")


def getSigAndBkgEffsAndErrs(preds, plotData, bins, varName, maskDict=None,
                            maskKey=None, sigEff=0.8):
    """
    Calculate Inclusive signal eff and bkg rejection in bins of some variables.

    Args
    ----
        preds: *array*
            See in top doc string.
            Only single model calculations possible.
        plotData: *dict*
            See in top doc string.
            Needed variables: truth, variable given by varName.
        bins: *array*
            Bins in which the sig eff and bkg rej should be calculated. Bins
            are given by putting the edges into one array, e.g.
            [0.0, 1.32, 1.57, 2.47] for eta bins.
        varName: *str*
            Variable name in which sig eff and bkg rej should be calculated.
        sigEff: *float*
            Inclusive signal efficiency that determines the cut value.

    Returns
    -------
        sigEffPart: *list*
            List containing the signal efficiencies in the given bins.
        sigErr: *list*
            List containing the uncertainties on the signal efficiencies in
            the given bins.
        bkgRej: *list*
            List containing the bkg rejection in the given bins.
        bkgErr: *list*
            List containing the uncertainties on the bkg rejection in
            the given bins.

    """
    # sort preds and get cutValue which corresponds to the given efficiency
    sortedPreds = np.sort(preds[(plotData["truth"] == 1) &
                                (maskDict[maskKey] == 1)]
                          if type(maskDict) == dict and type(maskKey) == str
                          else preds[(plotData["truth"] == 1)])
    cutValue = sortedPreds[int((1 - sigEff) * sortedPreds.shape[0])]

    sigEffPart, sigErr, bkgRej, bkgErr = [], [], [], []
    # Iterate over the bins and calculate sigEff, sigErr, bkgRej, and bkgErr
    for i, (low, up) in enumerate(convertBins(bins)):
        # Select only electron candidates in the given bin.
        mask = (
                 (np.abs(plotData[varName]) > low) &
                 (np.abs(plotData[varName]) <= up)
               )

        if type(maskDict) == dict and type(maskKey) == str:
            mask = (mask == 1) & (maskDict[maskKey] == 1)

        # Calculate numbers of selected/total signal/bkg electrons
        nSigSel = np.sum(preds[(plotData["truth"] == 1) & mask] > cutValue)
        nSigTot = np.sum([plotData["truth"][mask] == 1])
        nBkgSel = np.sum(preds[(plotData["truth"] == 0) & mask] > cutValue)
        nBkgTot = np.sum([plotData["truth"][mask] == 0])

        # Calculate signal efficiency and bkg rejection and their uncertainties
        sigEffPart.append(nSigSel / nSigTot)
        sigErr.append(sigEffPart[-1] * np.sqrt(1 / nSigSel + 1 / nSigTot))
        bkgRej.append(nBkgTot / nBkgSel)
        bkgErr.append(bkgRej[-1] * np.sqrt(1 / nBkgSel + 1 / nBkgTot))

    return sigEffPart, sigErr, bkgRej, bkgErr


def plotEfficienciesInclusiveSigEff(logDir, plotData, preds,
                                    etBins, etaBins, muBins, sigEff=0.8,
                                    maskDict=None, maskKey=None):
    """
    Plot signal and bkg efficiencies in bins for a chosen inclusive sig eff.

    Args
    ----
        logDir: *str*
            See in top doc string.
        plotData: *dict*
            See in top doc string.
            Needed variables: et, eta, truth.
        preds: *array*
            See in top doc string.
            Both single and multiple model plotting possible
        etBins: *array*
            See in top doc string.
        etaBins: *array*
            See in top doc string.
        sigEff: *float*
            Inclusive signal effficiency which should be selected. The default
            value is 0.8
    """
    # If more than one model plotted make a legend on the plot
    legend = type(preds) == dict

    # Calculate efficiencies/rejections in bins of et
    if type(preds) == dict:
        eff, rej = {}, {}
        for key in preds:
            (sigEffPart, sigErr,
             bkgRej, bkgErr) = getSigAndBkgEffsAndErrs(preds[key]["preds"],
                                                       plotData, etBins, "et",
                                                       maskDict, maskKey)

            eff[preds[key]["label"]] = [sigEffPart, sigErr]
            rej[preds[key]["label"]] = [bkgRej, bkgErr]
    else:
        (sigEffPart, sigErr,
         bkgRej, bkgErr) = getSigAndBkgEffsAndErrs(preds, plotData,
                                                   etBins, "et")

        eff = {"Signal": [sigEffPart, sigErr]}
        rej = {"Bkg": [bkgRej, bkgErr]}

    (binCenter, binLength) = getBinCenter(etBins)
    # Adjust the last in that it fits nicely on one plot
    binCenter[-1] = binCenter[-2] + binLength[-2] + 20
    binLength[-1] = 20

    incString = "\nInclusive Signal " + \
                rf"Efficiency $\epsilon_\mathrm{{sig}}={sigEff}$"

    if type(maskDict) == dict and type(maskKey) == str:
        plotErrorBarPlot(binCenter, binLength, rej,
                         logDir+f"/Plots/InclusiveSigEff/" +
                         "inclusiveBkgRejVsEt{maskKey}.pdf",
                         legend=legend,
                         xlabel=r"$E_T$ [GeV]",
                         ylabel="1 / Background Efficiency",
                         text=rf"Inclusive in $|\eta|$" + incString +
                              "\nLast bin contains all events\nuntil " +
                              f"{etBins[-1]} GeV\nOnly {maskKey} bkg")
    else:
        plotErrorBarPlot(binCenter, binLength, eff,
                         logDir+f"/Plots/InclusiveSigEff/" +
                         "inclusiveSigEffVsEt.pdf",
                         legend=legend,
                         xlabel=r"$E_T$ [GeV]", ylabel="Signal Efficiency",
                         text=rf"Inclusive in $|\eta|$" + incString +
                              "\nLast bin contains all events\nuntil " +
                              f"{etBins[-1]} GeV")

        plotErrorBarPlot(binCenter, binLength, rej,
                         logDir+f"/Plots/InclusiveSigEff/" +
                         "inclusiveBkgRejVsEt.pdf",
                         legend=legend,
                         xlabel=r"$E_T$ [GeV]",
                         ylabel="1 / Background Efficiency",
                         text=rf"Inclusive in $|\eta|$" + incString +
                              "\nLast bin contains all events\nuntil " +
                              f"{etBins[-1]} GeV")

    # Calculate efficiencies/rejections in bins of eta
    if type(preds) == dict:
        eff, rej = {}, {}
        for key in preds:
            (sigEffPart, sigErr,
             bkgRej, bkgErr) = getSigAndBkgEffsAndErrs(preds[key]["preds"],
                                                       plotData, etaBins,
                                                       "eta", maskDict,
                                                       maskKey)

            eff[preds[key]["label"]] = [sigEffPart, sigErr]
            rej[preds[key]["label"]] = [bkgRej, bkgErr]
    else:
        (sigEffPart, sigErr,
         bkgRej, bkgErr) = getSigAndBkgEffsAndErrs(preds, plotData,
                                                   etaBins, "eta")

        eff = {"Signal": [sigEffPart, sigErr]}
        rej = {"Bkg": [bkgRej, bkgErr]}

    (binCenter, binLength) = getBinCenter(etaBins)

    if type(maskDict) == dict and type(maskKey) == str:
        plotErrorBarPlot(binCenter, binLength, rej,
                         logDir+f"/Plots/InclusiveSigEff/" +
                         "inclusiveBkgRejVsEta{maskKey}.pdf",
                         legend=legend,
                         xlabel=r"$|\eta|$",
                         ylabel="1 / Background Efficiency",
                         text=rf"Inclusive in $E_T$" + incString +
                              f"\nOnly {maskKey} bkg")
    else:
        plotErrorBarPlot(binCenter, binLength, eff,
                         logDir+f"/Plots/InclusiveSigEff/" +
                         "inclusiveSigEffVsEta.pdf",
                         legend=legend,
                         xlabel=r"$|\eta|$", ylabel="Signal Efficiency",
                         text=rf"Inclusive in $E_T$" + incString)

        plotErrorBarPlot(binCenter, binLength, rej,
                         logDir+f"/Plots/InclusiveSigEff/" +
                         "inclusiveBkgRejVsEta.pdf",
                         legend=legend,
                         xlabel=r"$|\eta|$",
                         ylabel="1 / Background Efficiency",
                         text=rf"Inclusive in $E_T$" + incString)
    # Calculate efficiencies/rejections in bins of mu
    if type(preds) == dict:
        eff, rej = {}, {}
        for key in preds:
            (sigEffPart, sigErr,
             bkgRej, bkgErr) = getSigAndBkgEffsAndErrs(preds[key]["preds"],
                                                       plotData, muBins,
                                                       "mu", maskDict, maskKey)

            eff[preds[key]["label"]] = [sigEffPart, sigErr]
            rej[preds[key]["label"]] = [bkgRej, bkgErr]
    else:
        (sigEffPart, sigErr,
         bkgRej, bkgErr) = getSigAndBkgEffsAndErrs(preds, plotData,
                                                   muBins, "mu")

        eff = {"Signal": [sigEffPart, sigErr]}
        rej = {"Bkg": [bkgRej, bkgErr]}

    (binCenter, binLength) = getBinCenter(muBins)

    if type(maskDict) == dict and type(maskKey) == str:
        plotErrorBarPlot(binCenter, binLength, rej,
                         logDir+f"/Plots/InclusiveSigEff/" +
                         "inclusiveBkgRejVsMu{maskKey}.pdf",
                         legend=legend,
                         xlabel=r"$<\mu>$", ylabel="1 / Background Efficiency",
                         text=rf"Inclusive in $E_T$ and $\eta$" + incString +
                              f"\nOnly {maskKey} bkg")
    else:
        plotErrorBarPlot(binCenter, binLength, eff,
                         logDir+f"/Plots/InclusiveSigEff/" +
                         "inclusiveSigEffVsMu.pdf",
                         legend=legend,
                         xlabel=r"$<\mu>$", ylabel="Signal Efficiency",
                         text=rf"Inclusive in $E_T$ and $\eta$" + incString)

        plotErrorBarPlot(binCenter, binLength, rej,
                         logDir+f"/Plots/InclusiveSigEff/" +
                         "inclusiveBkgRejVsMu.pdf",
                         legend=legend,
                         xlabel=r"$<\mu>$", ylabel="1 / Background Efficiency",
                         text=rf"Inclusive in $E_T$ and $\eta$" + incString)


def writeCutValueFile(logDir, transCutVals, etBins, etaBins):
    """
    Save calculated cut values in the same format as a config file for athena.

    Args
    ----
        logDir: *str*
            See in top doc string.
        transCutVals: *array*
            Array containing the cut values on the logit transformed NN output.
        etBins: *array*
            See in top doc string.
        etaBins: *array*
            See in top doc string.

    """
    cutValueFile = open(logDir + "/cutValuesConf.txt", "w")
    for i in range(len(etBins) - 1):
        if i == 0:
            cutValueFile.write("CutSelector:")
        else:
            cutValueFile.write("\n+CutSelector:")
        for j in range(len(etaBins) - 1):
            if i == len(etBins) - 1 and j == len(etaBins) - 1:
                cutValueFile.write(f"  {transCutVals[i][j]}")
            else:
                cutValueFile.write(f"  {transCutVals[i][j]};")


def plotCutValues(logDir, transCutVals, etBins, etaBins):
    """
    Plot cut values in bins of et and eta.

    Args
    ----
        logDir: *str*
            See in top doc string.
        transCutVals: *array*
            Array containing the cut values on the logit transformed NN output
            to be plotted.
        etBins: *array*
            See in top doc string.
        etaBins: *array*
            See in top doc string.

    """
    etBinsCenter = getBinCenter(etBins)
    etaBinsCenter = getBinCenter(etaBins)
    outDir = logDir + "/Plots/CutValues/"

    for i in range(len(etaBins)-1):
        values = {"cutValues": [transCutVals[:, i], transCutVals[:, i]*1e-3]}
        plotErrorBarPlot(etBinsCenter[0], etBinsCenter[1], values,
                         outDir + f"cutValues_eta{etaBins[i]}.pdf",
                         legend=False, xlabel=r"$E_T$ [GeV]",
                         ylabel="Cut Value")

    for i in range(len(etBins)-1):
        values = {"cutValues": [transCutVals[i, :], transCutVals[i, :]*1e-3]}
        plotErrorBarPlot(etaBinsCenter[0], etaBinsCenter[1], values,
                         outDir + f"cutValues_et{etBins[i]}.pdf",
                         legend=False, xlabel=r"$|\eta|$",
                         ylabel="Cut Value")


def calculateCutValues(plotData, preds, etBins, etaBins, workingPoint):
    """
    Calculate cut values on test set such that sig eff matches LH workingPoint.

    Args
    ----
        plotData: *dict*
            See in top doc string.
            Variables needed: eta, et, pileupWeight, truth, variable given in
            workingPoint.
        preds: *array*
            See in top doc string.
            Only single model at a time possible.
        etBins: *array*
            See in top doc string.
        etaBins: *array*
            See in top doc string.
        workingPoint: *str*
            Working point to which the signal efficiency should be matched.

    Returns
    -------
        transCutVals: *array*
            Array containing cut values on the logit transformed NN output
            which match the signal efficiency of the desired working point.

    """
    cutValArr = np.ones((len(etBins) - 1, len(etaBins) - 1))

    for i in range(len(etBins) - 1):
        for j in range(len(etaBins) - 1):
            mask = (
                    (np.abs(plotData["eta"]) > etaBins[j])
                    & (np.abs(plotData["eta"]) <= etaBins[j+1])
                    & (np.abs(plotData["et"]) > etBins[i])
                    & (np.abs(plotData["et"]) <= etBins[i+1])
                    & (plotData["truth"] == 1)
                   )

            weights = plotData["pileupWeight"][mask]

            weightMask = plotData[workingPoint][mask == 1] == 1
            looseSigEff = np.sum(weights[weightMask == 1]) / np.sum(weights)

            predictions = preds[mask == 1]
            indices = np.argsort(-predictions)

            weights = weights / np.sum(weights)
            cumsum = np.cumsum(weights[indices])
            cutIndex = 0
            for k in range(len(cumsum)):
                if cumsum[k] <= looseSigEff:
                    cutIndex = k

            cutVal = predictions[indices][cutIndex]
            cutValArr[i][j] = cutVal

    transCutVals = -np.log(1.0 / cutValArr - 1.0) / 10.0

    return transCutVals


def calculateAndPlotCutValues(logDir, plotData, preds, etBins, etaBins,
                              workingPoint="LHLoose"):
    """
    Calculate, plot, and write cut values for a given model.

    Args
    ----
        logDir: *str*
            See in top doc string.
            Directory where to save the files containing the
            cut values and the plots.
        plotData: *dict*
            See in top doc string.
            Needed variables: truth, et, eta, variable given by workingPoint
        preds: *array*
            See in top doc string.
            Only single model possible at the moment
        etBins: *array*
            See in top doc string.
        etaBins: *array*
            See in top doc string.
        workingPoint: *str*
            For which LH working point cut values with equivalent signal
            efficiencies should be calculated.

    """
    transCutVals = calculateCutValues(plotData, preds, etBins, etaBins,
                                      workingPoint)

    np.save(logDir + f"/transformedCutValues{workingPoint}.npy", transCutVals)

    writeCutValueFile(logDir, transCutVals, etBins, etaBins)

    plotCutValues(logDir, transCutVals, etBins, etaBins)


# TODO Test if try except statement woorks
def makePredsToBools(logDir, plotData, preds, etBins, etaBins,
                     workingPoint="LHLoose"):
    """
    Load cut values and turn predicitions into bools depending on cut value.

    Args
    ----
        logDir: *str*
            See in top doc string.
            This should always be the logDir of the network since here the
            cutValues either need to be already saved or will be saved.
        plotData: *dict*
            See in top doc string.
            Needed variables: eta, et
        preds: *array*
            See in top doc string.
            Only single model at the moment.
        etBins: *array*
            See in top doc string.
        etaBins: *array*
            See in top doc string.
        workingPoint: *str*
            Cut values which give the same signal efficiency as this working
            point will be used.

    Returns
    -------
        boolPreds: *array*
            Array of bools whether the cut was passed or not.

    """
    # TODO this is not nice but if cut values are calculated with different
    # TODO bins than the ones given here everything is fucked up
    etBins = np.array([15, 20, 25, 30, 35, 40, 45, 60, 80, 150, 250, 5000])
    etaBins = np.array([0.0, 0.1, 0.6, 0.8, 1.15, 1.37, 1.52, 1.81, 2.01,
                        2.37, 2.47])
    if os.path.isfile(logDir + f"/transformedCutValues{workingPoint}.npy"):
        # Load cut values
        transCutVals = np.load(logDir +
                               f"/transformedCutValues{workingPoint}.npy")
    else:
        # If cut values not alreaddy calculated and saved, do that
        transCutVals = calculateCutValues(plotData, preds, etBins, etaBins,
                                          workingPoint)
        np.save(logDir + f"/transformedCutValues{workingPoint}.npy",
                transCutVals)

    # Make predictions not equal 0 or 1 to avoid infinities (1/0, log(0))
    preds[preds > 1 - 1e-5] = 1 - 1e-5
    preds[preds < 1e-5] = 1e-5
    preds = -np.log(1.0 / preds - 1.0) / 10.0
    boolPreds = np.zeros(preds.shape)

    # Loop through the et- and etaBins since cut value depends on these
    for i in range(len(etBins) - 1):
        for j in range(len(etaBins) - 1):
            mask = (
                    (np.abs(plotData["eta"]) > etaBins[j])
                    & (np.abs(plotData["eta"]) <= etaBins[j+1])
                    & (np.abs(plotData["et"]) > etBins[i])
                    & (np.abs(plotData["et"]) <= etBins[i+1])
                   )

            boolPreds[(mask == 1) & (preds >= transCutVals[i][j])] = 1

    return boolPreds


def getBkgRejBins(plotData, boolPreds, etBins, etaBins):
    """
    Calcualte bkg rejection based on predictions given as bools.

    Args
    ----
        plotData: *dict*
            See in top doc string.
            Needed variables: et, eta, pileupWeight
        boolPreds: *array*
            Predictions given as bools. This can be either the predictions of
            an NN transformed into bools or the LH decisions.
        etBins: *array*
            See in top doc string.
        etaBins: *array*
            See in top doc string.

    Returns
    -------
        bkgRej: *array*
            Backround rejection in each of the et/etaBins
        bkgRejErr: *array*
            Uncertainty of the bkg rejection in each of the et/etaBins

    """
    # Create the arrays to be filled
    bkgRej = np.zeros((len(etBins)-1, len(etaBins)-1))
    bkgRejErr = np.zeros((len(etBins)-1, len(etaBins)-1))

    for i in range(len(etBins) - 1):
        for j in range(len(etaBins) - 1):
            mask = (
                    (np.abs(plotData["eta"]) > etaBins[j])
                    & (np.abs(plotData["eta"]) <= etaBins[j+1])
                    & (np.abs(plotData["et"]) > etBins[i])
                    & (np.abs(plotData["et"]) <= etBins[i+1])
                    & (plotData["truth"] == 0)
                   )

            # Get weights of bkg events in this bin
            weights = plotData["pileupWeight"][mask]

            # Get total bkg weights in the bin and the selected bkg weights
            nBkgTot = np.sum(weights)
            nBkgSel = np.sum(weights[boolPreds[mask] == 1])

            # Calculate the rejection and the corresponding Uncertainty
            bkgRej[i][j] = nBkgTot / nBkgSel
            bkgRejErr[i][j] = bkgRej[i][j] * np.sqrt(1 / nBkgSel + 1 / nBkgTot)

    return bkgRej, bkgRejErr


def plotRejectionPlots(logDir, plotData, preds, etBins, etaBins,
                       workingPoint="LHLoose"):
    """
    Plot rejection of model in bins of et or eta.

    Args
    ----
        logDir: *str*
            See in top doc string.
        plotData: *dict*
            See in top doc string.
            Needed variables: et, eta, truth, pileupWeight,
            variable given by workingPoint
        preds: *array*
            See in top doc string.
            Single and multiple Models are plttable
        etBins: *array*
            See in top doc string.
        etaBins: *array*
            See in top doc string.
        workingPoint: *str*
            Which working point is used to load the cut values and which
            rejection is plotted as reference.

    """
    outDir = logDir + "/Plots/Rejection/"

    # Get rejections and uncertainties for working point
    bkgLH, bkgLHErr = getBkgRejBins(plotData, plotData[workingPoint], etBins,
                                    etaBins)

    if type(preds) == dict:
        bkgRej, bkgRejErr = {}, {}
        for key in preds:
            # Turn predictions into bools, key is the logDir of the network
            boolPreds = makePredsToBools(key, plotData, preds[key]["preds"],
                                         etBins, etaBins, workingPoint)

            # Get rejections and uncertainties for predictions
            (bkgRej[preds[key]["label"]],
             bkgRejErr[preds[key]["label"]]) = getBkgRejBins(plotData,
                                                             boolPreds, etBins,
                                                             etaBins)

    else:
        # Turn predictions into bools
        boolPreds = makePredsToBools(logDir, plotData, preds, etBins, etaBins,
                                     workingPoint)

        # Get rejections and uncertainties for predictions
        bkgRej, bkgRejErr = getBkgRejBins(plotData, boolPreds, etBins, etaBins)

    # Get Bin centers and lengths for the errorvar plots
    etBinsCenter = getBinCenter(etBins)
    etaBinsCenter = getBinCenter(etaBins)
    etBinsCenter[0][-1] = etBinsCenter[0][-2] + etBinsCenter[1][-2] + 20
    etBinsCenter[1][-1] = 20

    for i in range(len(etaBins)-1):
        reference = {workingPoint: [bkgLH[:, i], bkgLHErr[:, i]]}
        if type(bkgRej) == dict:
            models = {}
            for key in bkgRej:
                models[key] = [bkgRej[key][:, i], bkgRejErr[key][:, i]]
        else:
            models = {"Model": [bkgRej[:, i], bkgRejErr[:, i]]}

        plotErrorBarRatioPlot(etBinsCenter[0], etBinsCenter[1], reference,
                              models, outDir + f"/rejection{workingPoint}_" +
                              f"eta_{etaBins[i]}_{etaBins[i+1]}.pdf",
                              xlabel=r"$E_T$",
                              ylabelTop="1 / Background Efficiency",
                              ylabelBot="Ratio to LH",
                              text=f"Signal efficiencies \ncorresponding to " +
                                   f"{workingPoint}\n" +
                                   rf"${etaBins[i]} < |\eta| \leq " +
                                   rf"{etaBins[i+1]}$" + "\nLast bin " +
                                   f"contains all events until {etBins[-1]} " +
                                   "GeV",
                              ylim=[0, 3])

    for i in range(len(etBins)-1):
        reference = {workingPoint: [bkgLH[i, :], bkgLHErr[i, :]]}
        if type(bkgRej) == dict:
            models = {}
            for key in bkgRej:
                models[key] = [bkgRej[key][i, :], bkgRejErr[key][i, :]]
        else:
            models = {"Model": [bkgRej[i, :], bkgRejErr[i, :]]}

        plotErrorBarRatioPlot(etaBinsCenter[0], etaBinsCenter[1], reference,
                              models, outDir + f"/rejection{workingPoint}_et" +
                              f"_{etBins[i]}_{etBins[i+1]}.pdf",
                              xlabel=r"$|\eta|$",
                              ylabelTop="1 / Background Efficiency",
                              ylabelBot="Ratio to LH",
                              text=f"Signal efficiencies \ncorresponding to " +
                                   f"{workingPoint}\n" +
                                   rf"${etBins[i]} < E_T\ [\mathrm{{GeV}}]" +
                                   rf"\leq {etBins[i+1]}$",
                              ylim=[0, 3])


def getEffsVsfMu(plotData, boolPreds, etBins, etaBins, muBins, bkg=False):
    """
    Calculate the signal efficiencies/bkg rejections in bins of et/eta/mu.

    Args
    ----
        plotData: *dict*
            See in top doc string.
            Needed variables: et, eta, mu, truth, pileupWeight
        boolPreds: *array*
            Predicitions whther points are signal or bkg given as bools.
        etBins: *array*
            See in top doc string.
        etaBins: *array*
            See in top doc string.
        muBins: *array*
            Same format as etBins and etaBins ut specifying the bin edges in
            mu.
        bkg: *bool*
            Whether signal efficiencies (true) or bkacground rejections (false)
            should be calculated.

    Returns
    -------
        eff: *array*
            Signal efficiencies/bkg rejections in bins of et/eta/mu.
        err: *array*
            Uncertainty on the signal efficiency/bkg rejection in bins of
            et/eta/mu.

    """
    # Create the arrays to be filled
    eff = np.zeros((len(etBins)-1, len(etaBins)-1, len(muBins)-1))
    err = np.zeros((len(etBins)-1, len(etaBins)-1, len(muBins)-1))

    truthValue = 0 if bkg else 1
    for i in range(len(etBins) - 1):
        for j in range(len(etaBins) - 1):
            for k in range(len(muBins) - 1):
                mask = (
                        (np.abs(plotData["eta"]) > etaBins[j])
                        & (np.abs(plotData["eta"]) <= etaBins[j+1])
                        & (np.abs(plotData["et"]) > etBins[i])
                        & (np.abs(plotData["et"]) <= etBins[i+1])
                        & (plotData["mu"] > muBins[k])
                        & (plotData["mu"] <= muBins[k+1])
                        & (plotData["truth"] == truthValue)
                       )

                # Get weights of sig events in this bin
                weights = plotData["pileupWeight"][mask]

                # Get total weights in the bin and the selected weights
                nTot = np.sum(weights)
                nSel = np.sum(weights[boolPreds[mask] == 1])

                # Calculate the efficiency and the corresponding uncertainty
                if not bkg:
                    eff[i][j][k] = nSel / nTot
                else:
                    eff[i][j][k] = nTot / nSel
                err[i][j][k] = eff[i][j][k] * np.sqrt(1. / nSel + 1. / nTot)

    return eff, err


def getEffRejMu(logDir, plotData, preds, workingPoint,
                etBins, etaBins, muBins, bkg=False):
    """
    Get signal efficiencies/bkg rejections for LH and equivalent for NN.

    Args
    ----
        logDir: *str*
            See in top doc string.
        plotData: *dict*
            See in top doc string.
            Needed variables: et, eta, mu, truth, pileupWeight, variable named
            in workingPoint.
        preds: *array* or *dict*
            See in top doc string.
            Signle and multiple model plotting possible.
        workingPoint: *str*
            Which working point is used to load the cut values and which
            efficiencies are plotted as reference.
        etBins: *array*
            See in top doc string.
        etaBins: *array*
            See in top doc string.
        muBins: *array*
            Same format as et and etaBins but specifying the bin edges for mu.
        bkg: *bool*
            Whether to calculate signale efficiencies (False) or bkg rejections
            (True).

    Returns
    -------
        LHEff: *array*
            Array containing efficiencies/rejections in the et/eta/mu bins for
            the LH working point.
        LHErr: *array*
            Uncertaininties for LHEff.
        eff: *array* or *dict*
            Array containing efficiencies/rejections in the et/eta/mu bins for
            the predictions of the NN. Depending on the structure of preds this
            will be an array or a dict.
        err: *array* or *dict*
            Uncertaininties for eff.

    """
    LHEff, LHErr = getEffsVsfMu(plotData, plotData[workingPoint], etBins,
                                etaBins, muBins, bkg=bkg)

    if type(preds) == dict:
        eff, err = {}, {}
        for key in preds:
            # Turn predictions into bools, key is the logDir of the network
            boolPreds = makePredsToBools(key, plotData, preds[key]["preds"],
                                         etBins, etaBins, workingPoint)

            # Get efficiencies and uncertainties for predictions
            (eff[preds[key]["label"]],
             err[preds[key]["label"]]) = getEffsVsfMu(plotData, boolPreds,
                                                      etBins, etaBins, muBins,
                                                      bkg=bkg)

    else:
        # Turn predictions into bools
        boolPreds = makePredsToBools(logDir, plotData, preds, etBins, etaBins,
                                     workingPoint)

        # Get efficiencies and uncertainties for predictions
        eff, err = getEffsVsfMu(plotData, boolPreds, etBins,
                                etaBins, muBins, bkg=bkg)

    return LHEff, LHErr, eff, err


def plotPerformanceVsMu(logDir, plotData, preds, etBins, etaBins, muBins,
                        workingPoint="LHLoose"):
    """
    Plot signal efficiency vs Mu in bins of et and eta with a constant cut.

    Args
    ----
        logDir: *str*
            See in top doc string.
        plotData: *dict*
            See in top doc string.
            Needed variables: et, eta, mu, truth, pileupWeight, variable named
            in workingPoint.
        preds: *array* or *dict*
            See in top doc string.
            Signle and multiple model plotting possible.
        etBins: *array*
            See in top doc string.
        etaBins: *array*
            See in top doc string.
        muBins: *array*
            Same format as et and etaBins but specifying the bin edges for mu.
        workingPoint: *str*
            Which working point is used to load the cut values and which
            efficiencies are plotted as reference.

    """
    outDir = logDir + "/Plots/PerformanceVsMu/"

    (LHsigEff, LHsigErr,
     signalEff, signalErr) = getEffRejMu(logDir, plotData, preds, workingPoint,
                                         etBins, etaBins, muBins, bkg=False)

    # Get Bin centers and lengths for the errorbar plots
    muBinsCenter = getBinCenter(muBins)

    for i in range(len(etBins)-1):
        for j in range(len(etaBins)-1):
            reference = {workingPoint: [LHsigEff[i, j, :], LHsigErr[i, j, :]]}
            if type(signalEff) == dict:
                models = {}
                for key in signalEff:
                    models[key] = [signalEff[key][i, j, :],
                                   signalErr[key][i, j, :]]
            else:
                models = {"Model": [signalEff[i, j, :], signalErr[i, j, :]]}

            plotErrorBarRatioPlot(muBinsCenter[0], muBinsCenter[1], reference,
                                  models, outDir + f"/SignalEffVsMu" +
                                  f"{workingPoint}_et_{etBins[i]}_" +
                                  f"{etBins[i+1]}_eta_{etaBins[j]}_" +
                                  f"{etaBins[j+1]}.pdf",
                                  xlabel=r"$<\mu>$",
                                  ylabelTop="Signal Efficiency",
                                  ylabelBot="Ratio to LH",
                                  text=f"Overall signal efficiency \n" +
                                       f"corresponding to {workingPoint}\n" +
                                       rf"${etBins[i]} < E_T\ $" +
                                       rf"$[\mathrm{{GeV}}]$" +
                                       rf"$\leq {etBins[i+1]}$" + "\n" +
                                       rf"${etaBins[j]} < |\eta| \leq $" +
                                       rf"${etaBins[j+1]}$")

    (LHbkgRej, LHbkgErr,
     bkgRej, bkgErr) = getEffRejMu(logDir, plotData, preds, workingPoint,
                                   etBins, etaBins, muBins, bkg=True)

    # Get Bin centers and lengths for the errorbar plots
    muBinsCenter = getBinCenter(muBins)

    for i in range(len(etBins)-1):
        for j in range(len(etaBins)-1):
            reference = {workingPoint: [LHbkgRej[i, j, :], LHbkgErr[i, j, :]]}
            if type(bkgRej) == dict:
                models = {}
                for key in signalEff:
                    models[key] = [bkgRej[key][i, j, :],
                                   bkgErr[key][i, j, :]]
            else:
                models = {"Model": [bkgRej[i, j, :], bkgErr[i, j, :]]}

            plotErrorBarRatioPlot(muBinsCenter[0], muBinsCenter[1], reference,
                                  models, outDir + f"/BkgRejVsMu" +
                                  f"{workingPoint}_et_{etBins[i]}_" +
                                  f"{etBins[i+1]}_eta_{etaBins[j]}_" +
                                  f"{etaBins[j+1]}.pdf",
                                  xlabel=r"$<\mu>$",
                                  ylabelTop="1 / Background Efficiency",
                                  ylabelBot="Ratio to LH",
                                  text=f"Overall signal efficiency \n" +
                                       f"corresponding to {workingPoint}\n" +
                                       rf"${etBins[i]} < E_T\ $" +
                                       rf"$[\mathrm{{GeV}}]$" +
                                       rf"$\leq {etBins[i+1]}$" + "\n" +
                                       rf"${etaBins[j]} < |\eta| \leq $" +
                                       rf"${etaBins[j+1]}$")


def plotMigrationMatrices(logDir, plotData, preds, truth, truthNames, etBins,
                          etaBins, workingPoint="LHLoose"):
    """
    Make plots of migration matrices for the NN vs the LH, binned and unbinned.

    Args
    ----
        logDir: *str*
            See in top doc string.
        plotData: *dict*
            See in top doc string.
            Needed variables: et, eta, variable given in workingPoint.
        preds: *array*
            See in top doc string.
            Only single model plotting is supported.
        truth: *array*
            Truth values for the electrons. This can also be a multiclass
            labeling. Electrons with the same class according to these values
            will be plotted in the same plot.
        truthNames: *list*
            List of names of the different classes in truth. The number of
            names given should be the same as the number of actual classes in
            the data.
        etBins: *array*
            See in top doc string.
        etaBins: *array*
            See in top doc string.
        workingPoint: *str*
            LH working point to which the NN should be compared to. The cut
            values of the NN are chosen such that the NN has the same signal
            efficiencies as the LH working point.

    """
    # Turn predictions into bools
    boolPreds = makePredsToBools(logDir, plotData, preds, etBins, etaBins,
                                 workingPoint)

    labelsX = ["Reject", "Accept"]
    labelsY = ["Accept", "Reject"]
    weight = plotData["pileupWeight"]
    for i in range(len(truthNames)):
        migMatrix = np.zeros(shape=(2, 2))

        migMatrix[0][0] = (np.sum(weight[(boolPreds == 1) &
                                         (plotData[workingPoint] == 0) &
                                         (truth == i)]) /
                           np.sum(weight[truth == i]))
        migMatrix[0][1] = (np.sum(weight[(boolPreds == 1) &
                                         (plotData[workingPoint] == 1) &
                                         (truth == i)]) /
                           np.sum(weight[truth == i]))
        migMatrix[1][0] = (np.sum(weight[(boolPreds == 0) &
                                         (plotData[workingPoint] == 0) &
                                         (truth == i)]) /
                           np.sum(weight[truth == i]))
        migMatrix[1][1] = (np.sum(weight[(boolPreds == 0) &
                                         (plotData[workingPoint] == 1) &
                                         (truth == i)]) /
                           np.sum(weight[truth == i]))

        fig, ax = plt.subplots()
        sns.heatmap(migMatrix, vmin=0, vmax=1,
                    annot=True, fmt='.3g', annot_kws={'fontsize': 30},
                    cmap=sns.color_palette("viridis", 21), ax=ax,
                    xticklabels=labelsX, yticklabels=labelsY
                    )
        ax.set_ylabel("DNN")
        ax.set_xlabel(workingPoint)
        ax.set_title(truthNames[i])

        plt.tight_layout()
        fig.savefig(logDir + "/Plots/MigrationMatrices/" +
                    f"migrationMatrix{truthNames[i]}.pdf")
        plt.close("all")

    # Iterate over the single et-/etaBins
    for i, (et_low, et_up) in enumerate(convertBins(etBins)):
        for j, (eta_low, eta_up) in enumerate(convertBins(etaBins)):
            # mask to select only electron candidates in this et/etaBin
            mask = (
                     (plotData["et"] > et_low) & (plotData["et"] <= et_up)
                     & (np.abs(plotData["eta"]) > eta_low)
                     & (np.abs(plotData["eta"]) <= eta_up)
                   )

            for k in range(len(truthNames)):
                migMatrix = np.zeros(shape=(2, 2))
                fullMask = (truth == k) & (mask == 1)

                migMatrix[0][0] = (np.sum(weight[(boolPreds == 1) &
                                                 (plotData[workingPoint] == 1)
                                                 & (fullMask == 1)]) /
                                   np.sum(weight[fullMask == 1]))
                migMatrix[0][1] = (np.sum(weight[(boolPreds == 1) &
                                                 (plotData[workingPoint] == 0)
                                                 & (fullMask == 1)]) /
                                   np.sum(weight[fullMask == 1]))
                migMatrix[1][0] = (np.sum(weight[(boolPreds == 0) &
                                                 (plotData[workingPoint] == 1)
                                                 & (fullMask == 1)]) /
                                   np.sum(weight[fullMask == 1]))
                migMatrix[1][1] = (np.sum(weight[(boolPreds == 0) &
                                          (plotData[workingPoint] == 0)
                                          & (fullMask == 1)]) /
                                   np.sum(weight[fullMask == 1]))

                fig, ax = plt.subplots()
                sns.heatmap(migMatrix, vmin=0, vmax=1, ax=ax,
                            annot=True, fmt='.3g', annot_kws={'fontsize': 30},
                            cmap=sns.color_palette("viridis", 21),
                            xticklabels=labelsX, yticklabels=labelsY)
                ax.set_ylabel("DNN")
                ax.set_xlabel(workingPoint)
                ax.set_title(truthNames[k] +
                             rf"$; {et_low} < E_T < {et_up};$" +
                             rf" {eta_low} < |\eta| < {eta_up}$")

                plt.tight_layout()
                fig.savefig(logDir + f"/Plots/MigrationMatrices/Binned/" +
                            f"migrationMatrix{truthNames[k]}_" +
                            f"et_{et_low}_{et_up}_eta_{eta_low}_{eta_up}.pdf")
                plt.close("all")
