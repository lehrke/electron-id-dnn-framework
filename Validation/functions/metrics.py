"""
Functions for calculating different metrics.

Quite a few metrics are basically just wrappers for sklearn implementations
to make all metrics have a similar API and so on.

Common arguments for the functions in this file. If there are specific things
for a function, they will be mentioned in the functions doc string.

preds: *array*
    Array containing the predictions of the MVA model between 0 and 1.

truth: *array*
    Array containing the truth information of the data points. So far only
    binary metrics are supported, therefore the truth information has to be
    either 0 or 1.

weights: *array*
    Array containing weights for each data point. This is not needed, all
    weights will be equal to 1 if this argument is not given.

"""
import numpy as np
from sklearn.metrics import (log_loss, roc_auc_score, f1_score,
                             cohen_kappa_score, roc_curve)
from .plottingFunctions import convertBins


def bkgEffAtSigEff(preds, truth, sigEff, weights=None):
    """
    Calculate the background efficiency at a certain signal efficiency.

    Args
    ----
        preds : *array*
            See in top doc string.
        truth : *array*
            See in top doc string.
        sigEff : *float*
            Signal efficeincy at which the bkg efficeincy should be calculated.
            Should be between 0 and 1.
        weights : *array*
            See in top doc string.

    Returns
    -------
        bkgEff : *float*
            Background efficiency at the given signal efficiency.

    """
    bkgEff = effAtOppEff(preds, truth, sigEff, weights, True)
    return bkgEff


def meanBkgEffAtSigEff(preds, truth, etBins, et, etaBins, eta, sigEff,
                       weights=None):
    """
    Calculate the mean of bkgEffs in bins of Et/eta with a sigEff in each bin.

    Args
    ----
        preds : *array*
            See in top doc string.
        truth : *array*
            See in top doc string.
        etBins: *array*
            Array containing bin edges in Et.
        et: *array*
            Et values of each of the data points
        etaBins: *array*
            Array containing the bin edges in absolute eta.
        eta: *array*
            eta values of each of the data points.
        sigEff: *float*
            Signal efficiency at which in each bin the bkg eff will be
            calculated.
        weights: *array*
            See in top doc string.

    Returns
    -------
        meanBkgEffs: *float*
            Mean of the bkg effs in each of the Et/eta bins.

    """
    bkgEffs = []

    for i, (et_low, et_up) in enumerate(convertBins(etBins)):
        for j, (eta_low, eta_up) in enumerate(convertBins(etaBins)):
            # mask to select only electron candidates in this et/etaBin
            mask = (
                     (et > et_low) & (et <= et_up)
                     & (np.abs(eta) > eta_low)
                     & (np.abs(eta) <= eta_up)
                   )

            if weights is None:
                bkgEffs.append(bkgEffAtSigEff(preds[mask == 1],
                                              truth[mask == 1],
                                              sigEff))
            else:
                bkgEffs.append(bkgEffAtSigEff(preds[mask == 1],
                                              truth[mask == 1],
                                              sigEff,
                                              weights=weights[mask == 1]))

    return np.mean(bkgEffs)


def sigEffAtBkgEff(preds, truth, bkgEff, weights=None):
    """
    Calculate the signal efficiency at a certain background efficiency.

    Args
    ----
        preds : *array*
            See in top doc string.
        truth : *array*
            See in top doc string.
        bkgEff : *float*
            Background efficeincy at which the signal efficeincy should be
            calculated. Should be between 0 and 1.
        weights : *array*
            See in top doc string.


    Returns
    -------
        sigEff : *float*
            Signal efficiency at the given background efficiency.

    """
    sigEff = effAtOppEff(preds, truth, bkgEff, weights, False)
    return sigEff


def effAtOppEff(preds, truth, eff, weights, sig=True):
    """
    Calculate sig/bkg efficiency at a given efficiency of the bkg/sig.

    Args
    ----
        preds : *array*
            See in top doc string.
        truth : *array*
            See in top doc string.
        eff : *float*
            Efficiency at which the other efficiency should be calculated.
            Should be between 0 and 1.
        weights : *array*
            See in top doc string.
        sig : *bool*
            Default=True; If true, calculate signal efficiency,
            if false, calculate bkg efficiency.

    Returns
    -------
        effReturn : *float*
            Calculated efficiency.

    """
    if np.abs(1 - eff) <= 1e-5:
        return 1
    elif np.abs(eff) <= 1e-10:
        return 0
    if not weights:
        weights = np.ones(preds.shape)
    oppWeights = weights[truth == 1 if sig else truth == 0]
    oppPreds = preds[truth == 1 if sig else truth == 0]
    weights = weights[truth == 0 if sig else truth == 1]
    preds = preds[truth == 0 if sig else truth == 1]

    oppInd = np.argsort(-oppPreds)

    accOppEff = np.cumsum(oppWeights[oppInd]) / np.sum(oppWeights[oppInd])
    cutVal = oppPreds[oppInd][np.digitize(eff, accOppEff)]

    effReturn = np.sum(weights[preds > cutVal]) / np.sum(weights)

    return effReturn


def binaryCrossentropy(preds, truth, weights=None):
    """
    Calculate binary cross entropy of predictions.

    Args
    ----
        preds : *array*
            See in top doc string.
        truth : *array*
            See in top doc string.
        weights : *array*
            See in top doc string.

    Returns
    -------
        bce : *float*
            Binary cross entropy of the predictions.

    """
    if not weights:
        weights = np.ones(preds.shape)
    bce = log_loss(truth, preds, eps=1e-7, sample_weight=weights)
    return bce


def auc(preds, truth, weights=None):
    """
    Calculate the AUC of the ROC curve of predictions.

    Args
    ----
        preds : *array*
            See in top doc string.
        truth : *array*
            See in top doc string.
        weights : *array*
            See in top doc string.

    Returns
    -------
        auc : *float*
            Area Under the Curve for the roc curve.

    """
    auc = roc_auc_score(truth, preds, sample_weight=weights)
    return auc


def aucWindow(preds, truth, weights=None, low=0.60, up=0.98):
    """
    Calculate the AUC of the ROC curve of predictions.

    Args
    ----
        preds : *array*
            See in top doc string.
        truth : *array*
            See in top doc string.
        weights : *array*
            See in top doc string.
        low: *float*
            Lower bound of the window of the signal efficiency in which the AUC
            will be calculated.
        up: *float*
            Upper bound of the window of the signal efficiency in which the AUC
            will be calculated.

    Returns
    -------
        aucLowUp : *float*
            Area Under the Curve for the roc curve in the window betweeen low
            and up.

    """
    # Calculate the fpr and tpr
    fpr, tpr, _ = roc_curve(truth, preds, sample_weight=weights)
    # Interpolate the fpr to get the value at the low/up end of the window
    fpLow = np.interp(low, tpr, fpr)
    fpUp = np.interp(up, tpr, fpr)
    # Add the fpr at the low/up end of the window and the values of the edges
    tpr = np.append(tpr, [low, up])
    fpr = np.append(fpr, [fpLow, fpUp])
    # Mask to only have fpr/tpr in the desired window
    fpr = fpr[(tpr <= up) & (tpr >= low)]
    tpr = tpr[(tpr <= up) & (tpr >= low)]

    # Estimate the AUC using a trapezoidal integration.
    aucLowUp = np.trapz(fpr, tpr)
    return 1 - aucLowUp


def f1Score(preds, truth, weights=None):
    """
    Calculate the f1 score of predictions.

    Args
    ----
        preds : *array*
            See in top doc string.
        truth : *array*
            See in top doc string.
        weights : *array*
            See in top doc string.

    Returns
    -------
        f1 : *float*
            F1 score of the predictions.

    """
    f1 = f1_score(truth, preds, sample_weight=weights)
    return f1


def cohenKappa(preds, truth, weights=None):
    """
    Calculate the Cohen Kappa score of predictions.

    Args
    ----
        preds : *array*
            See in top doc string.
        truth : *array*
            See in top doc string.
        weights : *array*
            See in top doc string.

    Returns
    -------
        ck : *float*
            Cohen Kappa score of the predictions.

    """
    ck = cohen_kappa_score(truth, preds, sample_weight=weights)
    return ck
