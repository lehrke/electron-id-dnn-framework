"""Plot performance plot for several trained networks."""
import numpy as np
import h5py
import argparse
import copy
import json
import sys
sys.path.insert(0, sys.path[0]+"/../")
from CommonFunctions.functions import mkdir
import Validation.functions.plottingFunctions as pl


def getParser():
    """Get options set in command line."""
    parser = argparse.ArgumentParser(description="Options for evaluation of " +
                                                 "one NN training")

    parser.add_argument("--outDir", action="store", required=True, type=str,
                        help="Directory where to save the plots.")
    parser.add_argument("--inputFile", action="store", type=str,
                        help="File to load data from.")
    parser.add_argument("--preds", action="store", type=str, nargs="+",
                        help="Predictions of networks to plot.")
    parser.add_argument("--labels", action="store", type=str, nargs="+",
                        help="Labels for the networks.")
    parser.add_argument("--applyAmbiguityCut", action="store", type=int,
                        nargs="+",
                        help="Cut to be applied to the ambiguityBit. " +
                             "If given a list for each prediction file " +
                             "the cut can be chosen. " +
                             "LHLoose and LHMedium equivalent with 5, " +
                             "LHTight equivalent with 2")
    parser.add_argument("--applyBLayerCut", action="store", type=int,
                        nargs="+",
                        help="Whether to apply a cut on the BlayerHits. " +
                             "If given a list for each prediction file " +
                             "the cut can be turned on." +
                             "LHLoose equivalent, do not turn on (0), " +
                             "LHMedium and LHTight equivalent, turn on (1).")
    parser.add_argument("--plotInclusiveEfficiencies", action="store_true",
                        help="Plot inclusive efficiencies.")
    parser.add_argument("--plotBkgClassesSeparate", action="store_true",
                        help="Plot all plots where the plot makes sense " +
                             "with only one bkg class with each bkg class " +
                             "separately.")
    parser.add_argument("--correctedMC", action="store_true",
                        help="Whether the MC data file for the evaluation " +
                             "was a corrected MC file.")
    parser.add_argument("--dataSet", action="store", type=str,
                        choices=["ZJF", "Ztt"], default="ZJF")

    args = parser.parse_args()

    return args


def plotMulti(args, ambCut, BLrCut, dataSet):
    """
    Make plots for several or one trained network.

    Args
    ----
        args: ??
            Arguments of the argument parser (see above).
        ambCut: *list*
            List which is one if a cut on the ambiguity bit should be applied
            to the predictions corresponding to this position.
        BLrCur: *list*
            List which is one if a cut on the number of b-layer should be
            applied to the predictions corresponding to this position.
        dataSet: *str*
            String which will be put on plots to indicate which dataset was
            used for the evaluation.

    """
    ###########################################################################
    # Load predictions and data
    ###########################################################################
    plotData = {}
    with h5py.File(args.inputFile, "r") as hf:
        plotData["truth"] = hf["test/p_signal"][:]
        plotData["LHLoose"] = hf["test/p_LHLoose"][:]
        plotData["LHMedium"] = hf["test/p_LHMedium"][:]
        plotData["LHTight"] = hf["test/p_LHTight"][:]
        plotData["pileupWeight"] = hf["test/p_pileupWeight"][:]
        plotData["iff"] = hf["test/p_iff"][:]
        plotData["truthType"] = hf["test/p_truthType"][:]
        plotData["et"] = hf["test/p_et"][:] / 1e3
        plotData["eta"] = hf["test/p_eta"][:]
        plotData["nBlayerCut"] = hf["test/p_LHBlayerCut"][:]
        plotData["ambiguityBit"] = hf["test/p_ambiguityBit"][:]
        plotData["mu"] = hf["test/p_mu"][:]

    mask = (plotData["truth"] == 1) | (plotData["iff"] == 5) | \
           (plotData["iff"] == 8) | (plotData["iff"] == 9) | \
           (plotData["iff"] == 10)

    for key in plotData:
        plotData[key] = plotData[key][mask == 1]

    mkdir(args.outDir)

    storeData = {"predFiles": args.preds,
                 "labels": args.labels,
                 "applyAmbiguityCut": ambCut,
                 "applyBLayerCut": BLrCut}
    json.dump(storeData, open(args.outDir+"/storeData.json", "w"), indent=4)

    preds = {}
    for i, p in enumerate(args.preds):
        preds[i] = {}
        preds[i]["preds"] = np.load(p)[mask == 1]
        if ambCut is not None:
            preds[i]["preds"][plotData["ambiguityBit"] >= ambCut[i]] = -999.
        if BLrCut is not None and BLrCut[i] == 1:
            preds[i]["preds"][plotData["nBlayerCut"] == 0] = -999.
        preds[i]["label"] = args.labels[i]

    etBins = np.array([15, 20, 25, 30, 35, 40, 60, 100, 250, 2000])
    etaBins = np.array([0, 0.8, 1.37, 1.52, 2.01, 2.47])
    muBins = np.array([0, 20, 30, 40, 50, 60, 80, 100])

    bkgMasks = {
                 "PhotonConv": (plotData["truth"] == 1) |
                               (plotData["iff"] == 5),
                 "HeavyFlavor": ((plotData["truth"] == 1)
                                 | (plotData["iff"] == 8)
                                 | (plotData["iff"] == 9)),
                 "LFEgamma": ((plotData["truth"] == 1) |
                              ((plotData["iff"] == 10)
                               & ((plotData["truthType"] == 4)
                               | (plotData["truthType"] == 16)))),
                 "LFHadron": ((plotData["truth"] == 1) |
                              ((plotData["iff"] == 10)
                               & (plotData["truthType"] == 17))),
               }

    ###########################################################################
    # Plot binned roc curve
    ###########################################################################
    mkdir(args.outDir + "/Plots/")
    mkdir(args.outDir + "/Plots/ROC/")
    mkdir(args.outDir + "/Plots/ROC/Binned/")
    pl.plotBinnedROCCurve(args.outDir, plotData, preds, etBins, etaBins,
                          weights=plotData["pileupWeight"],
                          text="Evaluated on " + dataSet)
    if args.plotBkgClassesSeparate:
        for bkgKey in bkgMasks:
            pl.plotBinnedROCCurve(args.outDir, copy.deepcopy(plotData),
                                  copy.deepcopy(preds), etBins, etaBins,
                                  bkgMasks, bkgKey,
                                  weights=plotData["pileupWeight"],
                                  text="Evaluated on " + dataSet)

    ###########################################################################
    # Plot inclusive efficiencies
    ###########################################################################
    if args.plotInclusiveEfficiencies:
        mkdir(args.outDir + "/Plots/InclusiveSigEff/")
        pl.plotEfficienciesInclusiveSigEff(args.outDir, plotData, preds,
                                           etBins, etaBins, muBins, sigEff=0.8)


def processArguments(args):
    """
    Based on the arguments of the ArgParser, configure some variables.

    Args
    ----
        args: ??
            Arguments of the argument parser (see above).

    Returns
    -------
        ambCut: *list*
            List which is one if a cut on the ambiguity bit should be applied
            to the predictions corresponding to this position.
        BLrCur: *list*
            List which is one if a cut on the number of b-layer should be
            applied to the predictions corresponding to this position.
        dataSet: *str*
            String which will be put on plots to indicate which dataset was
            used for the evaluation.

    """
    if args.applyAmbiguityCut is not None and len(args.applyAmbiguityCut) == 1:
        ambCut = args.applyAmbiguityCut * len(args.preds)
    else:
        ambCut = args.applyAmbiguityCut
    if args.applyBLayerCut is not None and len(args.applyBLayerCut) == 1:
        BLrCut = args.applyBLayerCut * len(args.preds)
    else:
        BLrCut = args.applyBLayerCut

    if args.dataSet == "ZJF":
        dataSet = r"$Z+\mathrm{JF17}$"
    else:
        dataSet = r"$Z+t\bar{t}$"

    if args.correctedMC:
        dataSet = "Corrected " + dataSet

    return ambCut, BLrCut, dataSet


if __name__ == "__main__":
    args = getParser()

    ambCut, BLrCut, dataSet = processArguments(args)

    plotMulti(args, ambCut, BLrCut, dataSet)
